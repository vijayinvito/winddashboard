document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        editable: false,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        selectable: false,
        eventRender: function (info) {
            let eventTitle = info.event.title;
            let location = "at " + info.event.extendedProps.location;
            let eventTest = "Test";

            $(info.el).popover({
                title: info.event.title,
                placement: 'top',
                trigger: 'focus click',
                content: `${location}<br />${info.event.extendedProps.description}`,
                container: 'body',
                html: true
            }).popover('show');
        },
        events: {
            url: 'calendar/events.php',
            failure: function () {
                document.getElementById('script-warning').style.display = 'block';
            },
            success: function (eventSource) {
                calendar.removeAllEvents();

                eventSource.forEach(element => {
                    newEvent = {
                        title: element.name,
                        start: element.appointment_start,
                        end: element.appointment_end,
                        allDay: false,
                        html: true,
                        location: `${element.address}<br />${element.city}, ${element.state}, ${element.zipcode}`,
                        description: `Inspector: ${element.inspector_name}<br /><a target="_blank" href="../request?reqid=${element.reqid}">View Request</a>`
                    };

                    calendar.addEvent(newEvent);
                })
            }
        },
        loading: function (bool) {
            document.getElementById('loading').style.display =
                bool ? 'block' : 'none';
        }
    });

    calendar.render();
});