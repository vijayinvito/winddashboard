<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php if (!(isset($_SESSION['user']))) {
        echo '<script> window.location = "/index.php"; </script>';
    } ?>
    <title>Change Password | <?php echo $site['name'] ?></title>
</head>

<body>
<?php include('includes/header.php'); ?>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">Change Password
    </h4>
</div>
<div class="px-4 py-3">
    <?php
    $q = $a->con->prepare("select * from users where userid = ?");
    $q->execute([$_SESSION['user']]);
    $res = $q->fetch();
    ?>


    <form method="post" class="col-md-7 p-0 p-4 bg-white py-4  border rounded-10 shadow-sm">
        <div class="row">
            <div class="col-12 mb-3 font-weight-500">
                * Password should be more than 7 characters.
            </div>
            <div class="col-12 p-3">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Current Password *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" name="oldpass" maxlength="255" type="password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>New Password *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" name="pass" maxlength="255" type="password" required>
                    </div>
                </div>
                <div class="form-group row mt-4">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9">
                        <button type="submit" name="cpsubmit"
                                class="btn btn-dark px-4 btn-submit border shadow bg-main">Change password
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php
    if (isset($_POST['cpsubmit'])) {
        if (strlen($_POST['pass']) > 6) {
            $lr = $a->con->prepare("select * from users where userid = ?");
            $lr->execute([$_SESSION['user']]);
        if ($lr->rowCount() > 0) {
            $rl = $lr->fetch();
            $validPassword = password_verify($_POST['oldpass'], $rl['password']);
        if ($validPassword) {
            $uq = $a->con->prepare("update users set password = ? where userid = ?");
            $status = $uq->execute([password_hash($_POST['pass'], PASSWORD_BCRYPT), $_SESSION['user']]);
            if ($status) {
                $_SESSION['message'] = 'password';
                echo '<script> window.location = window.location.href; </script>';
                exit();
            }
        }
        else {
            ?>
            <script>
                swal("Invalid Current Password", "Please Try Again", "error");

            </script>
        <?php
        }
        }
        else{
        ?>
            <script>
                swal("Invalid Current Password", "Please Try Again", "error");

            </script>
        <?php
        }
        }
        else{
        ?>
            <script>
                swal("Password should not be less than 7 characters", "Please Try Again", "error");

            </script>
            <?php
        }
    }
    ?>


</div>
</body>

<?php include('includes/footer.php'); ?>

</html>


<script>
    $(".nav.updatepassword").addClass('active-link');

</script>
