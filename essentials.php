<meta charset=”UTF-8”>
<meta name="viewport" content="width=device-width, initial-scale=0.9, minimum-scale=0.9, shrink-to-fit=no">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/css/simple-sidebar.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;600;700;800;900&family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assets/swal/sweetalert.css" />
<script src="assets/swal/sweetalert.min.js"></script>

<?php 
class cls{
public $con;
function __construct(){
    include('cs.php');
    $this->con = new PDO($dbhost,$dbuser,$dbpass);
}
    
}

$a = new cls();
$sitequery = $a->con->prepare("select * from website");
$sitequery->execute();
$site = $sitequery->fetch();
$sitename = $site['name'];
$siteurl = "http://windmitigationlogin.com/";
$sitemail = "donot-reply@windmitigationlogin.com";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: $sitename <$sitemail>\nX-Mailer: PHP/";
?>
<link rel="icon" href="assets/images/favicon.jpg">

<script>
    $("#menu-toggle").click(function() {
        $("#wrapper").toggleClass("toggled");
    });

</script>
