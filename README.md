<a href="https://coderpro.net" target="_blank"><img src="https://camo.githubusercontent.com/380a13b02fa52c8d9105fa15792ca3b783bfc9fd91211767b574e4e79485486b/68747470733a2f2f636f64657270726f2e6e65742f6d656469612f313032342f636f64657270726f5f6c6f676f5f726f756e6465645f65787472612d39307839302e77656270" align="right" width="90" /></a>

[![LinkedIn][linkedin-shield]][linkedin-url]
[![Twitter](https://img.shields.io/twitter/url/https/twitter.com/cloudposse.svg?style=social&label=Follow%20%40coderProNet)](https://twitter.com/coderProNet)
[![GitHub](https://img.shields.io/github/followers/coderpros?label=Follow&style=social)](https://github.com/coderpros)

# wind-mitigation-login

> A homegrown PHP Core customer relationship manager  

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/company/coderpros
[twitter-shield]: https://img.shields.io/twitter/follow/coderpronet?style=social
[twitter-follow-url]: https://img.shields.io/twitter/follow/coderpronet?style=social
[github-shield]: https://img.shields.io/github/followers/coderpros?label=Follow&style=social
[github-follow-url]: https://img.shields.io/twitter/follow/coderpronet?style=social


## Installation Instructions
Not applicable

## Change Log
- 2021/11/30:
    - Finished first round of changes. 
- 2021/11/29:
    - Initial Checkin.
