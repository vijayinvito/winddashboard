<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include('includes/essentials.php'); ?>
    <title>Verify Account | <?php echo $site['name']; ?></title>
</head>

<body class="text-center">
<div class="p-5 text-center bg-black text-light">
    <img class="col-lg-4 col-md-6 col-sm-12 ml-auto mr-auto p-0" src="assets/images/logo.png">
    <br>
    <br>
    <br>
    <h1 class="text-light mb-0"> Account Verification </h1>
</div>
<?php if (isset($_GET['e'])) { ?>
    <div class="container p-5">
        <center>
            <?php
            $rel = $a->con->prepare("select * from users where email = ? and token = ?");
            $rel->execute([$_GET['e'], $_GET['t']]);
            if ($rel->rowCount() > 0) {
                $res = $rel->fetch();
                if ($res['status'] == 'Email') {
                    $result = $a->con->prepare("update users set status = 'Review' where email = ? and token = ?");
                    $result->execute([$_GET['e'], $_GET['t']]);
                    $e = $_GET['e'];
                    $name = $res['name'];
                    $message = '
Hi ' . $name . '<br>
Thank you for verifying your email. Your account has been sent to our team for review. You will be notified when its ready to use! <br><br>
<a href="http://' . $_SERVER['SERVER_NAME'] . '/index">Visit Portal</a>
<br><br>
Thank You!<br>
<b>' . $site['name'] . '</b>';
                    $sub = "Email Verified Successfully";
                    if (mail($e, $sub, $message, $headers)) {
                        echo '<h3> Email verified successfully! </h3>
        <p>Your account has been sent to our team for review. You will be notified when its ready to use!</p>
        <br>
        ';
                        echo '
        <a class="btn p-2 bg-main text-light pl-3 pr-3 mt-3" href="index.php"> Visit Portal </a>
        ';
                    }
                } else if ($res['status'] == 'Inactive') {
                    echo '<h3> Your account has been suspended </h3>';
                } else if ($res['status'] == 'Review') {
                    echo '<h3> Your account is under review </h3>';
                } else if ($res['status'] == 'Active') {
                    echo '<h3> Your email has already been verified </h3>';
                    echo '
        <a class="btn p-2 bg-main text-light pl-3 pr-3 mt-3" href="index.php"> Sign In to your Account </a>
        ';
                }
            } else {
                echo '<h3> Invalid Account Credentials </h3>';
            }
            ?></center>
    </div>
<?php } else {
    echo '<h3 class="py-5"> Invalid Account Credentials </h3>';
} ?>
<?php include('includes/footer.php'); ?>
</body>

</html>
