<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php if (!(isset($_SESSION['user']))) {
        echo '<script> window.location = "/index.php;" </script>';
    } ?>
    <title>My Profile | <?php echo $site['name'] ?></title>
</head>

<body>
<?php include('includes/header.php'); ?>

<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">My Profile
    </h4>
</div>
<div class="px-4 py-3">

    <?php
    if (isset($_SESSION['profile'])) {
        if ($_SESSION['profile'] == 'updated') {
            ?>
            <div class="alert alert-success rounded-10 font-weight-bold">
                Profile Updated Successfully
            </div>
            <?php
        }
        unset($_SESSION['profile']);
    } ?>
    <?php
    $q = $a->con->prepare("select * from users where userid = ?");
    $q->execute([$_SESSION['user']]);
    $res = $q->fetch();
    ?>

    <?php if ($user['type'] == 'Agency') { ?>
        <div class="row">
            <div class="col-md-8">
                <form method="post" class="p-0 p-4 bg-white py-4 border rounded-10 shadow-sm h-100"
                      enctype="multipart/form-data">
                    <div class="p-2">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Company Name</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="company" type="text" maxlength="100"
                                       value="<?php echo $res['company']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Company Address</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="address" type="text" maxlength="255"
                                       value="<?php echo $res['address']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>City</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="city" type="text" maxlength="30"
                                       value="<?php echo $res['city']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Zip Code</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="zipcode" type="text" maxlength="30"
                                       value="<?php echo $res['zipcode']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Company Phone</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="companyphone" type="tel" maxlength="20"
                                       value="<?php echo $res['companyphone']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Agent Full Name</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="name" type="text" maxlength="100"
                                       value="<?php echo $res['name']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Agent Direct Number</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="phone" type="tel" maxlength="20"
                                       value="<?php echo $res['phone']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Email Address</label>
                            </div>
                            <div class="col-md-9">
                                <input class="form-control" name="email" type="email" maxlength="255"
                                       placeholder="Email Address" value="<?php echo $res['email']; ?>" readonly
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label> Logo </label>
                            </div>
                            <div class="col-md-9">
                                <div class="file-upload">
                                    <div class="file-select">
                                        <div class="file-select-button" id="fileName">Choose File</div>
                                        <div class="file-select-name" id="noFile">Click to Change</div>
                                        <input type="file" accept="image/*" name="logo" id="logo"
                                               onchange="readURL(this);">
                                    </div>
                                </div>
                                <span class="font-85"><i class="fas fa-info-circle fa-sm"></i> Make sure logo looks good on the black background.</span>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-9">
                                <button type="submit" name="submit" class="btn btn-dark px-4 border shadow bg-main">Save
                                    Changes
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="col-12 p-0 bg-black text-center p-3 pt-5 rounded-10 shadow-sm h-100">
                    <img class="logopreview" style="max-width:15rem;"
                         src="<?php echo ($res['logo'] != '') ? $res['logo'] : $site['logo']; ?>">
                </div>
            </div>
        </div>
    <?php } else {
        ?>
        <form method="post" class="col-md-8 p-0 p-4 bg-white py-4  border rounded-10 shadow-sm">
            <div class="p-2">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Name *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" name="name" type="text" maxlength="100" placeholder="Name"
                               value="<?php echo $res['name']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Phone Number *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" name="phone" type="tel" maxlength="100" placeholder="Phone Number"
                               value="<?php echo $res['phone']; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Email Address *</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" name="email" type="email" maxlength="255"
                               placeholder="Email Address" value="<?php echo $res['email']; ?>" readonly required>
                    </div>
                </div>
                <div class="form-group row mt-4">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9">
                        <button type="submit" name="submit" class="btn btn-dark px-4 border shadow bg-main">Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <?php
    } ?>

    <?php
    if (isset($_POST['submit'])) {
        if ($user['type'] == 'Agency') {
            $img = $_FILES['logo']['tmp_name'];
            $pic = $_FILES['logo']['name'];
            if ($pic != '') {
                $path = "assets/images/logos/" . $_SESSION['user'] . ".png";
                copy($img, $path);
            } else {
                $path = $res['logo'];
            }
            $query = $a->con->prepare("update users set company = ?,address=?,city=?,zipcode=?,companyphone=?,name=?,phone=?,logo=? where userid = ?");
            $status = $query->execute([$_POST['company'], $_POST['address'], $_POST['city'], $_POST['zipcode'], $_POST['companyphone'], $_POST['name'], $_POST['phone'], $path, $_SESSION['user']]);
        } else {
            $query = $a->con->prepare("update users set name = ?,phone=?  where userid = ?");
            $status = $query->execute([$_POST['name'], $_POST['phone'], $_SESSION['user']]);
        }
        if ($status) {
            $_SESSION['profile'] = 'updated';
            echo '<script> window.location = window.location.href; </script>';
            exit();
        }
    }
    ?>
</div>
</body>

</html>

<script>
    $(".nav.updateprofile").addClass('active-link');

</script>

<script>
    function readURL(input) {
        var filename = $("#logo").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").text("No file chosen...");
        } else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
        }

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.logopreview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }

    }

</script>
