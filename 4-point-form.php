<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Inspector' && $user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>4 Point Form | <?php echo $site['name'] ?></title>
    <style>
        /*
        .form-group.row .col-md-3,
        .form-group.row .col-md-4 {
            padding-top: 6px;
        }
*/

        #main-page,
        #first-page,
        #second-page,
        #third-page,
        #forth-page {
            background: white;
        }

        .input-group-text {
            background-color: transparent;
            padding: .275rem .55rem;
            border: 0px;
            font-size: 14px;
        }

        label.border {
            background-color: transparent;
            border: 0px !important;
            font-size: 14px;
        }

        input {
            font-size: 15px !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        .images input {
            border: 0px !important;
            border-bottom: 1px solid #ccc !important;
        }

        label.px-2 {
            font-weight: 400 !important;
            font-size: 14px;
            color: #495057;
        }

    </style>
</head>

<body>
<?php
$reqq = $a->con->prepare("select * from 4pointform where reqid = ?");
$reqq->execute([$_GET['reqid']]);
$r = $reqq->fetch();
if (isset($_GET['reqid'])) {
    $requestq = $a->con->prepare("select * from requests where reqid = ?");
    $requestq->execute([$_GET['reqid']]);
    $request = $requestq->fetch();
    if ($request['status'] != 'Assigned' && $reqq->rowCount()) {
        if ($user['type'] == 'Inspector') {
            if (!(isset($_GET['download']))) {
                echo '<script> window.location = "?reqid=' . $_GET['reqid'] . '&download=pdf" </script>';
            }
        }
    }
    if ($request['status'] == 'Completed' && $reqq->rowCount()) {
        if ($user['type'] == 'Admin') {
            if (!(isset($_GET['download']))) {
                echo '<script> window.location = "?reqid=' . $_GET['reqid'] . '&download=pdf" </script>';
            }
        }
    }

} else {
    echo '<script> window.location = "/index.php" </script>';
}
?>
<style>
    .file-upload-img {
        display: block;
        text-align: center;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 12px;
    }

    .file-upload-img .file-select {
        display: block;
        border: 2px solid #dce4ec;
        color: #34495e;
        cursor: pointer;
        height: 240px;
        line-height: 40px;
        text-align: left;
        background: #FFFFFF;
        overflow: hidden;
        position: relative;
    }

    .file-upload-img .file-select .file-select-button {
        background: #dce4ec;
        padding: 0 10px;
        display: inline-block;
        height: 30px;
        line-height: 30px;
        width: 100%;
    }

    .file-upload-img .file-select .file-select-name {
        width: 100%;
        height: auto;
        display: inline-block;
        padding: 50px;
        text-align: center;
    }

    .file-upload-img.active .file-select .file-select-name {
        height: auto;
        display: inline-block;
        padding: 0px;
        text-align: center;
    }

    .file-upload-img .file-select .file-select-name img {
        margin-left: auto;
        margin-right: auto;
        width: auto;
        height: 65px;
        margin: auto;
    }

    .file-upload-img.active .file-select .file-select-name img {
        height: 205px;
        width: 100%;
        margin: auto;
    }

    .file-upload-img .file-select:hover {
        border-color: #34495e;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img .file-select:hover .file-select-button {
        background: #34495e;
        color: #FFFFFF;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img.active .file-select {
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img.active .file-select .file-select-button {
        color: #34495e;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .remove-extra i {
        color: #34495e !important;
    }

    .file-upload-img .file-select input[type=file] {
        z-index: 100;
        cursor: pointer;
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .file-upload-img .file-select.file-select-disabled {
        opacity: 0.65;
    }

    .file-upload-img .file-select.file-select-disabled:hover {
        cursor: default;
        display: block;
        border: 2px solid #dce4ec;
        color: #34495e;
        cursor: pointer;
        height: 40px;
        line-height: 40px;
        margin-top: 5px;
        text-align: left;
        background: #FFFFFF;
        overflow: hidden;
        position: relative;
    }

    .file-upload-img .file-select.file-select-disabled:hover .file-select-button {
        background: #dce4ec;
        color: #666666;
        padding: 0 10px;
        display: inline-block;
        height: 40px;
        line-height: 40px;
    }

    .file-upload-img .file-select.file-select-disabled:hover .file-select-name {
        line-height: 40px;
        display: inline-block;
        padding: 0 10px;
    }

    @media (max-width: 786px) {
        .file-upload-img .file-select {
            height: 120px;
        }

        .file-upload-img .file-select .file-select-name {
            padding: 10px;
        }
    }

</style>
<?php if (isset($_GET['download'])) {
    if ($reqq->rowCount() > 0) { ?>
        <div class="px-4 mb-3 prepare-progress" style="display:none;">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:10%">Preparing PDF
                </div>
            </div>
            <style>
                .progress .progress-bar {
                    animation: preparing 5s forwards;
                }

                @keyframes preparing {
                    from {
                        width: 10%;
                    }

                    to {
                        width: 100%;
                    }
                }

            </style>
        </div>
    <?php }
} ?>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">4-Point Inspection Form
    </h4>
    <?php if ($reqq->rowCount() > 0) { ?>
        <div>
            <a class="btn btn-primary" href="?reqid=<?php echo $_GET['reqid']; ?>&download=pdf">Download PDF</a>
        </div>
    <?php } ?>
</div>
<div class="px-4 py-3" id="pdfcontent">
    <?php if (isset($_GET['download'])) {
        if ($reqq->rowCount() > 0) { ?>
            <div class="nav justify-content-between text-center mb-3">
                <div>
                    <img src="assets/images/favicon.jpg" width="50">
                </div>
                <div>
                    <h2 class="text-main font-weight-bold mb-0">Windmitigations.com, LLC</h2>
                    <h3 class="text-black font-weight-bold mb-0">Roof Inspection Form</h3>
                </div>
                <div>
                    <img src="assets/images/favicon.jpg" width="50">
                </div>
            </div>
        <?php }
    } ?>
    <div class="col-12 p-0 p-3 bg-white py-4 border rounded-10 shadow-sm main-border">
        <form method="post" enctype="multipart/form-data" class="nav bg-white pt-3 justify-content-between">
            <div id="main-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group nav justify-content-start">
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Insured/Applicant Name:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['applicantname'];
                        } else if (isset($_POST['applicantname'])) {
                            echo $_POST['applicantname'];
                        } ?>" maxlength="255" name="applicantname">
                    </div>
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Application / Policy #:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['policyno'];
                        } else if (isset($_POST['policyno'])) {
                            echo $_POST['policyno'];
                        } ?>" maxlength="255" name="policyno">
                    </div>
                    <div class="form-group input-group col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Address Inspected:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['addressinspected'];
                        } else if (isset($_POST['addressinspected'])) {
                            echo $_POST['addressinspected'];
                        } ?>" maxlength="255" name="addressinspected">
                    </div>
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Actual Year Built:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['actualyearbuilt'];
                        } else if (isset($_POST['actualyearbuilt'])) {
                            echo $_POST['actualyearbuilt'];
                        } ?>" maxlength="255" name="actualyearbuilt">
                    </div>
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Date Inspected:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['dateinspected'];
                        } else if (isset($_POST['dateinspected'])) {
                            echo $_POST['dateinspected'];
                        } ?>" maxlength="255" name="dateinspected">
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="col-12 p-3 border">
                        <h5 class="font-weight-600">Minimum Photo Requirements:</h5>
                        <div class="col-12">
                            <?php
                            $generalconditionarr = ['Dwelling: Each side', 'Roof: Each slope', 'Plumbing: Water heater, under cabinet plumbing/drains, exposed valves', 'Main eletrical service panel with interior door label', 'Electical box with panel off', 'All hazards or definiencies noted in this report'];
                            $gcc = 0;
                            while ($gcc != count($generalconditionarr)) {
                                if ($gcc == 0) {
                                    echo '<div class="nav justify-content-start">';
                                }
                                ?>
                                <label class="p-1 px-2 mb-2"><input type="checkbox" name="photorequirementcheck"
                                                                    value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                        echo ((strpos($r['photorequirement'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                    } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                <br>
                                <?php
                                if ($gcc == 2) {
                                    echo '</div>';
                                }
                                $gcc++;
                            }
                            ?>
                            <input type="hidden" name="photorequirement" value="<?php if ($reqq->rowCount() > 0) {
                                echo $r['photorequirement'];
                            } ?>">
                            <h5 class="col-12 text-center p-0 mb-0 mt-2 font-weight-bold">A Florida-licensed inspector
                                must complete, sign and date this form.</h5>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group col-md-12">
                    <div class="col-12 px-3 py-2 border h6">Be advised that Underwriting will rely on the information in
                        this sample form, or a similar form, that is obtained from the Florida licensed professional of
                        your choice. This information only is used to determine insurability and is not a warranty or
                        assurance of the suitability, fitness or longevity of any of the systems inspected.
                    </div>
                </div>
                <br>
            </div>
            <div id="first-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Electrical System</h4>
                            <h6 class="font-weight-500 mb-0">Separate documentation of any aluminum wiring remediation
                                must be provided and certified by a licensed electrician.</h6>
                        </div>
                        <div class="col-12">
                            <div class="row border-bottom border-secondary">
                                <div class="col-6 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Main Panel</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Type:</span>
                                        </div>
                                        <label class="border form-control"><input type="radio" name="mainpaneltype"
                                                                                  value="Circuit breaker" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['mainpaneltype'] == 'Circuit breaker') ? 'checked' : '';
                                            } ?>> Circuit breaker</label>
                                        <label class="border form-control"><input type="radio" name="mainpaneltype"
                                                                                  value="Fuse" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['mainpaneltype'] == 'Fuse') ? 'checked' : '';
                                            } ?>> Fuse</label>
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Total Amps:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['mainpaneltotalamps'];
                                               } else if (isset($_POST['mainpaneltotalamps'])) {
                                                   echo $_POST['mainpaneltotalamps'];
                                               } ?>" maxlength="255" name="mainpaneltotalamps">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend col-12">
                                                <span class="input-group-text col-12 px-0">Is amperage sufficient for current usage?</span>
                                            </div>
                                            <label class="border form-control"><input name="mainpanelsufficiant"
                                                                                      type="checkbox"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['mainpanelsufficiant'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border form-control"><input name="mainpanelsufficiant"
                                                                                      type="checkbox"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['mainpanelsufficiant'] == 'No') ? 'checked' : '';
                                                } ?>> No (explain)</label>
                                        </div>
                                        <input class="form-control" type="text" name="mainpanelsufficiantexplain"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['mainpanelsufficiantexplain'];
                                               } else if (isset($_POST['mainpanelsufficiantexplain'])) {
                                                   echo $_POST['mainpanelsufficiantexplain'];
                                               } ?>" maxlength="255">
                                    </div>
                                </div>
                                <div class="col-6 p-3 border-secondary">
                                    <h5 class="font-weight-600">Second Panel</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Type:</span>
                                        </div>
                                        <label class="border form-control"><input type="radio" name="secondpaneltype"
                                                                                  value="Circuit breaker" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['secondpaneltype'] == 'Circuit breaker') ? 'checked' : '';
                                            } ?>> Circuit breaker</label>
                                        <label class="border form-control"><input type="radio" name="secondpaneltype"
                                                                                  value="Fuse" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['secondpaneltype'] == 'Fuse') ? 'checked' : '';
                                            } ?>> Fuse</label>
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Total Amps:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['secondpaneltotalamps'];
                                               } else if (isset($_POST['secondpaneltotalamps'])) {
                                                   echo $_POST['secondpaneltotalamps'];
                                               } ?>" maxlength="255" name="secondpaneltotalamps">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend col-12">
                                                <span class="input-group-text col-12 px-0">Is amperage sufficient for current usage?</span>
                                            </div>
                                            <label class="border form-control"><input name="secondpanelsufficiant"
                                                                                      type="checkbox"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['secondpanelsufficiant'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border form-control"><input name="secondpanelsufficiant"
                                                                                      type="checkbox"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['secondpanelsufficiant'] == 'No') ? 'checked' : '';
                                                } ?>> No (explain)</label>
                                        </div>
                                        <input class="form-control" type="text" name="secondpanelsufficiantexplain"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['secondpanelsufficiantexplain'];
                                               } else if (isset($_POST['secondpanelsufficiantexplain'])) {
                                                   echo $_POST['secondpanelsufficiantexplain'];
                                               } ?>" maxlength="255">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="py-3">
                                    <h5 class="font-weight-600">Indicate presence of any of the following:</h5>
                                    <div class="col-12">
                                        <?php
                                        $generalconditionarr = ['Cloth wiring', 'Active knob and tube', 'Branch circuit aluminum wiring (If present, describe the usage of all aluminum wiring):', 'Connections repaired via COPALUM crimp', 'Connections repaired via AlumiConn'];
                                        $gcc = 0;
                                        while ($gcc != count($generalconditionarr)) {
                                            ?>
                                            <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                name="indicatepresensecheck"
                                                                                value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                    echo ((strpos($r['indicatepresense'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                            <br>
                                            <?php
                                            if ($gcc == 2) {
                                                echo '<span class="extra">* If single strand (aluminum branch) wiring, provide details of all remediation. Separate documentation of all work must be provided.<br><br></span>';
                                            }
                                            $gcc++;
                                        }
                                        ?>
                                        <input type="hidden" name="indicatepresense"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['indicatepresense'];
                                               } ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom border-secondary border-top">
                                <div class="col-12 p-3">
                                    <div class="form-group mb-0">
                                        <div class="pt-3">
                                            <h5 class="font-weight-600 mb-3">Hazards Present</h5>
                                            <div class="nav justify-content-between">
                                                <div class="col-6">
                                                    <?php
                                                    $generalconditionarr = ['Blowing fuses', 'Tripping breakers', 'Empty sockets', 'Loose wiring', 'Improper grounding', 'Corrosion', 'Over fusing'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                            name="hazardspresentcheck"
                                                                                            value="<?php echo $generalconditionarr[$gcc]; ?>"> <?php echo $generalconditionarr[$gcc]; ?>
                                                        </label>
                                                        <br>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-6">
                                                    <?php
                                                    $generalconditionarr = ['Double taps', 'Exposed wiring', 'Unsafe wiring', 'Improper breaker size', 'Scorching', 'Other'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                            name="hazardspresentcheck"
                                                                                            value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                                echo ((strpos($r['hazardspresent'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                            } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                                        <br>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                    <br>
                                                    <input class="form-control" name="hazardspresentother">
                                                    <input type="hidden" class="form-control" name="hazardspresent"
                                                           value="<?php if ($reqq->rowCount() > 0) {
                                                               echo $r['hazardspresent'];
                                                           } ?>">
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom border-secondary">
                                <div class="col-12 p-3 border-right border-secondary">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">General condition of the electrical system:</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="electricalsystemcondition"
                                                                                  value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['electricalsystemcondition'] == 'Satisfactory') ? 'checked' : '';
                                                } ?>> Satisfactory</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="electricalsystemcondition"
                                                                                  value="Unatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['electricalsystemcondition'] == 'Unatisfactory') ? 'checked' : '';
                                                } ?>> Unatisfactory (explain)</label>
                                        </div>
                                        <input class="form-control mt-3" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['electricalsystemconditionexplain'];
                                               } else if (isset($_POST['electricalsystemconditionexplain'])) {
                                                   echo $_POST['electricalsystemconditionexplain'];
                                               } ?>" maxlength="255" name="electricalsystemconditionexplain">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="p-3 bg-secondary col-12 text-black">
                                    <h4 class="font-weight-600 mb-0">Electrical System</h4>
                                    <h6 class="font-weight-500 mb-0">Separate documentation of any aluminum wiring
                                        remediation must be provided and certified by a licensed electrician.</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Main Panel</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Panel age:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['mainpanelage'];
                                               } else if (isset($_POST['mainpanelage'])) {
                                                   echo $_POST['mainpanelage'];
                                               } ?>" maxlength="255" name="mainpanelage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Year last updated:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['mainpanellastupdated'];
                                               } else if (isset($_POST['mainpanellastupdated'])) {
                                                   echo $_POST['mainpanellastupdated'];
                                               } ?>" maxlength="255" name="mainpanellastupdated">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Brand/Model:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['mainpanelbrand'];
                                               } else if (isset($_POST['mainpanelbrand'])) {
                                                   echo $_POST['mainpanelbrand'];
                                               } ?>" maxlength="255" name="mainpanelbrand">
                                    </div>
                                </div>
                                <div class="col-md-4 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Second Panel</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Panel age:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['secondpanelage'];
                                               } else if (isset($_POST['secondpanelage'])) {
                                                   echo $_POST['secondpanelage'];
                                               } ?>" maxlength="255" name="secondpanelage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Year last updated:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['secondpanellastupdated'];
                                               } else if (isset($_POST['secondpanellastupdated'])) {
                                                   echo $_POST['secondpanellastupdated'];
                                               } ?>" maxlength="255" name="secondpanellastupdated">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Brand/Model:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['secondpanelbrand'];
                                               } else if (isset($_POST['secondpanelbrand'])) {
                                                   echo $_POST['secondpanelbrand'];
                                               } ?>" maxlength="255" name="secondpanelbrand">
                                    </div>
                                </div>
                                <div class="col-md-4 p-3">
                                    <h5 class="font-weight-600">Wiring Type</h5>
                                    <label class="p-1 px-2 mb-2"><input type="checkbox" name="wiringtype"
                                                                        value="Copper" <?php if ($reqq->rowCount() > 0) {
                                            echo ($r['wiringtype'] == 'Copper') ? 'checked' : '';
                                        } ?>> Copper</label>
                                    <br>
                                    <label class="p-1 px-2 mb-2"><input type="checkbox" name="wiringtype"
                                                                        value="NM, BX or Condult" <?php if ($reqq->rowCount() > 0) {
                                            echo ($r['wiringtype'] == 'NM, BX or Condult') ? 'checked' : '';
                                        } ?>> NM, BX or Condult</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="second-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group mt-4 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">HVAC System</h4>
                        </div>
                        <div class="col-12">
                            <div class="row border-bottom border-secondary">
                                <div class="col-12 p-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Central AC:</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox" name="hvaccentralac"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvaccentralac'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox" name="hvaccentralac"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvaccentralac'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Central heat:</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox" name="hvaccentralheat"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvaccentralheat'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox" name="hvaccentralheat"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvaccentralheat'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">If not central heat, indicate <b>&nbsp; primary &nbsp;</b> heat source and fuel type:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['hvacprimaryheatsource'];
                                               } else if (isset($_POST['hvacprimaryheatsource'])) {
                                                   echo $_POST['hvacprimaryheatsource'];
                                               } ?>" maxlength="255" name="hvacprimaryheatsource">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">Are the heating, ventilation and air conditioning systems in good working order?</span>
                                            </div>
                                            <label class="border form-control"><input type="checkbox"
                                                                                      name="hvacsystemsworking"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvacsystemsworking'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border form-control"><input type="checkbox"
                                                                                      name="hvacsystemsworking"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvacsystemsworking'] == 'No') ? 'checked' : '';
                                                } ?>> No (explain)</label>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['hvacsystemsworkingexplain'];
                                               } else if (isset($_POST['hvacsystemsworkingexplain'])) {
                                                   echo $_POST['hvacsystemsworkingexplain'];
                                               } ?>" maxlength="255" name="hvacsystemsworkingexplain">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last HVAC sevicing / inspection:</span>
                                        </div>
                                        <input class="form-control" type="date"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['dateoflasthvacservice'];
                                               } else if (isset($_POST['dateoflasthvacservice'])) {
                                                   echo $_POST['dateoflasthvacservice'];
                                               } ?>" maxlength="255" name="dateoflasthvacservice">
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom border-secondary">
                                <div class="col-12 p-3">
                                    <div class="form-group mb-0">
                                        <h5 class="font-weight-600 mb-3">Hazards Present</h5>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Wood-burning stove or central gas fireplace not professionally installed?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsstove"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsstove'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsstove"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsstove'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Space heater used as primary heat source?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsheater"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsheater'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsheater"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsheater'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Is the source portable?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsportable"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsportable'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsportable"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsportable'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Does the air handler/condensate line or drain pan show any signs of blockage or leakage, including water damage to the surrounding area?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsleakage"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsleakage'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="hvachazardsleakage"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['hvachazardsleakage'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="p-3 bg-secondary col-12 text-black">
                                    <h4 class="font-weight-600 mb-0">Supplemental Information</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 p-3 pt-4">
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Age of System:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['hvacageofsystem'];
                                               } else if (isset($_POST['hvacageofsystem'])) {
                                                   echo $_POST['hvacageofsystem'];
                                               } ?>" maxlength="255" name="hvacageofsystem">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Year last updated:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['hvaclastupdated'];
                                               } else if (isset($_POST['hvaclastupdated'])) {
                                                   echo $_POST['hvaclastupdated'];
                                               } ?>" maxlength="255" name="hvaclastupdated">
                                    </div>
                                    <h6>(Please attach photo(s) of HVAC equipment, including dated manufacturer's
                                        plate)</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Plumbing System</h4>
                        </div>
                        <div class="col-12">
                            <div class="row border-bottom border-secondary">
                                <div class="col-12 p-3 border-right border-secondary">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Is there a temperature pressure relifed valve on the water heater?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="plumbingpressure"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingpressure'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="plumbingpressure"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingpressure'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Is there any indication of an active leak?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox" name="plumbingleak"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingleak'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox" name="plumbingleak"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingleak'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Is there any indication of prior leak?</span>
                                            </div>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="plumbingpriorleak"
                                                                                  value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingpriorleak'] == 'Yes') ? 'checked' : '';
                                                } ?>> Yes</label>
                                            <label class="border p-2 px-4"><input type="checkbox"
                                                                                  name="plumbingpriorleak"
                                                                                  value="No" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['plumbingpriorleak'] == 'No') ? 'checked' : '';
                                                } ?>> No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row border-bottom border-secondary">
                                <div class="col-12 p-3">
                                    <div class="form-group mb-0">
                                        <h5 class="font-weight-600 mb-3">General Condition of the Following Plumbing
                                            Fixtures and connection to appliances:</h5>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table text-center">
                                                    <tr>
                                                        <th></th>
                                                        <th>Satisfactory</th>
                                                        <th>Unsatisfactory</th>
                                                        <th>N/A</th>
                                                    </tr>
                                                    <?php
                                                    $generalconditionarr = ['Dishwasher', 'Refrigerator', 'Washing machine', 'Water heater', 'Showers/Tubs'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-left"><?php echo $generalconditionarr[$gcc]; ?></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . str_replace('/', '', str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))))] == 'Satisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])))] == 'Unsatisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="N/A" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])))] == 'N/A') ? 'checked' : '';
                                                                } ?>></td>
                                                        </tr>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table text-center">
                                                    <tr>
                                                        <th></th>
                                                        <th>Satisfactory</th>
                                                        <th>Unsatisfactory</th>
                                                        <th>N/A</th>
                                                    </tr>
                                                    <?php
                                                    $generalconditionarr = ['Toilets', 'Sinks', 'Sump pump', 'Main shut off walve', 'All other visible'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-left"><?php echo $generalconditionarr[$gcc]; ?></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'Satisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'Unsatisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="plumbing<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="N/A" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['plumbing' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'N/A') ? 'checked' : '';
                                                                } ?>></td>
                                                        </tr>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="p-3 bg-secondary col-12 text-black">
                                    <h4 class="font-weight-600 mb-0">Supplemental Information</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Age of Piping System:</h5>
                                    <?php
                                    $generalconditionarr = ['Original to home', 'Completely re-piped', 'Partially re-piped'];
                                    $gcc = 0;
                                    while ($gcc != count($generalconditionarr)) {
                                        ?>
                                        <label class="p-1 px-2 mb-2"><input type="checkbox" name="plumbingageofststem"
                                                                            value="<?php echo $generalconditionarr[$gcc]; ?>"> <?php echo $generalconditionarr[$gcc]; ?>
                                        </label>
                                        <br>
                                        <?php
                                        $gcc++;
                                    }
                                    ?>
                                    <br>
                                    <div class="form-group">
                                        <label>(Provide year and extent of renovation in the comments below)</label>
                                        <textarea class="form-control" name="plumbingrevonation"
                                                  rows="4"><?php if ($reqq->rowCount() > 0) {
                                                echo $r['plumbingrevonation'];
                                            } ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 p-3">
                                    <h5 class="font-weight-600">Type of pipes (check all that apply)</h5>
                                    <?php
                                    $generalconditionarr = ['Copper', 'NM, BX or Condult', 'Galvanized', 'PEX', 'Polybutylene'];
                                    $gcc = 0;
                                    while ($gcc != count($generalconditionarr)) {
                                        ?>
                                        <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                            name="plumbingtypeofpipescheck"
                                                                            value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                echo ((strpos($r['plumbingtypeofpipes'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                            } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                        <br>
                                        <?php
                                        $gcc++;
                                    }
                                    ?>
                                    <label class="p-1 px-2 mb-2"><input type="checkbox" name="plumbingtypeofpipescheck"
                                                                        value="Other" <?php if ($reqq->rowCount() > 0) {
                                            echo ((strpos($r['plumbingtypeofpipes'], 'Other') !== false)) ? 'checked' : '';
                                        } ?>> Other (specify)</label>
                                    <br>
                                    <input type="hidden" name="plumbingtypeofpipes"
                                           value="<?php if ($reqq->rowCount() > 0) {
                                               echo $r['plumbingtypeofpipes'];
                                           } ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="third-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group mt-4 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Roof <small>(with photos of each roof slope, this section
                                    can take the place of the Roof Inspection Form.)</small></h4>
                        </div>
                        <div class="col-12">
                            <div class="row border-bottom border-secondary">
                                <div class="col-md-6 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Predominant Roof</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Covering Material:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofcoveringmaterial'];
                                               } else if (isset($_POST['roofcoveringmaterial'])) {
                                                   echo $_POST['roofcoveringmaterial'];
                                               } ?>" maxlength="255" name="roofcoveringmaterial">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Roof Age (Years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofage'];
                                               } else if (isset($_POST['roofage'])) {
                                                   echo $_POST['roofage'];
                                               } ?>" maxlength="255" name="roofage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Remaining useful life (years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofusefullife'];
                                               } else if (isset($_POST['roofusefullife'])) {
                                                   echo $_POST['roofusefullife'];
                                               } ?>" maxlength="255" name="roofusefullife">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last roofing permit:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['dateoflastroofing'];
                                               } else if (isset($_POST['dateoflastroofing'])) {
                                                   echo $_POST['dateoflastroofing'];
                                               } ?>" maxlength="255" name="dateoflastroofing">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last update:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['rooflastupdate'];
                                               } else if (isset($_POST['rooflastupdate'])) {
                                                   echo $_POST['rooflastupdate'];
                                               } ?>" maxlength="255" name="rooflastupdate">
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">If updated (check one):</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofreplacementtype"
                                                                                      value="Full replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofreplacementtype'] == 'Full replacement') ? 'checked' : '';
                                                    } ?>> Full replacement</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofreplacementtype"
                                                                                      value="Partial replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofreplacementtype'] == 'Partial replacement') ? 'checked' : '';
                                                    } ?>> Partial replacement</label>
                                            </div>
                                            <div class="form-group input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">% of replacement:</span>
                                                </div>
                                                <input class="form-control" type="text"
                                                       value="<?php if ($reqq->rowCount() > 0) {
                                                           echo $r['roofreplacementprcntg'];
                                                       } else if (isset($_POST['roofreplacementprcntg'])) {
                                                           echo $_POST['roofreplacementprcntg'];
                                                       } ?>" maxlength="255" name="roofreplacementprcntg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">Overall condition:</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofoverallcondition"
                                                                                      value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofoverallcondition'] == 'Satisfactory') ? 'checked' : '';
                                                    } ?>> Satisfactory</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofoverallcondition"
                                                                                      value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofoverallcondition'] == 'Unsatisfactory') ? 'checked' : '';
                                                    } ?>> Unsatisfactory</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5 class="font-weight-600">Any visible signs of damage / deterioration?</h5>
                                        <h6 class="font-weight-400 font-95">(check all that apply and explain
                                            below)</h6>
                                        <?php
                                        $generalconditionarr = ['Cracking', 'Cupping/curling', 'Excessive granule loss', 'Exposed asphalt', 'Exposed felt', 'Missing/loose/cracked tabs or tiles', 'Soft spots in decking', 'Visible hail damage'];
                                        $gcc = 0;
                                        while ($gcc != count($generalconditionarr)) {
                                            ?>
                                            <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                name="roofvisibledamagecheck"
                                                                                value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                    echo ((strpos($r['roofvisibledamage'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                            <br>
                                            <?php
                                            $gcc++;
                                        }
                                        ?>
                                        <input type="hidden" name="roofvisibledamage"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofvisibledamage'];
                                               } ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Any visible signs of leaks</b>:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofvisibleleaks"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofvisibleleaks'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofvisibleleaks"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofvisibleleaks'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Attic/underside of decking:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox" name="roofdecking"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofdecking'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox" name="roofdecking"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofdecking'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Interior ceilings:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofceilings"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofceilings'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofceilings"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofceilings'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-3">
                                    <h5 class="font-weight-600">Secondary Roof</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Covering Material:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofcoveringmaterial'];
                                               } else if (isset($_POST['sroofcoveringmaterial'])) {
                                                   echo $_POST['sroofcoveringmaterial'];
                                               } ?>" maxlength="255" name="sroofcoveringmaterial">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Roof Age (Years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofage'];
                                               } else if (isset($_POST['sroofage'])) {
                                                   echo $_POST['sroofage'];
                                               } ?>" maxlength="255" name="sroofage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Remaining useful life (years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofusefullife'];
                                               } else if (isset($_POST['sroofusefullife'])) {
                                                   echo $_POST['sroofusefullife'];
                                               } ?>" maxlength="255" name="sroofusefullife">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last roofing permit:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sdateoflastroofing'];
                                               } else if (isset($_POST['sdateoflastroofing'])) {
                                                   echo $_POST['sdateoflastroofing'];
                                               } ?>" maxlength="255" name="sdateoflastroofing">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last update:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['srooflastupdate'];
                                               } else if (isset($_POST['srooflastupdate'])) {
                                                   echo $_POST['srooflastupdate'];
                                               } ?>" maxlength="255" name="srooflastupdate">
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">If updated (check one):</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofreplacementtype"
                                                                                      value="Full replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofreplacementtype'] == 'Full replacement') ? 'checked' : '';
                                                    } ?>> Full replacement</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofreplacementtype"
                                                                                      value="Partial replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofreplacementtype'] == 'Partial replacement') ? 'checked' : '';
                                                    } ?>> Partial replacement</label>
                                            </div>
                                            <div class="form-group input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">% of replacement:</span>
                                                </div>
                                                <input class="form-control" type="text"
                                                       value="<?php if ($reqq->rowCount() > 0) {
                                                           echo $r['sroofreplacementprcntg'];
                                                       } else if (isset($_POST['sroofreplacementprcntg'])) {
                                                           echo $_POST['sroofreplacementprcntg'];
                                                       } ?>" maxlength="255" name="sroofreplacementprcntg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">Overall condition:</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofoverallcondition"
                                                                                      value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofoverallcondition'] == 'Satisfactory') ? 'checked' : '';
                                                    } ?>> Satisfactory</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofoverallcondition"
                                                                                      value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofoverallcondition'] == 'Unsatisfactory') ? 'checked' : '';
                                                    } ?>> Unsatisfactory</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5 class="font-weight-600">Any visible signs of damage / deterioration?</h5>
                                        <h6 class="font-weight-400 font-95">(check all that apply and explain
                                            below)</h6>
                                        <?php
                                        $generalconditionarr = ['Cracking', 'Cupping/curling', 'Excessive granule loss', 'Exposed asphalt', 'Exposed felt', 'Missing/loose/cracked tabs or tiles', 'Soft spots in decking', 'Visible hail damage'];
                                        $gcc = 0;
                                        while ($gcc != count($generalconditionarr)) {
                                            ?>
                                            <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                name="sroofvisibledamagecheck"
                                                                                value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                    echo ((strpos($r['sroofvisibledamage'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                            <br>
                                            <?php
                                            $gcc++;
                                        }
                                        ?>
                                        <input type="hidden" name="sroofvisibledamage"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofvisibledamage'];
                                               } ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Any visible signs of leaks</b>:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofvisibleleaks"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofvisibleleaks'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofvisibleleaks"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofvisibleleaks'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Attic/underside of decking:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofdecking"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofdecking'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofdecking"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofdecking'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Interior ceilings:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofceilings"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofceilings'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofceilings"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofceilings'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 p-3">
                                    <div class="form-group mb-0">
                                        <h5 class="font-weight-600 mb-3">General Condition of the Following Plumbing
                                            Fixtures and connection to appliances:</h5>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table text-center">
                                                    <tr>
                                                        <th></th>
                                                        <th>Satisfactory</th>
                                                        <th>Unsatisfactory</th>
                                                        <th>N/A</th>
                                                    </tr>
                                                    <?php
                                                    $generalconditionarr = ['Dishwasher', 'Refrigerator', 'Washing machine', 'Water heater', 'Showers/Tubs'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-left"><?php echo $generalconditionarr[$gcc]; ?></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . str_replace('/', '', str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))))] == 'Satisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])))] == 'Unsatisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))); ?>"
                                                                       value="N/A" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . str_replace('/', '', strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])))] == 'N/A') ? 'checked' : '';
                                                                } ?>></td>
                                                        </tr>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table text-center">
                                                    <tr>
                                                        <th></th>
                                                        <th>Satisfactory</th>
                                                        <th>Unsatisfactory</th>
                                                        <th>N/A</th>
                                                    </tr>
                                                    <?php
                                                    $generalconditionarr = ['Toilets', 'Sinks', 'Sump pump', 'Main shut off walve', 'All other visible'];
                                                    $gcc = 0;
                                                    while ($gcc != count($generalconditionarr)) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-left"><?php echo $generalconditionarr[$gcc]; ?></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'Satisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'Unsatisfactory') ? 'checked' : '';
                                                                } ?>></td>
                                                            <td><input type="radio"
                                                                       name="roof<?php echo strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc])); ?>"
                                                                       value="N/A" <?php if ($reqq->rowCount() > 0) {
                                                                    echo ($r['roof' . strtolower(preg_replace('/\s*/', '', $generalconditionarr[$gcc]))] == 'N/A') ? 'checked' : '';
                                                                } ?>></td>
                                                        </tr>
                                                        <?php
                                                        $gcc++;
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Additional Comments/Observations <small>(use additional
                                    pages if needed)</small></h4>
                        </div>
                        <div class="col-12 border-bottom border-secondary mt-3">
                            <div class="form-group">
                                <textarea class="form-control" rows="5"
                                          name="additionalcomments"><?php if ($reqq->rowCount() > 0) {
                                        echo $r['additionalcomments'];
                                    } ?></textarea>
                            </div>
                        </div>
                        <div class="col-12 border-bottom border-secondary mt-3">
                            <div class="form-group">
                                <h6>All 4-Point Inspection Forms must be completed and signed by a verifiable
                                    Florida-licensed inspector.</h6>
                                <h6>I certify that the above statement are true and correct.</h6>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="inspectorsignature"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['inspectorsignature'];
                                               } ?>">
                                        <label>Inspector Signature</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="title"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['title'];
                                               } ?>">
                                        <label>Title</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="licensenumber"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['licensenumber'];
                                               } ?>">
                                        <label>License Number</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="date"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['date'];
                                               } ?>">
                                        <label>Date</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="companyname"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['companyname'];
                                               } ?>">
                                        <label>Company Name</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="licensetype"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['licensetype'];
                                               } ?>">
                                        <label>License Type</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="workphone"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['workphone'];
                                               } ?>">
                                        <label>Work Phone</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="forth-page" class="nav justify-content-between col-12 p-0">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-4 nav justify-content-start images">
                    <?php
                    $doctypearr = ['Elevations', 'Roof', 'Ait Conditoning and Heating', 'Plumbing', 'Electrical'];
                    $ic = 1;
                    $ict = 0;
                    $icn = 0;
                    $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                    $class = '';
                    while ($ict < count($doctypearr)) {
                        ?>
                        <div class="col-12 my-4">
                            <h4 class="mb-0"><?php echo $doctypearr[$ict]; ?></h4>
                        </div>
                        <?php
                        $ic = 0;
                        while ($ic != 6) {
                            if ($reqq->rowCount() > 0) {
                                $alreadyq = $a->con->prepare("select * from 4pointfiles where reqid = ? and imgno = ?");
                                $alreadyq->execute([$_GET['reqid'], $icn]);
                                if ($alreadyq->rowCount() > 0) {
                                    $already = $alreadyq->fetch();
                                    $thumbnail = $already['file'];
                                    $class = 'active';
                                    $imgn = $already['name'];
                                } else {
                                    $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                                    $class = '';
                                    $imgn = $doctypearr[$ict];
                                }
                            } else {
                                $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                                $class = '';
                                $imgn = $doctypearr[$ict];
                            }
                            if (isset($_GET['download'])) {
                                if ($alreadyq->rowCount() == 0) {
                                    $show = 0;
                                } else {
                                    $show = 1;
                                }
                            } else {
                                $show = 1;
                            }
                            if ($show == 1) {
                                ?>
                                <div class="col-4 mb-4">
                                    <div class="file-upload-img image-box<?php echo $icn; ?> <?php echo $class; ?>">
                                        <div class="file-select">
                                            <?php if (!(isset($_GET['download']))) { ?>
                                                <div class="nav justify-content-between">
                                                    <div class="file-select-button" id="fileName">
                                                        <div class="nav justify-content-between">
                                                            <div>Choose File</div>
                                                            <div class="mt-0 remove-extra"
                                                                 style="<?php if ($class == '') {
                                                                     echo 'display:none;';
                                                                 } ?> position:absolute; z-index:999;right:10px;"
                                                                 id="<?php echo $icn; ?>"><i
                                                                        class="fas fa-times text-light"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="file-select-name" id="noFile"><img class="col-12 p-0"
                                                                                           src="<?php echo $thumbnail; ?>">
                                            </div>
                                            <input type="file" name="img<?php echo $icn; ?>" accept="image/*">
                                        </div>
                                    </div>
                                    <input class="form-control rounded-0 text-center mt-2"
                                           name="imagename<?php echo $icn; ?>" value="<?php echo $imgn; ?>">
                                </div>
                                <?php
                            }
                            $icn++;
                            $ic++;
                        }
                        $ict++;
                    }
                    ?>
                </div>
            </div>
            <?php if (!(isset($_GET['download']))) { ?>
                <div class="col-lg-12 col-md-12 text-center col-sm-12 mt-5 nav justify-content-center">
                    <?php if ($reqq->rowCount() > 0) {
                        ?>
                        <button type="submit" name="update"
                                class="btn btn-dark col-md-6 px-3 btn-update border shadow bg-main">Update
                        </button>
                        <?php
                    } else { ?>
                        <button type="submit" name="submit"
                                class="btn btn-dark col-md-6 px-4 btn-submit border shadow bg-main">Submit Form
                        </button>
                    <?php } ?>
                    <?php
                    if ($reqq->rowCount() > 0) {
                        ?>
                        <a href="assigned-requests.php" class="btn btn-sm col-md-6 btn-light text-danger border-danger">Cancel</a>
                        <?php
                    }
                    ?>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
</body>

</html>
<script>
    $('input[type="checkbox"]').change(function () {
        var namex = $(this).attr('name');
        var name = namex.slice(0, -5);
        if ($(this).prop("checked") == true) {
            var catg = $(this).val();
            if ($("input[name='" + name + "']").val().includes(catg) == true) {
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val());
            } else {
                if ($("input[name='" + name + "']").val() == '') {
                    $("input[name='" + name + "']").val($("input[name='" + name + "']").val() + catg);
                } else {
                    $("input[name='" + name + "']").val($("input[name='" + name + "']").val() + ' - ' + catg);
                }
            }
        } else if ($(this).prop("checked") == false) {
            var catg = $(this).val();
            if ($("input[name='" + name + "']").val().includes(catg) == true) {
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(' - ' + catg, ''));
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(catg + ' - ', ''));
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(catg, ''));
            }
        }
    });

</script>
<?php
if (isset($_POST['submit'])) {
    $reqid = $_GET['reqid'];
    $query = $a->con->prepare("insert into 4pointform(user, reqid, applicantname, policyno, addressinspected, actualyearbuilt, dateinspected, photorequirement, mainpaneltype, mainpaneltotalamps, mainpanelsufficiant, mainpanelsufficiantexplain, secondpaneltype, secondpaneltotalamps, secondpanelsufficiant, secondpanelsufficiantexplain, indicatepresense, hazardspresentother, hazardspresent, electricalsystemcondition, electricalsystemconditionexplain, mainpanelage, mainpanellastupdated, mainpanelbrand, secondpanelage, secondpanellastupdated, secondpanelbrand, wiringtype, hvaccentralac, hvaccentralheat, hvacprimaryheatsource, hvacsystemsworking, hvacsystemsworkingexplain, dateoflasthvacservice, hvachazardsstove, hvachazardsheater, hvachazardsportable, hvachazardsleakage, hvacageofsystem, hvaclastupdated, plumbingpressure, plumbingleak, plumbingpriorleak, plumbingdishwasher, plumbingrefrigerator, plumbingwashingmachine, plumbingwaterheater, plumbingshowerstubs, plumbingtoilets, plumbingsinks, plumbingsumppump, plumbingmainshutoffwalve, plumbingallothervisible, plumbingageofststem, plumbingrevonation, plumbingtypeofpipes, roofcoveringmaterial, roofage, roofusefullife, dateoflastroofing, rooflastupdate, roofreplacementtype, roofreplacementprcntg, roofoverallcondition, roofvisibledamage, roofvisibleleaks, roofdecking, roofceilings, sroofcoveringmaterial, sroofage, sroofusefullife, sdateoflastroofing, srooflastupdate, sroofreplacementtype, sroofreplacementprcntg, sroofoverallcondition, sroofvisibledamage, sroofvisibleleaks, sroofdecking, sroofceilings, roofdishwasher, roofrefrigerator, roofwashingmachine, roofwaterheater, roofshowerstubs, rooftoilets, roofsinks, roofsumppump, roofmainshutoffwalve, roofallothervisible, additionalcomments, inspectorsignature, title, licensenumber, date, companyname, licensetype, workphone) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $status = $query->execute([$_SESSION['user'], $reqid, $_POST['applicantname'], $_POST['policyno'], $_POST['addressinspected'], $_POST['actualyearbuilt'], $_POST['dateinspected'], $_POST['photorequirement'], $_POST['mainpaneltype'], $_POST['mainpaneltotalamps'], $_POST['mainpanelsufficiant'], $_POST['mainpanelsufficiantexplain'], $_POST['secondpaneltype'], $_POST['secondpaneltotalamps'], $_POST['secondpanelsufficiant'], $_POST['secondpanelsufficiantexplain'], $_POST['indicatepresense'], $_POST['hazardspresentother'], $_POST['hazardspresent'], $_POST['electricalsystemcondition'], $_POST['electricalsystemconditionexplain'], $_POST['mainpanelage'], $_POST['mainpanellastupdated'], $_POST['mainpanelbrand'], $_POST['secondpanelage'], $_POST['secondpanellastupdated'], $_POST['secondpanelbrand'], $_POST['wiringtype'], $_POST['hvaccentralac'], $_POST['hvaccentralheat'], $_POST['hvacprimaryheatsource'], $_POST['hvacsystemsworking'], $_POST['hvacsystemsworkingexplain'], $_POST['dateoflasthvacservice'], $_POST['hvachazardsstove'], $_POST['hvachazardsheater'], $_POST['hvachazardsportable'], $_POST['hvachazardsleakage'], $_POST['hvacageofsystem'], $_POST['hvaclastupdated'], $_POST['plumbingpressure'], $_POST['plumbingleak'], $_POST['plumbingpriorleak'], $_POST['plumbingdishwasher'], $_POST['plumbingrefrigerator'], $_POST['plumbingwashingmachine'], $_POST['plumbingwaterheater'], $_POST['plumbingshowerstubs'], $_POST['plumbingtoilets'], $_POST['plumbingsinks'], $_POST['plumbingsumppump'], $_POST['plumbingmainshutoffwalve'], $_POST['plumbingallothervisible'], $_POST['plumbingageofststem'], $_POST['plumbingrevonation'], $_POST['plumbingtypeofpipes'], $_POST['roofcoveringmaterial'], $_POST['roofage'], $_POST['roofusefullife'], $_POST['dateoflastroofing'], $_POST['rooflastupdate'], $_POST['roofreplacementtype'], $_POST['roofreplacementprcntg'], $_POST['roofoverallcondition'], $_POST['roofvisibledamage'], $_POST['roofvisibleleaks'], $_POST['roofdecking'], $_POST['roofceilings'], $_POST['sroofcoveringmaterial'], $_POST['sroofage'], $_POST['sroofusefullife'], $_POST['sdateoflastroofing'], $_POST['srooflastupdate'], $_POST['sroofreplacementtype'], $_POST['sroofreplacementprcntg'], $_POST['sroofoverallcondition'], $_POST['sroofvisibledamage'], $_POST['sroofvisibleleaks'], $_POST['sroofdecking'], $_POST['sroofceilings'], $_POST['roofdishwasher'], $_POST['roofrefrigerator'], $_POST['roofwashingmachine'], $_POST['roofwaterheater'], $_POST['roofshowerstubs'], $_POST['rooftoilets'], $_POST['roofsinks'], $_POST['roofsumppump'], $_POST['roofmainshutoffwalve'], $_POST['roofallothervisible'], $_POST['additionalcomments'], $_POST['inspectorsignature'], $_POST['title'], $_POST['licensenumber'], $_POST['date'], $_POST['companyname'], $_POST['licensetype'], $_POST['workphone']]);
    if ($status) {
        $doctypearr = ['Elevations', 'Roof', 'Ait Conditoning and Heating', 'Plumbing', 'Electrical'];
        $icn = 0;
        $class = '';
        while ($icn < (count($doctypearr) * 4)) {
            if ($_POST['imagename' . $icn] != '') {
                $img = $_FILES['img' . $icn]['tmp_name'];
                $pic = $_FILES['img' . $icn]['name'];
                if ($pic != '') {
                    $ext = pathinfo($pic, PATHINFO_EXTENSION);
                    $path = 'assets/images/4pointform/' . sha1($pic . date("Y-m-d H:i:s")) . '.' . $ext;
                    $addimageq = $a->con->prepare("insert into 4pointfiles(reqid,imgno,name,file,user) values(?,?,?,?,?)");
                    $addimageq->execute([$reqid, $icn, $_POST['imagename' . $icn], $path, $_SESSION['user']]);
                    copy($img, $path);
                }
            }
            $icn++;
        }

        $_SESSION['form'] = 'added';
        echo '<script> window.location = "4-point-form.php?reqid=' . $_GET['reqid'] . '&x=' . $_GET['reqid'] . '&download=pdf"; </script>';
    }
}
if (isset($_POST['update'])) {
    $reqid = $_GET['reqid'];
    $query = $a->con->prepare("update 4pointform set applicantname=?, policyno=?, addressinspected=?, actualyearbuilt=?, dateinspected=?, photorequirement=?, mainpaneltype=?, mainpaneltotalamps=?, mainpanelsufficiant=?, mainpanelsufficiantexplain=?, secondpaneltype=?, secondpaneltotalamps=?, secondpanelsufficiant=?, secondpanelsufficiantexplain=?, indicatepresense=?, hazardspresentother=?, hazardspresent=?, electricalsystemcondition=?, electricalsystemconditionexplain=?, mainpanelage=?, mainpanellastupdated=?, mainpanelbrand=?, secondpanelage=?, secondpanellastupdated=?, secondpanelbrand=?, wiringtype=?, hvaccentralac=?, hvaccentralheat=?, hvacprimaryheatsource=?, hvacsystemsworking=?, hvacsystemsworkingexplain=?, dateoflasthvacservice=?, hvachazardsstove=?, hvachazardsheater=?, hvachazardsportable=?, hvachazardsleakage=?, hvacageofsystem=?, hvaclastupdated=?, plumbingpressure=?, plumbingleak=?, plumbingpriorleak=?, plumbingdishwasher=?, plumbingrefrigerator=?, plumbingwashingmachine=?, plumbingwaterheater=?, plumbingshowerstubs=?, plumbingtoilets=?, plumbingsinks=?, plumbingsumppump=?, plumbingmainshutoffwalve=?, plumbingallothervisible=?, plumbingageofststem=?, plumbingrevonation=?, plumbingtypeofpipes=?, roofcoveringmaterial=?, roofage=?, roofusefullife=?, dateoflastroofing=?, rooflastupdate=?, roofreplacementtype=?, roofreplacementprcntg=?, roofoverallcondition=?, roofvisibledamage=?, roofvisibleleaks=?, roofdecking=?, roofceilings=?, sroofcoveringmaterial=?, sroofage=?, sroofusefullife=?, sdateoflastroofing=?, srooflastupdate=?, sroofreplacementtype=?, sroofreplacementprcntg=?, sroofoverallcondition=?, sroofvisibledamage=?, sroofvisibleleaks=?, sroofdecking=?, sroofceilings=?, roofdishwasher=?, roofrefrigerator=?, roofwashingmachine=?, roofwaterheater=?, roofshowerstubs=?, rooftoilets=?, roofsinks=?, roofsumppump=?, roofmainshutoffwalve=?, roofallothervisible=?, additionalcomments=?, inspectorsignature=?, title=?, licensenumber=?, date=?, companyname=?, licensetype=?, workphone=?,lastupdatedat=?,lastupdatedby=? where reqid = ?");
    $status = $query->execute([$_POST['applicantname'], $_POST['policyno'], $_POST['addressinspected'], $_POST['actualyearbuilt'], $_POST['dateinspected'], $_POST['photorequirement'], $_POST['mainpaneltype'], $_POST['mainpaneltotalamps'], $_POST['mainpanelsufficiant'], $_POST['mainpanelsufficiantexplain'], $_POST['secondpaneltype'], $_POST['secondpaneltotalamps'], $_POST['secondpanelsufficiant'], $_POST['secondpanelsufficiantexplain'], $_POST['indicatepresense'], $_POST['hazardspresentother'], $_POST['hazardspresent'], $_POST['electricalsystemcondition'], $_POST['electricalsystemconditionexplain'], $_POST['mainpanelage'], $_POST['mainpanellastupdated'], $_POST['mainpanelbrand'], $_POST['secondpanelage'], $_POST['secondpanellastupdated'], $_POST['secondpanelbrand'], $_POST['wiringtype'], $_POST['hvaccentralac'], $_POST['hvaccentralheat'], $_POST['hvacprimaryheatsource'], $_POST['hvacsystemsworking'], $_POST['hvacsystemsworkingexplain'], $_POST['dateoflasthvacservice'], $_POST['hvachazardsstove'], $_POST['hvachazardsheater'], $_POST['hvachazardsportable'], $_POST['hvachazardsleakage'], $_POST['hvacageofsystem'], $_POST['hvaclastupdated'], $_POST['plumbingpressure'], $_POST['plumbingleak'], $_POST['plumbingpriorleak'], $_POST['plumbingdishwasher'], $_POST['plumbingrefrigerator'], $_POST['plumbingwashingmachine'], $_POST['plumbingwaterheater'], $_POST['plumbingshowerstubs'], $_POST['plumbingtoilets'], $_POST['plumbingsinks'], $_POST['plumbingsumppump'], $_POST['plumbingmainshutoffwalve'], $_POST['plumbingallothervisible'], $_POST['plumbingageofststem'], $_POST['plumbingrevonation'], $_POST['plumbingtypeofpipes'], $_POST['roofcoveringmaterial'], $_POST['roofage'], $_POST['roofusefullife'], $_POST['dateoflastroofing'], $_POST['rooflastupdate'], $_POST['roofreplacementtype'], $_POST['roofreplacementprcntg'], $_POST['roofoverallcondition'], $_POST['roofvisibledamage'], $_POST['roofvisibleleaks'], $_POST['roofdecking'], $_POST['roofceilings'], $_POST['sroofcoveringmaterial'], $_POST['sroofage'], $_POST['sroofusefullife'], $_POST['sdateoflastroofing'], $_POST['srooflastupdate'], $_POST['sroofreplacementtype'], $_POST['sroofreplacementprcntg'], $_POST['sroofoverallcondition'], $_POST['sroofvisibledamage'], $_POST['sroofvisibleleaks'], $_POST['sroofdecking'], $_POST['sroofceilings'], $_POST['roofdishwasher'], $_POST['roofrefrigerator'], $_POST['roofwashingmachine'], $_POST['roofwaterheater'], $_POST['roofshowerstubs'], $_POST['rooftoilets'], $_POST['roofsinks'], $_POST['roofsumppump'], $_POST['roofmainshutoffwalve'], $_POST['roofallothervisible'], $_POST['additionalcomments'], $_POST['inspectorsignature'], $_POST['title'], $_POST['licensenumber'], $_POST['date'], $_POST['companyname'], $_POST['licensetype'], $_POST['workphone'], date("Y-m-d H:i:s"), $_SESSION['user'], $_GET['reqid']]);
    if ($status) {

        $doctypearr = ['Elevations', 'Roof', 'Ait Conditoning and Heating', 'Plumbing', 'Electrical'];
        $icn = 0;
        $class = '';
        while ($icn < (count($doctypearr) * 4)) {
            if ($_POST['imagename' . $icn] != '') {
                $img = $_FILES['img' . $icn]['tmp_name'];
                $pic = $_FILES['img' . $icn]['name'];
                if ($pic != '') {
                    $ext = pathinfo($pic, PATHINFO_EXTENSION);
                    $path = 'assets/images/4pointform/' . sha1($pic . date("Y-m-d H:i:s")) . '.' . $ext;
                    $addimageq = $a->con->prepare("insert into 4pointfiles(reqid,imgno,name,file,user) values(?,?,?,?,?)");
                    $addimageq->execute([$reqid, $icn, $_POST['imagename' . $icn], $path, $_SESSION['user']]);
                    copy($img, $path);
                }
            }
            $icn++;
        }

        $_SESSION['contact'] = 'updated';
        echo '<script> window.location = "4-point-form.php?reqid=' . $_GET['reqid'] . '&x=' . $_GET['reqid'] . '&download=pdf"; </script>';
    }
}
?>

<script>
    $(".nav.4pointform").addClass('active-link');

    $(".file-upload-img input").change(function (e) {
        var namex = $(this).attr("name");
        var name = namex.slice(3);
        $(".image-box" + name).addClass('active');
        var filename = $(this).val();
        if (/^\s*$/.test(filename)) {
            $(".image-box" + name).removeClass('active');
            $(".image-box" + name + " img").attr('src', 'https://static.thenounproject.com/png/971625-200.png');
        } else {
            $(".image-box" + name).addClass('active');
            var files = e.target.files,
                filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function (e) {
                    var file = e.target;
                    $(".image-box" + name + " img").attr('src', e.target.result);
                    $(".image-box" + name + " .remove-extra").css('display', 'flex');
                });
                fileReader.readAsDataURL(f);
            }
        }
    });

    $(".remove-extra").click(function () {
        var i = $(this).attr('id');
        $(".image-box" + i).removeClass('active');
        $(".image-box" + i + " img").attr('src', 'https://static.thenounproject.com/png/971625-200.png');
        $("#" + i).css('display', 'none');

    });

</script>
<?php if ($reqq->rowCount() > 0 && isset($_GET['download'])) { ?>
    <style>
        .main-border {
            border: 0px !important;
            padding: 0px !important;
        }

        .file-upload-img.active .file-select .file-select-name img {
            height: 260px !important;
            object-fit: contain;
        }

        .file-upload-img .file-select {
            height: 260px !important;
        }

        .images input.form-control {
            height: 40px !important;
            padding: .25rem .55rem !important;
            font-size: 15px !important;
        }

        label {
            color: black !important;
            font-size: 10px;
        }

        label.border {
            color: black !important;
            font-size: 10px;
        }

        input.form-control {
            height: 16px !important;
            padding: .0rem .35rem !important;
        }

        label.form-control {
            padding: .1rem .01rem !important;
        }

        label {
            padding-bottom: 0px !important;
        }

        label.p-2 {
            padding-top: .1rem !important;
            padding-bottom: .1rem !important;
            margin-bottom: 0px !important;
        }

        .p-1 {
            padding: .15rem !important;
        }

        .table td,
        .table th {
            padding: .25rem !important;
        }

        .input-group-prepend {
            height: 16px !important;
        }

        .input-group-text {
            color: black !important;
            font-size: 12px;
        }

        .extra {
            font-size: 85% !important;
            color: black !important;
        }

        input,
        textarea {
            font-size: 12px !important;
        }

        .form-group {
            margin-bottom: 0.3rem !important;
        }

        .col-12.p-3 {
            padding: 0.4rem !important;
        }

        .p-3.bg-secondary.text-black {
            padding: 0.4rem !important;
        }

        label.px-2 {
            margin-bottom: 0px !important;
            font-weight: 400 !important;
            font-size: 11px;
            color: #495057;
        }


        .main-border table {
            font-size: 0.7rem !important;
        }

        .main-border h5 {
            font-size: 0.9rem !important;
        }

        .main-border h4 {
            font-size: 1rem !important;
        }

        .main-border h6,
        .main-border .h6 {
            font-size: 0.74rem;
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js"></script>
    <script type="text/javascript">
        $(".prepare-progress").css('display', 'block');
        filename = '4-Point-Form-<?php echo date("d-m-Y"); ?>.pdf'
        var element = document.getElementById('pdfcontent');
        html2pdf(element, {
            margin: [10, 0, 10, 0], //top, left, buttom, right
            filename: filename,
            image: {
                type: 'jpeg',
                quality: 1
            },
            html2canvas: {
                scale: 2,
                bottom: 20,
                letterRendering: true
            },
            jsPDF: {
                unit: 'mm',
                format: 'letter',
                orientation: 'portrait'
            },
            pagebreak: {
                mode: ['avoid-all', 'css', 'legacy']
            }
        });

    </script>
<?php } ?>
