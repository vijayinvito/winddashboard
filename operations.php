<?php
session_start();
include('includes/config.php');
$con = new PDO($SETTINGS['host'], $SETTINGS['mysql_user'], $SETTINGS['mysql_pass']);
$t = $_POST['t'];
$sitequery = $con->prepare("select * from site");
$sitequery->execute();
$site = $sitequery->fetch();
$sitename = $site['name'];
if ($t == 'newnote') {
    $q = $con->prepare("select * from sphere where contactid = ? and user = ?");
    $q->execute([$_POST['i'], $_SESSION['user']]);
    $r = $q->fetch();
    ?>
<h5 class="text-dark mb-2"><span class="font-90">Name:</span> <span class="font-weight-600"><?php echo $r['name']; ?></span></h5>
<h5 class="text-dark mb-2"><span class="font-90">Status:</span> <span class="font-weight-600"><?php echo $r['status']; ?></span></h5>
<form method="post">
    <input type="hidden" name="contactid" value="<?php echo $r['contactid']; ?>" readonly>
    <textarea class="form-control" required name="note" placeholder="Type your note here..." rows="5"></textarea>
    <div class="nav justify-content-end mt-3">
        <button type="button" class="btn text-danger border-danger btn-light" data-dismiss="modal" aria-label="Close">
            Cancel ✕
        </button>
        &nbsp;&nbsp;
        <button type="submit" name="submitnote" class="btn btn-primary">Submit <i class="fas fa-save fa-sm"></i>
        </button>
    </div>
</form>
    <?php
}

switch ($t) {
    case 'deleteNote':
        $checkq = $con->prepare("select * from spherenotes where id = ? and user = ?");
        $checkq->execute([$_POST['i'], $_SESSION['user']]);
        if ($checkq->rowCount() > 0) {
            $check = $checkq->fetch();
            $q = $con->prepare("delete from spherenotes where id = ?");
            $q->execute([$_POST['i']]);
            echo 'sphere?contact=' . $check['contactid'];
        }
        break;
    case 'cancelRequest':
        $q = $con->prepare("UPDATE requests SET status = ?, canceldate = ?, cancel = ?, cancelby = ? WHERE reqid = ? AND user = ? AND (status = ? OR status = ? OR status = ?)");
        $q->execute(['Cancelled', date("Y-m-d H:i:s"), $_POST['note'], $_SESSION['user'], $_POST['i'], $_SESSION['user'], 'Pending', 'Assigned', 'Scheduled']);
        break;
}
?>
