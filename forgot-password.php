<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('includes/essentials.php'); ?>
    <title>Forgot Password | <?php echo $site['name']; ?> </title>
</head>

<body>
<?php if (isset($_SESSION['user'])) {
    echo "<script> window.location = '/index.php' </script>";
} else { ?>
    <style>
        html,
        body {
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
            background-repeat: no-repeat;
        }

        .login-form label {
            margin-bottom: 0px;
            font-weight: 600;
            font-size: 90%;
            letter-spacing: 0.8px;
            color: #4B5258;
        }

        .login-form input,
        .login-form input:hover {
            color: black;
            background: transparent !important;
            border-bottom: 1px solid #6c757d !important;
            border: 0px;
            border-radius: 0px !important;
        }

        .input-group-text {
            color: black !important;
            background: transparent;
            width: 42px;
            border: 0px;
            text-align: center;
        }

        .form-bottom-border {
            border-bottom: 1px solid #4B5258 !important;
        }

        .form-bottom-border input::placeholder {
            color: #4B5258 !important;
        }

        .form-bottom-border input,
        .form-bottom-border input:-webkit-autofill {
            color: white !important;
            background: transparent !important;
            border: 0px !important;
            border-radius: 0px !important;
        }

    </style>
    <div class="col-12 h-100">
        <div class="container nav justify-content-between p-0 py-5 h-100">
            <div class="col-lg-6 col-md-6 col-sm-12 h-100">
                <div class="text-center py-5 bg-black shadow nav align-items-top h-100">
                    <div class="col-12 p-0">
                        <a href="index.php"><img class="col-10 ml-auto mr-auto p-0" src="assets/images/logo.png"></a>
                        <h3 class="text-light mt-5 mb-4">Quick To Schedule<br>Accurate Same Day Reports</h3>
                        <br><br>
                        <br>
                        <h3 class="text-light">Insurance Agents/Agencies</h3>
                        <h3 class="text-light">Roofing Contractors</h3>
                        <h3 class="text-light">Realtors</h3>
                        <br>
                        <br>
                        <a href="index.php" class="btn bg-main text-white font-weight-500 px-5 mt-2">Login Here</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 h-100">
                <form method="post" class="col-12 p-5 pt-5 pb-5 text-dark rounded bg-white border login-form h-100">
                    <h2 class="mb-0 text-black font-weight-bold" style="letter-spacing:0.8px;">Forgot Password ?</h2>
                    <h6 class="mb-0 text-secondary" style="font-weight:600;">Enter your email below</h6>
                    <hr>
                    <br>
                    <div>
                        <div class="form-group col-md-12 p-0">
                            <label>Email Address</label>
                            <input maxlength="255" name="email" type="email" class="form-control" required>
                        </div>
                        <br>
                        <button type="submit" name="submit" class="col-12 btn bg-main border-dark text-light">Proceed
                        </button>
                    </div>
                    <br>
                    <hr>
                    <h6 class="text-dark">Already have an account ? <a href="index.php"
                                                                       class="font-weight-bold text-dark">Login Here</a>
                    </h6>
                </form>
            </div>
        </div>
    </div>
    <?php
    if (isset($_POST['submit'])) {
        $d = $_POST['email'];
        $result = $a->con->prepare("select * from users where email = ? LIMIT 1");
        $result->execute([$_POST['email']]);
        $token = $result->fetch();
        if ($result->rowCount() > 0) {
            $t = $token['token'];
            $name = $token['name'];
            $c = rand(5, 15);
            $b = rand(25, 35);
            $str = mb_strimwidth($t, $c, $b);
            $r = $a->con->prepare("update users set tp = ? where id = ? and email = ?");
            $r->execute([$str, $token[0], $_POST['email']]);

            $e = $_POST['email'];
            $message = '
Hi ' . $name . '<br>
We’re sorry you forgot your password, but you can reset now by clicking this link to reset immediately.
<br><br>
<a href="https://' . $_SERVER['SERVER_NAME'] . '/reset-password.php?u=' . $d . '&t=' . $str . '">Reset Password</a>
<br><br>
or copy this link and paste in your browser: <a href="https://' . $_SERVER['SERVER_NAME'] . '/reset-password.php?u=' . $d . '&t=' . $str . '">https://' . $_SERVER['SERVER_NAME'] . '/reset-password.php?u=' . $d . '&t=' . $str . '</a>
<br><br>
Thank You<br>
<b>' . $site['name'] . '</b>
';
            $sub = "Reset Password";
            if (mail($e, $sub, $message, $headers)) {
                echo '<script> 

                        setTimeout(function() {
                            swal({
                                title: "Email Sent!",
                                text: "Please check your email.",
                                icon: "success",
                                buttons: false,
                                allowOutsideClick: false
                            });
                        }, 3000);

</script>';
            }
        } else {
            echo '<script> 
                        swal({ 
                            title: "Email Not Registered!",
                            text: "Please check your input.",
                            icon: "error",
                            buttons: false,
                            allowOutsideClick: false
                        });
                        
                      </script>';
        }
    }
    ?>
<?php } ?>
</body>

</html>
