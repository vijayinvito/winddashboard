<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <meta charset="UTF-8">
    <title></title>
    <style>
        body,
        html {
            height: 100%;
        }

    </style>
</head>

<body class="bg-dark">
    <div class="container h-100">
        <div class="row h-100 text-center justify-content-center align-items-center">
            <div class="col-12">
                <code class="text-light">CODED ENTIRELY BY USMAN MALIK<br>
                    Email: usmanmalikweb@gmail.com<br>
                    Email: usmanmalik@developmint.xyz<br>
                </code>
                <br><br>
                <code class="text-light"><a class="text-light" href="http://www.developmint.xyz">WWW.DEVELOPMINT.XYZ</a><br>
                    Email: email@developmint.xyz<br><br><br>
                    September 2021
                </code>
            </div>
        </div>
    </div>
</body>

</html>
