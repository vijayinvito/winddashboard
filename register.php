<?php session_start(); ?>
<?php include("includes/common.class.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <title>Register | <?php echo $site['name']; ?> </title>
</head>

<body>
<?php if (isset($_SESSION['user'])) {
    echo '<script> window.location = "/index.php;" </script>';
} else {
    $common = new common();
    ?>

    <div class="col-12 mb-5">
        <div class="container nav justify-content-between p-0 py-5 h-100">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="text-center py-5 bg-black shadow nav align-items-top h-100">
                    <div class="col-12 p-0">
                        <a href="/index.php"><img class="col-10 ml-auto mr-auto p-0" src="assets/images/logo.png"></a>
                        <h3 class="text-light mt-5 mb-4">Quick To Schedule<br>Accurate Same Day Reports</h3>
                        <br><br>
                        <br>
                        <h3 class="text-light">Insurance Agents/Agencies</h3>
                        <h3 class="text-light">Roofing Contractors</h3>
                        <h3 class="text-light">Realtors</h3>
                        <br>
                        <br>
                        <a href="/index.php" class="btn bg-main text-white font-weight-500 px-5 mt-2">Login Here</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form method="post" class="col-12 px-5 pt-4 pb-5 text-dark rounded bg-white border login-form">
                    <h2 class="mb-0 text-black font-weight-bold" style="letter-spacing:0.8px;">Create an Account</h2>
                    <h6 class="mb-0 text-secondary" style="font-weight:600;">All fields are required</h6>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Company Name</label>
                            <input maxlength="50" name="company" type="text" class="form-control"
                                   value="<?php if (isset($_POST['company'])) {
                                       echo $_POST['company'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Company Address</label>
                            <input maxlength="50" name="address" type="text" class="form-control"
                                   value="<?php if (isset($_POST['address'])) {
                                       echo $_POST['address'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>City</label>
                            <input maxlength="30" name="city" type="text" class="form-control"
                                   value="<?php if (isset($_POST['city'])) {
                                       echo $_POST['city'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Zip Code</label>
                            <input maxlength="30" name="zipcode" type="text" class="form-control"
                                   value="<?php if (isset($_POST['zipcode'])) {
                                       echo $_POST['zipcode'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Company Phone Number</label>
                            <input maxlength="20" name="companyphone" type="tel" class="form-control"
                                   value="<?php if (isset($_POST['companyphone'])) {
                                       echo $_POST['companyphone'];
                                   } ?>" required>
                        </div>
                        <div class="col-12">
                            <br>
                            <hr>
                            <br>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Name</label>
                            <input maxlength="50" name="name" type="text" class="form-control" autofocus
                                   value="<?php if (isset($_POST['name'])) {
                                       echo $_POST['name'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Direct Number</label>
                            <input maxlength="20" name="phone" type="tel" class="form-control"
                                   value="<?php if (isset($_POST['phone'])) {
                                       echo $_POST['phone'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Email Address</label>
                            <input maxlength="255" name="email" type="email" class="form-control"
                                   value="<?php if (isset($_POST['email'])) {
                                       echo $_POST['email'];
                                   } ?>" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Password</label>
                            <input maxlength="30" name="password" type="password" class="form-control"
                                   value="<?php if (isset($_POST['password'])) {
                                       echo $_POST['password'];
                                   } ?>" required>
                            <div class="pt-2 px-0 mb-2 bg-light text-left col-12" style="font-size:90%;"><i
                                        class="fas fa-info-circle"></i> Password must have at least 6 characters
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Confirm Password</label>
                            <input maxlength="30" name="cpassword" type="password" class="form-control"
                                   value="<?php if (isset($_POST['cpassword'])) {
                                       echo $_POST['cpassword'];
                                   } ?>" required>
                        </div>
                        <hr>
                        <p class="text-left col-12 p-0 mt-2 mb-2 mb-0"><a class=" text-dark">By clicking Register, you
                                agree to our <a target="_blank"
                                                href="https://www.windmitigation.network/terms-of-service"
                                                class="text-main font-weight-500"><u>terms of service</u></a> and <a
                                        target="_blank" href="https://www.windmitigation.network/privacy-policy"
                                        class="text-main font-weight-500"><u>privacy policy</u></a>.</a></p>
                        <br>
                        <button type="submit" name="submit" class="col-12 btn bg-main border-dark text-light">Register
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    if (isset($_POST['submit'])) {
        $querycheck = $a->con->prepare("SELECT id FROM users WHERE email = ?");
        $querycheck->execute([$_POST['email']]);
        if ($_POST['password'] == $_POST['cpassword']) {
            if (strlen($_POST['password']) > 5) {


                if (($querycheck->rowCount() > 0)) {
                    echo "<script> swal('Email Already Registered', 'Please Use Any Other Email Address', 'error'); </script>";
                } else {
                    $userid = substr(sha1($_POST['email'] . strtotime("now") . rand(1111, 9999)), 0, 6);
                    $token = sha1($_POST['email'] . strtotime("now") . rand(1111, 9999));
                    $lr = $a->con->prepare("INSERT INTO users(userid,name,company,address,city,zipcode,companyphone, email, password,phone, type, token) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

                    if ($lr->execute([$userid, $_POST['name'], $_POST['company'], $_POST['address'], $_POST['city'], $_POST['zipcode'], $_POST['companyphone'], $_POST['email'], password_hash($_POST['password'], PASSWORD_BCRYPT), $_POST['phone'], 'Agency', $token])) {

                        // Send verification email to registrant.
                        $sub = 'Email Verification | ' . $site['name'] . '';
                        $e = $_POST['email'];
                        $msg = '
Hi ' . $_POST['name'] . ', 
<br><br>
Thank you for registering at ' . $site['name'] . '.<br>
Please click the link to verify your Account: <a href="http://' . $_SERVER['SERVER_NAME'] . '/verify?e=' . $e . '&t=' . $token . '">Verify Account</a>
<br><br>
or copy and paste this link in your browser: http://' . $_SERVER['SERVER_NAME'] . '/verify?e=' . $e . '&t=' . $token . '

<br><br>
Thank You!<br>
<b>' . $site['name'] . '</b>';


                        if (mail($e, $sub, $msg, $headers)) {

                        }

                        // Send notification to administrators.
                        $admin_msg = '
Hello There,
<br><br>
A new agency has signed up at ' . $site['name'] . '.<br><br/>
Their details are as follows:<br />
&nbsp;&nbsp;&nbsp;Company: ' . $_POST['company'] . '<br />
&nbsp;&nbsp;&nbsp;Name: ' . $_POST['name'] . '<br />
&nbsp;&nbsp;&nbsp;Direct Phone: ' . $_POST['phone'] . '<br />
&nbsp;&nbsp;&nbsp;Company Phone: ' . $_POST['companyphone'] . '<br /> 
&nbsp;&nbsp;&nbsp;Email:  ' . $_POST['email'] . '<br />
&nbsp;&nbsp;&nbsp;Address: ' . $_POST['address'] . '<br />
&nbsp;&nbsp;&nbsp;City: ' . $_POST['city'] . '<br />
&nbsp;&nbsp;&nbsp;Zip Code: ' . $_POST['zipcode'] . '<br />
<br><br>
Thank You!<br>
<b>' . $site['name'] . '</b>';

                        $common->sendEmailToGroup('Admin', "New Agency Registered!", $admin_msg, $headers);

                        $_SESSION['register'] = 'successful';
                        echo '<script> swal("Successfully Registered!", "Please Check your Email for Account Verification!", "success"); 
          
          setTimeout(function () {
                 window.location = "/index.php;";
          }, 3000);
          
          </script>';


                    } else {
                        echo "<script> swal('An Error Occured', 'Please try again..', 'warning'); </script>";
                    }
                }
            } else {
                echo "<script> swal('Password should not be less than 6 characters', 'Please enter your password again', 'error'); </script>";
            }
        } else {
            echo "<script> swal('Password Don\'t Match', 'Please enter your password again', 'error'); </script>";
        }
    }
    ?>
<?php } ?>
</body>
</html>
