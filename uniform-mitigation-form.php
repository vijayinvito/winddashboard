<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Inspector' && $user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>Uniform Mitigation Verification Inspection Form | <?php echo $site['name'] ?></title>
    <style>
        #main-page,
        #first-page,
        #second-page,
        #third-page,
        #forth-page {
            background: white;
        }

        .input-group-text {
            background-color: transparent;
            padding: .275rem .55rem;
            border: 0px;
            font-size: 14px;
        }

        label.border {
            background-color: transparent;
            border: 0px !important;
            font-size: 14px;
        }

        input {
            font-size: 15px !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        .images input {
            border: 0px !important;
            border-bottom: 1px solid #ccc !important;
        }

        label.px-2 {
            font-weight: 400 !important;
            font-size: 14px;
            color: #495057;
        }

        .table th {
            vertical-align: middle;
            text-align: center;
            white-space: break-spaces;
        }

        .table td {
            vertical-align: middle;
        }

        .table-custom {
            font-size: 90%;
        }

        .table-custom td,
        .table-custom th {
            padding: .25rem;
        }

    </style>
</head>

<body>
<style>
    .file-upload-img {
        display: block;
        text-align: center;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 12px;
    }

    .file-upload-img .file-select {
        display: block;
        border: 2px solid #dce4ec;
        color: #34495e;
        cursor: pointer;
        height: 240px;
        line-height: 40px;
        text-align: left;
        background: #FFFFFF;
        overflow: hidden;
        position: relative;
    }

    .file-upload-img .file-select .file-select-button {
        background: #dce4ec;
        padding: 0 10px;
        display: inline-block;
        height: 30px;
        line-height: 30px;
        width: 100%;
    }

    .file-upload-img .file-select .file-select-name {
        width: 100%;
        height: auto;
        display: inline-block;
        padding: 50px;
        text-align: center;
    }

    .file-upload-img.active .file-select .file-select-name {
        height: auto;
        display: inline-block;
        padding: 0px;
        text-align: center;
    }

    .file-upload-img .file-select .file-select-name img {
        margin-left: auto;
        margin-right: auto;
        width: auto;
        height: 65px;
        margin: auto;
    }

    .file-upload-img.active .file-select .file-select-name img {
        height: 205px;
        width: 100%;
        margin: auto;
    }

    .file-upload-img .file-select:hover {
        border-color: #34495e;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img .file-select:hover .file-select-button {
        background: #34495e;
        color: #FFFFFF;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img.active .file-select {
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .file-upload-img.active .file-select .file-select-button {
        color: #34495e;
        transition: all .2s ease-in-out;
        -moz-transition: all .2s ease-in-out;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
    }

    .remove-extra i {
        color: #34495e !important;
    }

    .file-upload-img .file-select input[type=file] {
        z-index: 100;
        cursor: pointer;
        position: absolute;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .file-upload-img .file-select.file-select-disabled {
        opacity: 0.65;
    }

    .file-upload-img .file-select.file-select-disabled:hover {
        cursor: default;
        display: block;
        border: 2px solid #dce4ec;
        color: #34495e;
        cursor: pointer;
        height: 40px;
        line-height: 40px;
        margin-top: 5px;
        text-align: left;
        background: #FFFFFF;
        overflow: hidden;
        position: relative;
    }

    .file-upload-img .file-select.file-select-disabled:hover .file-select-button {
        background: #dce4ec;
        color: #666666;
        padding: 0 10px;
        display: inline-block;
        height: 40px;
        line-height: 40px;
    }

    .file-upload-img .file-select.file-select-disabled:hover .file-select-name {
        line-height: 40px;
        display: inline-block;
        padding: 0 10px;
    }

    @media (max-width: 786px) {
        .file-upload-img .file-select {
            height: 120px;
        }

        .file-upload-img .file-select .file-select-name {
            padding: 10px;
        }
    }

</style>
<?php
$reqq = $a->con->prepare("select * from uniformmitigation where reqid = ?");
$reqq->execute([$_GET['reqid']]);
$r = $reqq->fetch();
if (isset($_GET['reqid'])) {
    $requestq = $a->con->prepare("select * from requests where reqid = ?");
    $requestq->execute([$_GET['reqid']]);
    $request = $requestq->fetch();
    if ($request['status'] != 'Assigned' && $reqq->rowCount()) {
        if ($user['type'] == 'Inspector') {
            if (!(isset($_GET['download']))) {
                echo '<script> window.location = "?reqid=' . $_GET['reqid'] . '&download=pdf" </script>';
            }
        }
    }
    if ($request['status'] == 'Completed' && $reqq->rowCount()) {
        if ($user['type'] == 'Admin') {
            if (!(isset($_GET['download']))) {
                echo '<script> window.location = "?reqid=' . $_GET['reqid'] . '&download=pdf" </script>';
            }
        }
    }

} else {
    echo '<script> window.location = "/index.php" </script>';
}
?>
<?php if (isset($_GET['download'])) {
    if ($reqq->rowCount() > 0) { ?>
        <div class="px-4 mb-3 prepare-progress" style="display:none;">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:10%">Preparing PDF
                </div>
            </div>
            <style>
                .progress .progress-bar {
                    animation: preparing 5s forwards;
                }

                @keyframes preparing {
                    from {
                        width: 10%;
                    }

                    to {
                        width: 100%;
                    }
                }

            </style>
        </div>
    <?php }
} ?>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">Uniform Mitigation Verification Inspection Form
    </h4>
    <?php if ($reqq->rowCount() > 0) { ?>
        <div>
            <a class="btn btn-primary" href="?reqid=<?php echo $_GET['reqid']; ?>&download=pdf">Download PDF</a>
        </div>
    <?php } ?>
</div>
<div class="px-4 py-3" id="pdfcontent">
    <?php if (isset($_GET['download'])) {
        if ($reqq->rowCount() > 0) { ?>
            <div class="nav justify-content-between text-center mb-3">
                <div>
                    <img src="assets/images/favicon.jpg" width="50">
                </div>
                <div>
                    <h2 class="text-main font-weight-bold mb-0">Windmitigations.com, LLC</h2>
                    <h3 class="text-black font-weight-bold mb-0">Uniform Mitigation Verification Inspection Form</h3>
                </div>
                <div>
                    <img src="assets/images/favicon.jpg" width="50">
                </div>
            </div>
        <?php }
    } ?>
    <div class="col-12 p-0 p-3 bg-white py-4 border rounded-10 shadow-sm main-border">
        <form method="post" enctype="multipart/form-data" class="nav bg-white pt-3 justify-content-between">
            <div id="main-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group nav justify-content-start">
                    <div class="form-group input-group col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Inspection Date:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['inspectiondate'];
                        } else if (isset($_POST['inspectiondate'])) {
                            echo $_POST['inspectiondate'];
                        } ?>" maxlength="255" name="inspectiondate">
                    </div>
                    <div class="form-group input-group col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Owner Information</b></span>
                        </div>
                    </div>
                    <div class="form-group input-group col-md-8">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Owner Name:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['ownername'];
                        } else if (isset($_POST['ownername'])) {
                            echo $_POST['ownername'];
                        } ?>" maxlength="255" name="ownername">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Contact Person:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['contactperson'];
                        } else if (isset($_POST['contactperson'])) {
                            echo $_POST['contactperson'];
                        } ?>" maxlength="255" name="contactperson">
                    </div>
                    <div class="form-group input-group col-md-8">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Address:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['address'];
                        } else if (isset($_POST['address'])) {
                            echo $_POST['address'];
                        } ?>" maxlength="255" name="address">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Home Phone:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['homephone'];
                        } else if (isset($_POST['homephone'])) {
                            echo $_POST['homephone'];
                        } ?>" maxlength="255" name="homephone">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">City:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['city'];
                        } else if (isset($_POST['city'])) {
                            echo $_POST['city'];
                        } ?>" maxlength="255" name="city">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Zip Code:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['zip'];
                        } else if (isset($_POST['zip'])) {
                            echo $_POST['zip'];
                        } ?>" maxlength="255" name="zip">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Work Phone:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['workphone'];
                        } else if (isset($_POST['workphone'])) {
                            echo $_POST['workphone'];
                        } ?>" maxlength="255" name="workphone">
                    </div>
                    <div class="form-group input-group col-md-8">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Country:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['country'];
                        } else if (isset($_POST['country'])) {
                            echo $_POST['country'];
                        } ?>" maxlength="255" name="country">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cell Phone:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['cellphone'];
                        } else if (isset($_POST['cellphone'])) {
                            echo $_POST['cellphone'];
                        } ?>" maxlength="255" name="cellphone">
                    </div>
                    <div class="form-group input-group col-md-8">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Insurance Company #:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['insurancecompany'];
                        } else if (isset($_POST['insurancecompany'])) {
                            echo $_POST['insurancecompany'];
                        } ?>" maxlength="255" name="insurancecompany">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Policy #:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['policyno'];
                        } else if (isset($_POST['policyno'])) {
                            echo $_POST['policyno'];
                        } ?>" maxlength="255" name="policyno">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Year of Home:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['yearofhome'];
                        } else if (isset($_POST['yearofhome'])) {
                            echo $_POST['yearofhome'];
                        } ?>" maxlength="255" name="yearofhome">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"># of Stories:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['storiesno'];
                        } else if (isset($_POST['storiesno'])) {
                            echo $_POST['storiesno'];
                        } ?>" maxlength="255" name="storiesno">
                    </div>
                    <div class="form-group input-group col-md-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Email:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['email'];
                        } else if (isset($_POST['email'])) {
                            echo $_POST['email'];
                        } ?>" maxlength="255" name="email">
                    </div>
                </div>
                <div class="form-group col-md-12 mt-3">
                    <div class="col-12 px-3 py-2 border h6">NOTE: Any documentation used in validating the compliance or
                        existence of each construction or mitigation attribute must accompany this form. At least one
                        photograph must accompany this form to validate each attribute marked in questions 3 though 7.
                        The insurer may ask additional questions regarding the mitigated feature(s) verified on this
                        form.
                    </div>
                </div>
                <div class="form-group col-md-12 mt-3 h6">
                    <ul style="list-style:number;">
                        <li>
                            <b><u>Building Code:</u></b> Was the structure built in compliance with the Florida Building
                            Code (FBC 2001 or later) OR for homes located in the HVHZ (Miami-Dade or Broward counties),
                            South Florida Building Code (SFBC-94)?
                            <br>
                            <label class="mt-2"><input type="checkbox" name="buildingcodea"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['buildingcodea'] == 'Yes') ? 'checked' : '';
                                } ?>> A. Built in compliance with the FBC: Year Built <input
                                        class="form-control-sm font-90 border" style="width:100px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearbuilt'];
                                        } else if (isset($_POST['bcyearbuilt'])) {
                                            echo $_POST['bcyearbuilt'];
                                        } ?>" maxlength="255" name="bcyearbuilt">. For homes built in 2002/2003 provide
                                a permit application with a date after 3/1/2002: Building Permit Application Date
                                <small>(MM/DD/YYYY)</small> <input class="form-control-sm font-90 border"
                                                                   style="width:50px;" type="text"
                                                                   value="<?php if ($reqq->rowCount() > 0) {
                                                                       echo $r['bcyearpermitdatemonth'];
                                                                   } else if (isset($_POST['bcyearpermitdatemonth'])) {
                                                                       echo $_POST['bcyearpermitdatemonth'];
                                                                   } ?>" maxlength="2"
                                                                   name="bcyearpermitdatemonth">/<input
                                        class="form-control-sm font-90 border" style="width:50px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearpermitdateday'];
                                        } else if (isset($_POST['bcyearpermitdateday'])) {
                                            echo $_POST['bcyearpermitdateday'];
                                        } ?>" maxlength="255" name="bcyearpermitdateday">/<input
                                        class="form-control-sm font-90 border" style="width:80px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearpermitdateyear'];
                                        } else if (isset($_POST['bcyearpermitdateyear'])) {
                                            echo $_POST['bcyearpermitdateyear'];
                                        } ?>" maxlength="255" name="bcyearpermitdateyear"> </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="buildingcodeb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['buildingcodeb'] == 'Yes') ? 'checked' : '';
                                } ?>> B. For the HVHZ Only: Built in compliance with the SFBC-94: Year Built <input
                                        class="form-control-sm font-90 border" style="width:100px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearbuilt2'];
                                        } else if (isset($_POST['bcyearbuilt2'])) {
                                            echo $_POST['bcyearbuilt2'];
                                        } ?>" maxlength="255" name="bcyearbuilt2">. For homes built in 1994,1995. and
                                1996 provide a permit application with a date after 9/1/1994: Building Permit
                                Application Date <small>(MM/DD/YYYY)</small> <input
                                        class="form-control-sm font-90 border" style="width:50px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearpermitdatemonth2'];
                                        } else if (isset($_POST['bcyearpermitdatemonth2'])) {
                                            echo $_POST['bcyearpermitdatemonth2'];
                                        } ?>" maxlength="2" name="bcyearpermitdatemonth">/<input
                                        class="form-control-sm font-90 border" style="width:50px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearpermitdateday2'];
                                        } else if (isset($_POST['bcyearpermitdateday2'])) {
                                            echo $_POST['bcyearpermitdateday2'];
                                        } ?>" maxlength="255" name="bcyearpermitdateday">/<input
                                        class="form-control-sm font-90 border" style="width:80px;" type="text"
                                        value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['bcyearpermitdateyear2'];
                                        } else if (isset($_POST['bcyearpermitdateyear2'])) {
                                            echo $_POST['bcyearpermitdateyear2'];
                                        } ?>" maxlength="255" name="bcyearpermitdateyear">
                            </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="buildingcodec"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['buildingcodec'] == 'Yes') ? 'checked' : '';
                                } ?>> C. Unknown or does not meet the requirements of Answer "A" or "B"</label>
                            <br>
                            <br>
                        </li>
                        <li>
                            <b><u>Roof Covering:</u></b> Select all roof covering types in use. Provide the permit
                            application date OR FBC/MDC Product Approval number OR Year of Original
                            Installation/Replacement OR indicate that no information was available to verify compliance
                            for each roof covering identified.
                            <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                echo $r['rcdescription'];
                            } else if (isset($_POST['rcdescription'])) {
                                echo $_POST['rcdescription'];
                            } ?>" maxlength="255" name="rcdescription">
                            <br>
                            <br>
                            <table class="table-custom table-responsive table-bordered text-center">
                                <tr>
                                    <th>2.1 Roof Covering Type:</th>
                                    <th>Permit Application Date</th>
                                    <th>FBC or MDC Product<br> Approval #</th>
                                    <th>Year of Original Installation<br> or Replacement</th>
                                    <th>No Information Provided for Conspliance</th>
                                </tr>
                                <?php
                                $roofarr = ['Asphalt/Fiberglass Shingle', 'Concrete/Clat Tile', 'Metal', 'Built Up', 'Membrane', 'Other'];
                                $rcc = 0;
                                while ($rcc != count($roofarr)) {
                                    ?>
                                    <tr>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="roofcoveringtype<?php echo $rcc + 1; ?>"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                    echo ($r['roofcoveringtype' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                                } ?>> <?php echo $rcc + 1; ?>. <?php echo $roofarr[$rcc]; ?>
                                            </label>
                                        </td>
                                        <td>
                                            <input class="form-control-sm font-90 border" style="width:50px;"
                                                   value="<?php if ($reqq->rowCount() > 0) {
                                                       echo $r['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'datemonth'];
                                                   } else if (isset($_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'datemonth'])) {
                                                       echo $_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'datemonth'];
                                                   } ?>"
                                                   name="rc<?php echo(str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))); ?>datemonth">/<input
                                                    class="form-control-sm font-90 border" style="width:50px;"
                                                    value="<?php if ($reqq->rowCount() > 0) {
                                                        echo $r['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateday'];
                                                    } else if (isset($_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateday'])) {
                                                        echo $_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateday'];
                                                    } ?>"
                                                    name="rc<?php echo(str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))); ?>dateday">/<input
                                                    class="form-control-sm font-90 border" style="width:80px;"
                                                    value="<?php if ($reqq->rowCount() > 0) {
                                                        echo $r['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateyear'];
                                                    } else if (isset($_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateyear'])) {
                                                        echo $_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'dateyear'];
                                                    } ?>"
                                                    name="rc<?php echo(str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))); ?>dateyear">
                                        </td>
                                        <td class="text-center">
                                            <input class="form-control-sm font-90 border"
                                                   value="<?php if ($reqq->rowCount() > 0) {
                                                       echo $r['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'approvalno'];
                                                   } else if (isset($_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'approvalno'])) {
                                                       echo $_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'approvalno'];
                                                   } ?>" maxlength="255"
                                                   name="rc<?php echo(str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))); ?>approvalno">
                                        </td>
                                        <td class="text-center">
                                            <input class="form-control-sm font-90 border"
                                                   value="<?php if ($reqq->rowCount() > 0) {
                                                       echo $r['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'installationyear'];
                                                   } else if (isset($_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'installationyear'])) {
                                                       echo $_POST['rc' . (str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))) . 'installationyear'];
                                                   } ?>" maxlength="255"
                                                   name="rc<?php echo(str_replace('/', '', strtolower(preg_replace('/\s*/', '', $roofarr[$rcc])))); ?>installationyear">
                                        </td>
                                        <td class="text-center">
                                            <input type="checkbox" name="roofcoveringconspliance<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['roofcoveringconspliance' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>> <?php echo $rcc + 1; ?>. <?php echo $roofarr[$rcc]; ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $rcc++;
                                }
                                ?>
                            </table>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofcoveringa"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofcoveringa'] == 'Yes') ? 'checked' : '';
                                } ?>> A. All roof coverings listed above meet the FBC with a FBC or Miami-Dade Product
                                Approval listing current at time of installation OR have a roofing permit application
                                date on or after 3/1/02 OR the roof is original and built in 2004 or later. </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofcoveringb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofcoveringb'] == 'Yes') ? 'checked' : '';
                                } ?>> B. All roof coverings have a Miami-Dade Product Approval listing current at time
                                of installation OR (for the HVHZ only) a roofing permit application after 9/1/1994 and
                                before 3/1/2002 OR the roof is original and built in 1997 or later. </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofcoveringc"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofcoveringc'] == 'Yes') ? 'checked' : '';
                                } ?>> C. One or more roof coverings do not meet the requirements of Answer "A" or "B".
                            </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofcoveringd"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofcoveringd'] == 'Yes') ? 'checked' : '';
                                } ?>> D. No roof coverings meet the requirements of Answer "A" or "B". </label>
                        </li>
                        <br><br>
                        <li>
                            <b><u>Roof Deck Attachment:</u></b> What is the weakest form of roof deck attachment?
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofdecka"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdecka'] == 'Yes') ? 'checked' : '';
                                } ?>> A. Plywood/Oriented strand board (OSB) roof sheathing attached to the roof
                                truss/rafter (spaced a maximum of 24" inches o.c.) by staples or 6d nails spaced at 6"
                                along the edge and 12" in the field. -OR- Batten decking supporting wood shakes or wood
                                shingles. -OR- Any system of screws, nails, adhesives, other deck fastening system or
                                truss/rafter spacing that has an equivalent mean uplift less than that required for
                                Options B or C below. </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofdeckb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdeckb'] == 'Yes') ? 'checked' : '';
                                } ?>> B. Plywood/OSB roof sheathing with a minimum thickness of 7/16"inch attached to
                                the roof truss/rafter (spaced a maximum of 24"inches o.c.) by 8d common nails spaced a
                                maximum of 12" inches in the field.-OR- Any system of screws, nails, adhesives, other
                                deck fastening system or truss/rafter spacing that is shown to have an equivalent or
                                greater resistance than 8d nails spaced a maximum of 12 inches in the field or has a
                                mean uplift resistance of at least 103 psf. </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="roofdeckc"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdeckc'] == 'Yes') ? 'checked' : '';
                                } ?>> C. Plywood/OSB roof sheathing with a minimum thickness of 7/16"inch attached to
                                the roof truss/rafter (spaced a maximum of 24"inches o.c.) by 8d common nails spaced a
                                maximum of 6" inches in the field. -OR- Dimensional lumber/Tongue & Groove decking with
                                a minimum of 2 nails per board (or 1 nail per board if each board is equal to or less
                                than 6 inches in width). -OR-Any system of screws, nails, adhesives, other deck
                                fastening system or truss/rafter spacing that is shown to have an equivalent. </label>
                            <br>
                        </li>
                        <div class="col-12 p-0 nav justify-content-start py-3">
                            <div class="col-3 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Inspectors Initials</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['inspectorsinitials'];
                                    } else if (isset($_POST['inspectorsinitials'])) {
                                        echo $_POST['inspectorsinitials'];
                                    } ?>" maxlength="255" name="inspectorsinitials">
                                </div>
                            </div>
                            <div class="col-9 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Property Address</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['propertyaddress'];
                                    } else if (isset($_POST['propertyaddress'])) {
                                        echo $_POST['propertyaddress'];
                                    } ?>" maxlength="255" name="propertyaddress">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 my-3"><b>•This verification form is valid for up to five (5) years
                                provided no material changes have been made to the structure or inaccuracies found on
                                the form.<br>
                                01R-B1.1802 (Rev. 01/12) Adopted by Rule 690-170.0155
                            </b>
                        </div>
                        <div class="col-12">
                            or greater resistance than 8d common nails spaced a maximum of 6 inches in the field or has
                            a mean uplift resistance of at least 182 psf.
                        </div>
                        <div><label class="mt-2"><input type="checkbox" name="roofdeckd"
                                                        value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdeckd'] == 'Yes') ? 'checked' : '';
                                } ?>> D. Reinforced Concrete Roof Deck. </label></div>
                        <div><label class="mt-2"><input type="checkbox" name="roofdecke"
                                                        value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdecke'] == 'Yes') ? 'checked' : '';
                                } ?>> E. Other <input class="border"> </label></div>
                        <div><label class="mt-2"><input type="checkbox" name="roofdeckf"
                                                        value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdeckf'] == 'Yes') ? 'checked' : '';
                                } ?>> F. Unknown or unidentified. </label></div>
                        <div><label class="mt-2"><input type="checkbox" name="roofdeckg"
                                                        value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['roofdeckg'] == 'Yes') ? 'checked' : '';
                                } ?>> G. No attic access.</label></div>
                        <br><br>
                        <li>
                            <b><u>Roof to Wall Attachment:</u></b> What is the WEAKEST roof to wall connection? (Do not
                            include attachment of hip/valley jacks within 5 feet of the inside or outside corner of the
                            roof in determination of WEAKEST type)
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowalla"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowalla'] == 'Yes') ? 'checked' : '';
                                } ?>> A. Toe Nails </label>
                            <div class="col-12 pl-5">
                                <label class="mt-2"><input type="checkbox" name="rooftowalla1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowalla1'] == 'Yes') ? 'checked' : '';
                                    } ?>> Truss/ratter anchored to top plate of wall using nails driven at an angle
                                    through the truss/rafter and attached to the top plate of the wall, or </label>
                                <label class="mt-2"><input type="checkbox" name="rooftowalla2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowalla2'] == 'Yes') ? 'checked' : '';
                                    } ?>> Metal connectors that do not meet the minimal conditions or requirements of B,
                                    C, or D </label>
                            </div>
                            <br>
                            <div>
                                <b><u>Minimal conditions to qualify for categories B. C. or D. All visible metal
                                        connectors are: </u></b>
                            </div>
                            <div class="col-12 pl-5">
                                <label class="mt-2"><input type="checkbox" name="rooftowalla3"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowalla3'] == 'Yes') ? 'checked' : '';
                                    } ?>> Secured to truss/rafter with a minimum of three (3) nails, and </label>
                                <label class="mt-2"><input type="checkbox" name="rooftowalla4"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowalla4'] == 'Yes') ? 'checked' : '';
                                    } ?>> Attached to the wall top plate of the wall framing, or embedded in the bond
                                    beam, with less than a 16" gap from the blocking or truss/rafter and blocked no more
                                    than 1.5" of the nuss/rafter, and free of visible severe corrosion. </label>
                            </div>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowallb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowallb'] == 'Yes') ? 'checked' : '';
                                } ?>> B. Clips</label>
                            <div class="col-12 pl-5">
                                <label class="mt-2"><input type="checkbox" name="rooftowallb1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowallb1'] == 'Yes') ? 'checked' : '';
                                    } ?>> Metal connectors that do not wrap over the top of the truss/rafter, or
                                </label>
                                <label class="mt-2"><input type="checkbox" name="rooftowallb2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['rooftowallb2'] == 'Yes') ? 'checked' : '';
                                    } ?>> Metal connectors with a minimum of 1 strap that wraps over the top of the
                                    truss/rafter and does not meet the nail position requirements of C or D, but is
                                    secured with a minimum of 3 nails. </label>
                            </div>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowallc"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowallc'] == 'Yes') ? 'checked' : '';
                                } ?>> C. Single Wraps</label>
                            <div class="col-12 pl-5">
                                <label class="mt-2">Metal connectors consisting of a single strap that wraps over the
                                    top of the truss/rafter and is secured with a minimum of 2 nails on the front side
                                    and a minimum of 1 nail on the opposing side. </label>
                            </div>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowalld"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowalld'] == 'Yes') ? 'checked' : '';
                                } ?>> D. Double Wraps</label>
                            <div class="col-12 pl-5">
                                <label class="mt-2">Metal Connectors consisting of 2 separate straps that are attached
                                    to the wall frame. or embedded in the bond beam, on either side of the truss/rafter
                                    where each strap wraps over the top of the truss/rafter and is secured with a
                                    minimum of 2 nails on the front side, and a minimum of 1 nail on the opposing side,
                                    or</label>
                                <label class="mt-2">Metal connectors consisting of a single strap that wraps over the
                                    top of the truss/rafter, is secured to the wall on both sides, and is secured to the
                                    top plate with a minimum of three nails on each side.</label>
                            </div>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowalle"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowalle'] == 'Yes') ? 'checked' : '';
                                } ?>> E. Structural - Anchor bolts structurally connected or reinforced concrete
                                roof.</label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowallf"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowallf'] == 'Yes') ? 'checked' : '';
                                } ?>> F. Other <input class="border"></label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowallg"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowallg'] == 'Yes') ? 'checked' : '';
                                } ?>> G. Unknown or unidentified</label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="rooftowallh"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['rooftowallh'] == 'Yes') ? 'checked' : '';
                                } ?>> H. No attic access</label>
                        </li>
                        <br><br>
                        <li>
                            <b><u>Roof Geometry:</u></b> What is the roof shape? (Do not consider roofs of porches or
                            carports that arc attached only to the fascia or wall of the host structure over unenclosed
                            space in the determination of roof perimeter or roof area for roof geometry classification).
                            <br>
                            <br>
                            <table class="col-12 p-0">
                                <tr>
                                    <td><input type="checkbox" name="roofgeometrya"
                                               value="Yes" <?php if ($reqq->rowCount() > 0) {
                                            echo ($r['roofgeometrya'] == 'Yes') ? 'checked' : '';
                                        } ?>> A. Hip Roof
                                    </td>
                                    <td>Hip roof with no other roof shapes greater than 10% of the total roof system
                                        perimeter. <br>Total length of non-hip features: <input
                                                class="form-control-sm font-90 border" style="width:100px;"
                                                value="<?php if ($reqq->rowCount() > 0) {
                                                    echo $r['hrtotalfeatures'];
                                                } else if (isset($_POST['hrtotalfeatures'])) {
                                                    echo $_POST['hrtotalfeatures'];
                                                } ?>" maxlength="255" name="hrtotalfeatures"> feet Total roof system
                                        perimeter: <input class="form-control-sm font-90 border" style="width:100px;"
                                                          value="<?php if ($reqq->rowCount() > 0) {
                                                              echo $r['hrtotalperimeter'];
                                                          } else if (isset($_POST['hrtotalperimeter'])) {
                                                              echo $_POST['hrtotalperimeter'];
                                                          } ?>" maxlength="255" name="hrtotalperimeter"> feet
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="roofgeometryb"
                                               value="Yes" <?php if ($reqq->rowCount() > 0) {
                                            echo ($r['roofgeometryb'] == 'Yes') ? 'checked' : '';
                                        } ?>> B. Flat Roof
                                    </td>
                                    <td>Roof on a building with 5 or more units where at least 90% of the main roof area
                                        has a roof slope of less than 2:12.<br> Roof area with slope less than 2:12
                                        <input class="form-control-sm font-90 border" style="width:100px;"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['frroofslope'];
                                               } else if (isset($_POST['frroofslope'])) {
                                                   echo $_POST['frroofslope'];
                                               } ?>" maxlength="255" name="frroofslope"> sq ft; Total roof area <input
                                                class="form-control-sm font-90 border" style="width:100px;"
                                                value="<?php if ($reqq->rowCount() > 0) {
                                                    echo $r['frtotalarea'];
                                                } else if (isset($_POST['frtotalarea'])) {
                                                    echo $_POST['frtotalarea'];
                                                } ?>" maxlength="255" name="frtotalarea"> sq ft
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="roofgeometryc"
                                               value="Yes" <?php if ($reqq->rowCount() > 0) {
                                            echo ($r['roofgeometryc'] == 'Yes') ? 'checked' : '';
                                        } ?>> C. Other Roof
                                    </td>
                                    <td>Any roof that does not qualify as either (A) or (B) above.</td>
                                </tr>
                            </table>
                        </li>
                        <br><br>
                        <li>
                            <b><u>Secondary Water Resistance (SWR): </u></b> (standard underlayments or hot-mopped felts
                            do not qualify as an SWR)
                            <br>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="waterresistancea"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['waterresistancea'] == 'Yes') ? 'checked' : '';
                                } ?>> A. SWR (also called Sealed Roof Deck) Self-adhering polymer modified-bitumen
                                roofing underlayment applied directly to the sheathing or foam adhesive SWR barrier (not
                                foamed-on insulation) applied as a supplemental means to protect the dwelling from water
                                intrusion in the event of roof covering loss. </label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="waterresistanceb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['waterresistanceb'] == 'Yes') ? 'checked' : '';
                                } ?>> B. No SWR.</label>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="waterresistancec"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['waterresistancec'] == 'Yes') ? 'checked' : '';
                                } ?>> C. Unknown or undetermined.</label>
                            <br>
                        </li>
                        <div class="col-12 p-0 nav justify-content-start py-3">
                            <div class="col-3 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Inspectors Initials</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['inspectorsinitials2'];
                                    } else if (isset($_POST['inspectorsinitials2'])) {
                                        echo $_POST['inspectorsinitials2'];
                                    } ?>" maxlength="255" name="inspectorsinitials2">
                                </div>
                            </div>
                            <div class="col-9 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Property Address</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['propertyaddress2'];
                                    } else if (isset($_POST['propertyaddress2'])) {
                                        echo $_POST['propertyaddress2'];
                                    } ?>" maxlength="255" name="propertyaddress2">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 my-3"><b>•This verification form is valid for up to five (5) years
                                provided no material changes have been made to the structure or inaccuracies found on
                                the form.<br>
                                01R-B1.1802 (Rev. 01/12) Adopted by Rule 690-170.0155
                            </b>
                        </div>
                        <br><br>
                        <li>
                            <b><u>Opening Protection:</u></b> What is the weakest form of wind borne debris protection
                            installed on the structure? First, use the table to determine the weakest form of protection
                            for each category of opening. Second, (a) check one answer below (A, B, C, N, or X) based
                            upon the lowest protection level for ALL Glazed openings and (b) check the protection level
                            for all Non-Glazed openings (.1, .2, or .3) as applicable.
                            <br>
                            <br>
                            <table class="table-custom table-responsive table-bordered text-center">
                                <tr>
                                    <th colspan="2" class="text-left font-weight-bold">Opening Protection Level Chart
                                    </th>
                                    <th colspan="4">Glazed Openings</th>
                                    <th colspan="2">Non Glazed Openings</th>
                                </tr>
                                <tr>
                                    <th colspan="2" class="text-left">Place an "X" in each row to identify all forms of
                                        protection In use for each opening type. Check only one answer below (A thru X),
                                        based on the weakest form of protection (lowest row) for any of the Glazed
                                        openings and indicate the weakest form of protection (lowest row) for Non-Glazed
                                        openings.
                                    </th>
                                    <th>Windows<br>or Entry<br>Doors</th>
                                    <th>Garage<br>Doors</th>
                                    <th>Skylights</th>
                                    <th>Glass<br>Block</th>
                                    <th>Entry<br>Doors</th>
                                    <th>Garage<br>Doors</th>
                                </tr>
                                <?php
                                $tablesrarr = ['N/A', 'A', 'B', 'C', 'D', 'N', '', 'X'];
                                $tablearr = ['Not Applicable- there are no openings of this type on the structure', 'Verified cyclic pressure & large missile (9-lb for windows doors/4.5 lb for skylights)', 'Verified cyclic pressure & large missile (4-8 lb for windows doors/2 lb for skylights)', 'Verified plywood/OSB meeting Table 1609.1.2 of the FBC 2007', 'Verified Non-Glazed Entry or Garage doors indicating compliance with ASTM E 330, ANSI/DASMA 108, or PA/TAS 202 for wind pressure resistance', 'Opening Protection products that appear to be A or B but are not verified', 'Other protective coverings that cannot be identified as A, B, or C', 'No Windbome Debris Protection'];
                                $rcc = 0;
                                while ($rcc != count($tablearr)) {
                                    $ts = $rcc;
                                    ?>
                                    <tr>
                                        <?php
                                        if ($tablesrarr[$rcc] == "N") {
                                            ?>
                                            <td rowspan="2"><?php echo $tablesrarr[$rcc]; ?></td>
                                            <?php
                                        } else {
                                            if ($tablesrarr[$rcc] != "") {
                                                ?>
                                                <td><?php echo $tablesrarr[$rcc]; ?></td>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <td class="text-left"><?php echo $tablearr[$ts]; ?></td>
                                        <td><input type="checkbox" name="windowdoors<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['windowdoors' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                        <td><input type="checkbox" name="garagedoors<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['garagedoors' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                        <td><input type="checkbox" name="skylights<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['skylights' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                        <td><input type="checkbox" name="glassback<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['glassback' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                        <td><input type="checkbox" name="entrydoors<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['entrydoors' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                        <td><input type="checkbox" name="garagedoors2<?php echo $rcc + 1; ?>"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                echo ($r['garagedoors2' . ($rcc + 1)] == 'Yes') ? 'checked' : '';
                                            } ?>></td>
                                    </tr>

                                    <?php
                                    $rcc++;
                                }
                                ?>
                            </table>
                            <br>
                            <br>
                            <label class="mt-2"><input type="checkbox" name="openingprotectiona"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['openingprotectiona'] == 'Yes') ? 'checked' : '';
                                } ?>> <b><u>A. Exterior Openings Cyclic Pressure and 9-lb Large Missile (45 lb for
                                        skylights only)</u> </b> All Glazed openings are protected at a minimum, with
                                impact resistant coverings or products listed as wind borne debris protection devices in
                                the product approval system of the State of Florida or Miami-Dade County and meet the
                                requirements of one of the following for "Cyclic Pressure and Large Missile Impact"
                                (Level A in the table above).
                            </label>
                            <div class="pl-4 my-2">
                                <ul>
                                    <li> Miami-Dade County PA 201,202 la 203</li>
                                    <li> Florida Building Code Testing Application Standard (TAS) 201,202 itg 203</li>
                                    <li> American Society for Testing and Materials (ASTM) E 1886 arg ASTM E 1996</li>
                                    <li> Southern Standards Technical Document (SSTD) 12</li>
                                    <li> For Skylights Only: ASTM E 1886 and ASTM E 1996</li>
                                    <li> For Garage Doors Only: ANSI/DASMA 115</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <label class="mt-2"><input type="checkbox" name="openingprotectiona1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectiona1'] == 'Yes') ? 'checked' : '';
                                    } ?>> A.1 All Non-Glazed openings classified as A in the table above, or no
                                    Non-Glazed openings exist</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectiona2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectiona2'] == 'Yes') ? 'checked' : '';
                                    } ?>> A.2 One or More Non-Glazed openings classified as Level D in the table above,
                                    and no Non-Glazed openings classified as Level B, C, N, or X in the table
                                    above</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectiona3"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectiona3'] == 'Yes') ? 'checked' : '';
                                    } ?>> A.3 One or More Non-Glazed Openings is classified as Level B, C, N, or X in
                                    the table above</label>
                            </div>
                            <br><br>
                            <label class="mt-2"><input type="checkbox" name="openingprotectionb"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['openingprotectionb'] == 'Yes') ? 'checked' : '';
                                } ?>> <b><u>B. Exterior Opening Protection- Cyclic Pressure and 4 to 8-lb Large Missile
                                        (24.5 lb for skylights only)</u> </b> All Glazed openings are protected, at a
                                minimum, with impact resistant coverings or products listed as windbome debris
                                protection devices in the product approval system of the State of Florida or Miami-Dade
                                County and meet the requirements of one of the following for "Cyclic Pressure and Large
                                Missile Impact" (Level B in the table above):
                            </label>
                            <div class="pl-4 my-2">
                                <ul>
                                    <li> ASTM E 1886 and ASTM E 1996 (Large Missile — 43 lb.)</li>
                                    <li> SSTD 12 (Large Missile — 4 lb. to 8 lb.)</li>
                                    <li> For Skylights Only: ASTM E 1886 and ASTM E 1996 (Large Missile - 2 to 4.5
                                        lb.)
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <label class="mt-2"><input type="checkbox" name="openingprotectionb1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionb1'] == 'Yes') ? 'checked' : '';
                                    } ?>> B.1 All Non-Glazed openings classified as A or B in the table above, or no
                                    Non-Glazed openings exist</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionb2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionb2'] == 'Yes') ? 'checked' : '';
                                    } ?>> B.2 One or More Non-Glazed openings classified as Level D in the table above,
                                    and no Non-Glazed openings classified as Level C, N, or X in the table above</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionb3"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionb3'] == 'Yes') ? 'checked' : '';
                                    } ?>> B.3 One or More Non-Glazed openings is classified as Level C. N, or X in the
                                    table above</label>
                            </div>
                            <br><br>
                            <label class="mt-2"><input type="checkbox" name="openingprotectionc"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['openingprotectionc'] == 'Yes') ? 'checked' : '';
                                } ?>> <b><u>C. Exterior Opening Protection- Wood Structural Panels meeting FBC 2007</u>
                                </b> All Glazed openings are covered with plywood/OSB meeting the requirements of Table
                                1609.1.2 of the FBC 2007 (Level C in the table above).
                            </label>
                            <div class="col-12">
                                <label class="mt-2"><input type="checkbox" name="openingprotectionc1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionc1'] == 'Yes') ? 'checked' : '';
                                    } ?>> C.1 All Non-Glazed openings classified as A, B, or C in the table above, or no
                                    Non-Glazed openings exist</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionc2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionc2'] == 'Yes') ? 'checked' : '';
                                    } ?>> C.2 One or More Non-Glazed openings classified as Level D in the table above,
                                    and no Non-Glazed openings classified as Level N or X in the table above</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionc3"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionc3'] == 'Yes') ? 'checked' : '';
                                    } ?>> C.3 One or More Non-Glazed openings is classified as Level N or X in the table
                                    above</label>
                            </div>
                            <br><br>
                            <div class="col-12 p-0 nav justify-content-start py-3">
                                <div class="col-3 p-0">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text font-weight-500">Inspectors Initials</span>
                                        </div>
                                        <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['inspectorsinitials3'];
                                        } else if (isset($_POST['inspectorsinitials3'])) {
                                            echo $_POST['inspectorsinitials3'];
                                        } ?>" maxlength="255" name="inspectorsinitials3">
                                    </div>
                                </div>
                                <div class="col-9 p-0">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text font-weight-500">Property Address</span>
                                        </div>
                                        <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                            echo $r['propertyaddress3'];
                                        } else if (isset($_POST['propertyaddress3'])) {
                                            echo $_POST['propertyaddress3'];
                                        } ?>" maxlength="255" name="propertyaddress3">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 p-0 my-3"><b>•This verification form is valid for up to five (5) years
                                    provided no material changes have been made to the structure or inaccuracies found
                                    on the form.<br>
                                    01R-B1.1802 (Rev. 01/12) Adopted by Rule 690-170.0155
                                </b>
                            </div>
                            <br><br>
                            <label class="mt-2"><input type="checkbox" name="openingprotectionn"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['openingprotectionn'] == 'Yes') ? 'checked' : '';
                                } ?>> <b><u>N. Exterior Opening Protection (unverified shutter systems with no
                                        documentation)</u></b> All Glazed openings are protected with protective
                                coverings not meeting the requirements of Answer "A", "B", or C" or systems that appear
                                to meet Answer "A" or "B" with no documentation of compliance (Level N in the table
                                above).
                            </label>
                            <div class="col-12">
                                <label class="mt-2"><input type="checkbox" name="openingprotectionn1"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionn1'] == 'Yes') ? 'checked' : '';
                                    } ?>> N.1 All Non-Glazed openings classified as Level A, B, C, or N in the table
                                    above, or no Non-Glazed openings exist</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionn2"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionn2'] == 'Yes') ? 'checked' : '';
                                    } ?>> N.2 One or More Non-Glazed openings classified as Level D in the table above,
                                    and no Non-Glazed openings classified as Level X in the table above</label>
                                <label class="mt-2"><input type="checkbox" name="openingprotectionn3"
                                                           value="Yes" <?php if ($reqq->rowCount() > 0) {
                                        echo ($r['openingprotectionn3'] == 'Yes') ? 'checked' : '';
                                    } ?>> N.3 One or More Non-Glazed openings is classified as Level X in the table
                                    above</label>
                            </div>
                            <br><br>
                            <label class="mt-2"><input type="checkbox" name="openingprotectionx"
                                                       value="Yes" <?php if ($reqq->rowCount() > 0) {
                                    echo ($r['openingprotectionx'] == 'Yes') ? 'checked' : '';
                                } ?>> <b><u>X. None or Some Glazed Openings</u></b> One or more Glazed openings
                                classified and Level X in the table above.
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="form-group col-md-12 mt-5">
                    <div class="col-12 border border-dark p-0">
                        <div class="col-12 text-center py-3">
                            <h6 class="mb-0"><u><i>MITIGATION INSPECTIONS MUST BE CERTIFIED BY A QUALIFIED
                                        INSPECTOR.</i></u></h6>
                            <i>Section 627.711(2), Florida Statutes, provides a listing of individuals who may sign this
                                form.</i>
                        </div>
                        <div class="col-12 p-0 border-top border-bottom border-dark py-2 nav justify-content-start">
                            <div class="col-6 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Qualified Inspector Name:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['miinspectorname'];
                                    } else if (isset($_POST['miinspectorname'])) {
                                        echo $_POST['miinspectorname'];
                                    } ?>" maxlength="255" name="miinspectorname">
                                </div>
                            </div>
                            <div class="col-3 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">License Type:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['milicensetype'];
                                    } else if (isset($_POST['milicensetype'])) {
                                        echo $_POST['milicensetype'];
                                    } ?>" maxlength="255" name="milicensetype">
                                </div>
                            </div>
                            <div class="col-3 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">License or Certificate #:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['milicenseno'];
                                    } else if (isset($_POST['milicenseno'])) {
                                        echo $_POST['milicenseno'];
                                    } ?>" maxlength="255" name="milicenseno">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 pb-2 nav justify-content-start">
                            <div class="col-3 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Inspectors Initials</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['inspectorsinitials4'];
                                    } else if (isset($_POST['inspectorsinitials4'])) {
                                        echo $_POST['inspectorsinitials4'];
                                    } ?>" maxlength="255" name="inspectorsinitials4">
                                </div>
                            </div>
                            <div class="col-9 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-500">Property Address</span>
                                    </div>
                                    <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['propertyaddress4'];
                                    } else if (isset($_POST['propertyaddress4'])) {
                                        echo $_POST['propertyaddress4'];
                                    } ?>" maxlength="255" name="propertyaddress4">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-12 px-3 py-2">
                        <h3 class="text-black"><u>Qualified Inspector — I hold an active license as a:</u> (check one)
                        </h3>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector1"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector1'] == 'Yes') ? 'checked' : '';
                            } ?>> Home inspector licensed under Section 468.8314, Florida Statutes who has completed the
                            statutory number of hours of hurricane mitigation training approved by the Construction
                            Industry Licensing Board and completion of a proficiency exam.
                        </label>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector2"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector2'] == 'Yes') ? 'checked' : '';
                            } ?>> Building code inspector certified under Section 468.607, Florida Statutes.
                        </label>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector3"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector3'] == 'Yes') ? 'checked' : '';
                            } ?>> General, building or residential contractor licensed under Section 489.111, Florida
                            Statutes.
                        </label>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector4"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector4'] == 'Yes') ? 'checked' : '';
                            } ?>> Professional engineer licensed under Section 471.015, Florida Statutes.
                        </label>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector5"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector5'] == 'Yes') ? 'checked' : '';
                            } ?>> Professional architect licensed under Section 481213, Florida Statutes.
                        </label>
                        <label class="mt-2"><input type="checkbox" name="qualifiedinspector6"
                                                   value="Yes" <?php if ($reqq->rowCount() > 0) {
                                echo ($r['qualifiedinspector6'] == 'Yes') ? 'checked' : '';
                            } ?>> Any other individual or entity recognized by the insurer as possessing the necessary
                            qualifications to properly complete a uniform mitigation verification form pursuant to
                            Section 627.711(2), Florida Statutes.
                        </label>
                    </div>
                    <div class="col-12 border border-dark p-3">
                        <b><u>Individuals other than licensed contractors licensed under Section 489.111, Florida
                                Statutes, or professional engineer licensed under Section 471.015, Florida Statues, must
                                inspect the structures personally and not through employees or other persons. Licensees
                                under s.471.015 or s.489.111 may authorize a direct employee who possesses the requisite
                                skill, knowledge, and experience to conduct a mitigation verification
                                inspection.</u></b>
                        <br>
                        <br>
                        <div class="nav justify-content-start font-weight-bold">I,
                            <div class="text-center"><input class="form-control-sm font-90 border-0"
                                                            style="width:100px;border-bottom:1.5px solid black !important;"
                                                            value="<?php if ($reqq->rowCount() > 0) {
                                                                echo $r['printname'];
                                                            } else if (isset($_POST['printname'])) {
                                                                echo $_POST['printname'];
                                                            } ?>" maxlength="255" name="printname"><br><span
                                        class="font-85">(print name)</span></div>
                            am a qualified inspector and I personally performed the inspection or <i>(licensed
                                contractors and professional engineers only)</i> I had my employee
                            <div class="text-center"><input class="form-control-sm font-90 border-0"
                                                            style="width:180px;border-bottom:1.5px solid black !important;"
                                                            value="<?php if ($reqq->rowCount() > 0) {
                                                                echo $r['inspectorprintname'];
                                                            } else if (isset($_POST['inspectorprintname'])) {
                                                                echo $_POST['inspectorprintname'];
                                                            } ?>" maxlength="255" name="inspectorprintname"><br><span
                                        class="font-85">(print name of inspector)</span></div>
                            perform the inspection and I agree to be responsible for his/her work.
                        </div>
                        <div class="col-12 p-0 py-3 nav justify-content-start font-weight-bold">
                            <div class="col-8 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Qualified Inspector Signature:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['qualifiedinspectorsign'];
                                    } else if (isset($_POST['qualifiedinspectorsign'])) {
                                        echo $_POST['qualifiedinspectorsign'];
                                    } ?>" maxlength="255" name="qualifiedinspectorsign">
                                </div>
                            </div>
                            <div class="col-4 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Date:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['inspectorsigndate'];
                                    } else if (isset($_POST['inspectorsigndate'])) {
                                        echo $_POST['inspectorsigndate'];
                                    } ?>" maxlength="255" name="inspectorsigndate">
                                </div>
                            </div>
                        </div>
                        <b><u>An individual or entity who knowingly or through gross negligence provides a false or
                                fraudulent mitigation verification form is subject to investigation by the Florida
                                Division of Insurance Fraud and may be subject to administrative action by the
                                appropriate licensing agency or to criminal prosecution. (Section 627.711(4)-(7),
                                Florida Statutes) The Qualified Inspector who certifies this form shall be directly
                                liable for the misconduct of employees as if the authorized mitigation inspector
                                personally performed the inspection. </u></b>
                    </div>
                    <br>
                    <div class="col-12 border border-dark p-3">
                        <b><u>Homeowner to complete:</u></b> I certify that the named Qualified Inspector or his or her
                        employee did perform an inspection of the residence identified on this form and that proof of
                        identification was provided to me or my Authorized Representative.
                        <br>
                        <div class="col-12 p-0 py-3 nav justify-content-start font-weight-bold">
                            <div class="col-8 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Signature:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['homeownersign'];
                                    } else if (isset($_POST['homeownersign'])) {
                                        echo $_POST['homeownersign'];
                                    } ?>" maxlength="255" name="homeownersign">
                                </div>
                            </div>
                            <div class="col-4 p-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Date:</span>
                                    </div>
                                    <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                                        echo $r['homeownersigndate'];
                                    } else if (isset($_POST['homeownersigndate'])) {
                                        echo $_POST['homeownersigndate'];
                                    } ?>" maxlength="255" name="homeownersigndate">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 border border-dark p-3">
                        <b><u>An individual or entity who knowingly provides or utters a false or fraudulent mitigation
                                verification form with the intent to obtain or receive a discount on an insurance
                                premium to which the individual or entity is not entitled commits a misdemeanor of the
                                first degree. (Section 627.711(7), Florida Statutes)</u></b>
                    </div>
                    <br><br>
                    <div class="col-12">
                        <b><u>The definitions on this form are for inspection purposes only and cannot be used to
                                certify any product or construction feature as offering protection from hurricanes.</u></b>
                    </div>
                    <br>
                    <div class="col-12 p-0 nav justify-content-start py-3">
                        <div class="col-3 p-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text font-weight-500">Inspectors Initials</span>
                                </div>
                                <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                    echo $r['inspectorsinitials5'];
                                } else if (isset($_POST['inspectorsinitials5'])) {
                                    echo $_POST['inspectorsinitials5'];
                                } ?>" maxlength="255" name="inspectorsinitials5">
                            </div>
                        </div>
                        <div class="col-9 p-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text font-weight-500">Property Address</span>
                                </div>
                                <input class="form-control" value="<?php if ($reqq->rowCount() > 0) {
                                    echo $r['propertyaddress5'];
                                } else if (isset($_POST['propertyaddress5'])) {
                                    echo $_POST['propertyaddress5'];
                                } ?>" maxlength="255" name="propertyaddress5">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 p-0 my-3"><b>*This verification form is valid for up to five (5) years provided
                            no material changes have been made to the structure or inaccuracies found on the form.<br>
                            01R-B1.1802 (Rev. 01/12) Adopted by Rule 690-170.0155
                        </b>
                    </div>
                </div>
                <br>
            </div>
            <div id="third-page" class="nav justify-content-between col-12 p-0">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-4 nav justify-content-start images">
                    <?php
                    $doctypearr = ['Photo Attachments'];
                    $ic = 1;
                    $ict = 0;
                    $icn = 0;
                    $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                    $class = '';
                    while ($ict < count($doctypearr)) {
                        ?>
                        <div class="col-12 my-4">
                            <h4 class="mb-0"><?php echo $doctypearr[$ict]; ?></h4>
                        </div>
                        <?php
                        $faname = ['Subject Property Address', 'Elevation', 'Elevation', 'Elevation', 'Elevation', 'Elevation', 'Elevation', '', '', '', '', '', '', '', ''];
                        $ic = 0;
                        while ($ic != count($faname)) {
                            if ($reqq->rowCount() > 0) {
                                $alreadyq = $a->con->prepare("select * from uniformmitigationfiles where reqid = ? and imgno = ?");
                                $alreadyq->execute([$_GET['reqid'], $icn]);
                                if ($alreadyq->rowCount() > 0) {
                                    $already = $alreadyq->fetch();
                                    $thumbnail = $already['file'];
                                    $class = 'active';
                                    $imgn = $already['name'];
                                } else {
                                    $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                                    $class = '';
                                    $imgn = $faname[$ic];
                                }
                            } else {
                                $thumbnail = "https://static.thenounproject.com/png/971625-200.png";
                                $class = '';
                                $imgn = $faname[$ic];
                            }
                            if (isset($_GET['download'])) {
                                if ($alreadyq->rowCount() == 0) {
                                    $show = 0;
                                } else {
                                    $show = 1;
                                }
                            } else {
                                $show = 1;
                            }
                            if ($show == 1) {
                                ?>
                                <div class="col-4 mb-4">
                                    <div class="file-upload-img image-box<?php echo $icn; ?> <?php echo $class; ?>">
                                        <div class="file-select">
                                            <?php if (!(isset($_GET['download']))) { ?>
                                                <div class="nav justify-content-between">
                                                    <div class="file-select-button" id="fileName">
                                                        <div class="nav justify-content-between">
                                                            <div>Choose File</div>
                                                            <div class="mt-0 remove-extra"
                                                                 style="<?php if ($class == '') {
                                                                     echo 'display:none;';
                                                                 } ?> position:absolute; z-index:999;right:10px;"
                                                                 id="<?php echo $icn; ?>"><i
                                                                        class="fas fa-times text-light"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="file-select-name" id="noFile"><img class="col-12 p-0"
                                                                                           src="<?php echo $thumbnail; ?>">
                                            </div>
                                            <input type="file" name="img<?php echo $icn; ?>" accept="image/*">
                                        </div>
                                    </div>
                                    <input class="form-control rounded-0 text-center  mt-2"
                                           name="imagename<?php echo $icn; ?>" value="<?php echo $imgn; ?>">
                                </div>
                                <?php
                            }
                            $icn++;
                            $ic++;
                        }
                        $ict++;
                    }
                    ?>
                </div>
            </div>
            <?php if (!(isset($_GET['download']))) { ?>
                <div class="col-lg-12 col-md-12 text-center col-sm-12 mt-5 nav justify-content-center">
                    <?php if ($reqq->rowCount() > 0) {
                        ?>
                        <button type="submit" name="update"
                                class="btn btn-dark col-md-6 px-3 btn-update border shadow bg-main">Update
                        </button>
                        <?php
                    } else { ?>
                        <button type="submit" name="submit"
                                class="btn btn-dark col-md-6 px-4 btn-submit border shadow bg-main">Submit Form
                        </button>
                    <?php } ?>
                    <?php
                    if ($reqq->rowCount() > 0) {
                        ?>
                        <a href="assigned-requests.php" class="btn btn-sm col-md-6 btn-light text-danger border-danger">Cancel</a>
                        <?php
                    }
                    ?>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
</body>

</html>
<script>
    $('input[type="checkbox"]').change(function () {
        var namex = $(this).attr('name');
        var name = namex.slice(0, -5);
        if ($(this).prop("checked") == true) {
            var catg = $(this).val();
            if ($("input[name='" + name + "']").val().includes(catg) == true) {
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val());
            } else {
                if ($("input[name='" + name + "']").val() == '') {
                    $("input[name='" + name + "']").val($("input[name='" + name + "']").val() + catg);
                } else {
                    $("input[name='" + name + "']").val($("input[name='" + name + "']").val() + ' - ' + catg);
                }
            }
        } else if ($(this).prop("checked") == false) {
            var catg = $(this).val();
            if ($("input[name='" + name + "']").val().includes(catg) == true) {
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(' - ' + catg, ''));
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(catg + ' - ', ''));
                $("input[name='" + name + "']").val($("input[name='" + name + "']").val().replace(catg, ''));
            }
        }
    });

</script>
<?php
if (isset($_POST['submit'])) {
    $reqid = $_GET['reqid'];
    $query = $a->con->prepare("insert into uniformmitigation(user, reqid, inspectiondate, ownername, contactperson, address, homephone, city, zip, workphone, country, cellphone, insurancecompany, policyno, yearofhome, storiesno, email, buildingcodea, bcyearbuilt, bcyearpermitdatemonth, bcyearpermitdateday, bcyearpermitdateyear, buildingcodeb, bcyearbuilt2, bcyearpermitdatemonth2, bcyearpermitdateday2, bcyearpermitdateyear2, buildingcodec, rcdescription, roofcoveringtype1, rcasphaltfiberglassshingledatemonth, rcasphaltfiberglassshingledateday, rcasphaltfiberglassshingledateyear, rcasphaltfiberglassshingleapprovalno, rcasphaltfiberglassshingleinstallationyear, roofcoveringconspliance1, roofcoveringtype2, rcconcreteclattiledatemonth, rcconcreteclattiledateday, rcconcreteclattiledateyear, rcconcreteclattileapprovalno, rcconcreteclattileinstallationyear, roofcoveringconspliance2, roofcoveringtype3, rcmetaldatemonth, rcmetaldateday, rcmetaldateyear, rcmetalapprovalno, rcmetalinstallationyear, roofcoveringconspliance3, roofcoveringtype4, rcbuiltupdatemonth, rcbuiltupdateday, rcbuiltupdateyear, rcbuiltupapprovalno, rcbuiltupinstallationyear, roofcoveringconspliance4, roofcoveringtype5, rcmembranedatemonth, rcmembranedateday, rcmembranedateyear, rcmembraneapprovalno, rcmembraneinstallationyear, roofcoveringconspliance5, roofcoveringtype6, rcotherdatemonth, rcotherdateday, rcotherdateyear, rcotherapprovalno, rcotherinstallationyear, roofcoveringconspliance6, roofcoveringa, roofcoveringb, roofcoveringc, roofcoveringd, roofdecka, roofdeckb, roofdeckc, inspectorsinitials, propertyaddress, roofdeckd, roofdecke, roofdeckf, roofdeckg, rooftowalla, rooftowalla1, rooftowalla2, rooftowalla3, rooftowalla4, rooftowallb, rooftowallb1, rooftowallb2, rooftowallc, rooftowalld, rooftowalle, rooftowallf, rooftowallg, rooftowallh, roofgeometrya, hrtotalfeatures, hrtotalperimeter, roofgeometryb, frroofslope, frtotalarea, roofgeometryc, waterresistancea, waterresistanceb, waterresistancec, inspectorsinitials2, propertyaddress2, windowdoors1, garagedoors1, skylights1, glassback1, entrydoors1, garagedoors21, windowdoors2, garagedoors2, skylights2, glassback2, entrydoors2, garagedoors22, windowdoors3, garagedoors3, skylights3, glassback3, entrydoors3, garagedoors23, windowdoors4, garagedoors4, skylights4, glassback4, entrydoors4, garagedoors24, windowdoors5, garagedoors5, skylights5, glassback5, entrydoors5, garagedoors25, windowdoors6, garagedoors6, skylights6, glassback6, entrydoors6, garagedoors26, windowdoors7, garagedoors7, skylights7, glassback7, entrydoors7, garagedoors27, windowdoors8, garagedoors8, skylights8, glassback8, entrydoors8, garagedoors28, openingprotectiona, openingprotectiona1, openingprotectiona2, openingprotectiona3, openingprotectionb, openingprotectionb1, openingprotectionb2, openingprotectionb3, openingprotectionc, openingprotectionc1, openingprotectionc2, openingprotectionc3, inspectorsinitials3, propertyaddress3, openingprotectionn, openingprotectionn1, openingprotectionn2, openingprotectionn3, openingprotectionx, miinspectorname, milicensetype, milicenseno, inspectorsinitials4, propertyaddress4, qualifiedinspector1, qualifiedinspector2, qualifiedinspector3, qualifiedinspector4, qualifiedinspector5, qualifiedinspector6, printname, inspectorprintname, qualifiedinspectorsign, inspectorsigndate, homeownersign, homeownersigndate, inspectorsinitials5, propertyaddress5) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $status = $query->execute([$_SESSION['user'], $reqid, $_POST['inspectiondate'], $_POST['ownername'], $_POST['contactperson'], $_POST['address'], $_POST['homephone'], $_POST['city'], $_POST['zip'], $_POST['workphone'], $_POST['country'], $_POST['cellphone'], $_POST['insurancecompany'], $_POST['policyno'], $_POST['yearofhome'], $_POST['storiesno'], $_POST['email'], $_POST['buildingcodea'], $_POST['bcyearbuilt'], $_POST['bcyearpermitdatemonth'], $_POST['bcyearpermitdateday'], $_POST['bcyearpermitdateyear'], $_POST['buildingcodeb'], $_POST['bcyearbuilt2'], $_POST['bcyearpermitdatemonth2'], $_POST['bcyearpermitdateday2'], $_POST['bcyearpermitdateyear2'], $_POST['buildingcodec'], $_POST['rcdescription'], $_POST['roofcoveringtype1'], $_POST['rcasphaltfiberglassshingledatemonth'], $_POST['rcasphaltfiberglassshingledateday'], $_POST['rcasphaltfiberglassshingledateyear'], $_POST['rcasphaltfiberglassshingleapprovalno'], $_POST['rcasphaltfiberglassshingleinstallationyear'], $_POST['roofcoveringconspliance1'], $_POST['roofcoveringtype2'], $_POST['rcconcreteclattiledatemonth'], $_POST['rcconcreteclattiledateday'], $_POST['rcconcreteclattiledateyear'], $_POST['rcconcreteclattileapprovalno'], $_POST['rcconcreteclattileinstallationyear'], $_POST['roofcoveringconspliance2'], $_POST['roofcoveringtype3'], $_POST['rcmetaldatemonth'], $_POST['rcmetaldateday'], $_POST['rcmetaldateyear'], $_POST['rcmetalapprovalno'], $_POST['rcmetalinstallationyear'], $_POST['roofcoveringconspliance3'], $_POST['roofcoveringtype4'], $_POST['rcbuiltupdatemonth'], $_POST['rcbuiltupdateday'], $_POST['rcbuiltupdateyear'], $_POST['rcbuiltupapprovalno'], $_POST['rcbuiltupinstallationyear'], $_POST['roofcoveringconspliance4'], $_POST['roofcoveringtype5'], $_POST['rcmembranedatemonth'], $_POST['rcmembranedateday'], $_POST['rcmembranedateyear'], $_POST['rcmembraneapprovalno'], $_POST['rcmembraneinstallationyear'], $_POST['roofcoveringconspliance5'], $_POST['roofcoveringtype6'], $_POST['rcotherdatemonth'], $_POST['rcotherdateday'], $_POST['rcotherdateyear'], $_POST['rcotherapprovalno'], $_POST['rcotherinstallationyear'], $_POST['roofcoveringconspliance6'], $_POST['roofcoveringa'], $_POST['roofcoveringb'], $_POST['roofcoveringc'], $_POST['roofcoveringd'], $_POST['roofdecka'], $_POST['roofdeckb'], $_POST['roofdeckc'], $_POST['inspectorsinitials'], $_POST['propertyaddress'], $_POST['roofdeckd'], $_POST['roofdecke'], $_POST['roofdeckf'], $_POST['roofdeckg'], $_POST['rooftowalla'], $_POST['rooftowalla1'], $_POST['rooftowalla2'], $_POST['rooftowalla3'], $_POST['rooftowalla4'], $_POST['rooftowallb'], $_POST['rooftowallb1'], $_POST['rooftowallb2'], $_POST['rooftowallc'], $_POST['rooftowalld'], $_POST['rooftowalle'], $_POST['rooftowallf'], $_POST['rooftowallg'], $_POST['rooftowallh'], $_POST['roofgeometrya'], $_POST['hrtotalfeatures'], $_POST['hrtotalperimeter'], $_POST['roofgeometryb'], $_POST['frroofslope'], $_POST['frtotalarea'], $_POST['roofgeometryc'], $_POST['waterresistancea'], $_POST['waterresistanceb'], $_POST['waterresistancec'], $_POST['inspectorsinitials2'], $_POST['propertyaddress2'], $_POST['windowdoors1'], $_POST['garagedoors1'], $_POST['skylights1'], $_POST['glassback1'], $_POST['entrydoors1'], $_POST['garagedoors21'], $_POST['windowdoors2'], $_POST['garagedoors2'], $_POST['skylights2'], $_POST['glassback2'], $_POST['entrydoors2'], $_POST['garagedoors22'], $_POST['windowdoors3'], $_POST['garagedoors3'], $_POST['skylights3'], $_POST['glassback3'], $_POST['entrydoors3'], $_POST['garagedoors23'], $_POST['windowdoors4'], $_POST['garagedoors4'], $_POST['skylights4'], $_POST['glassback4'], $_POST['entrydoors4'], $_POST['garagedoors24'], $_POST['windowdoors5'], $_POST['garagedoors5'], $_POST['skylights5'], $_POST['glassback5'], $_POST['entrydoors5'], $_POST['garagedoors25'], $_POST['windowdoors6'], $_POST['garagedoors6'], $_POST['skylights6'], $_POST['glassback6'], $_POST['entrydoors6'], $_POST['garagedoors26'], $_POST['windowdoors7'], $_POST['garagedoors7'], $_POST['skylights7'], $_POST['glassback7'], $_POST['entrydoors7'], $_POST['garagedoors27'], $_POST['windowdoors8'], $_POST['garagedoors8'], $_POST['skylights8'], $_POST['glassback8'], $_POST['entrydoors8'], $_POST['garagedoors28'], $_POST['openingprotectiona'], $_POST['openingprotectiona1'], $_POST['openingprotectiona2'], $_POST['openingprotectiona3'], $_POST['openingprotectionb'], $_POST['openingprotectionb1'], $_POST['openingprotectionb2'], $_POST['openingprotectionb3'], $_POST['openingprotectionc'], $_POST['openingprotectionc1'], $_POST['openingprotectionc2'], $_POST['openingprotectionc3'], $_POST['inspectorsinitials3'], $_POST['propertyaddress3'], $_POST['openingprotectionn'], $_POST['openingprotectionn1'], $_POST['openingprotectionn2'], $_POST['openingprotectionn3'], $_POST['openingprotectionx'], $_POST['miinspectorname'], $_POST['milicensetype'], $_POST['milicenseno'], $_POST['inspectorsinitials4'], $_POST['propertyaddress4'], $_POST['qualifiedinspector1'], $_POST['qualifiedinspector2'], $_POST['qualifiedinspector3'], $_POST['qualifiedinspector4'], $_POST['qualifiedinspector5'], $_POST['qualifiedinspector6'], $_POST['printname'], $_POST['inspectorprintname'], $_POST['qualifiedinspectorsign'], $_POST['inspectorsigndate'], $_POST['homeownersign'], $_POST['homeownersigndate'], $_POST['inspectorsinitials5'], $_POST['propertyaddress5']]);
    if ($status) {
        $doctypearr = ['Photo Attachments'];
        $icn = 0;
        $class = '';
        while ($icn < (count($doctypearr) * 4)) {
            if ($_POST['imagename' . $icn] != '') {
                $img = $_FILES['img' . $icn]['tmp_name'];
                $pic = $_FILES['img' . $icn]['name'];
                if ($pic != '') {
                    $ext = pathinfo($pic, PATHINFO_EXTENSION);
                    $path = 'assets/images/uniformmitigation/' . sha1($pic . date("Y-m-d H:i:s")) . '.' . $ext;
                    $addimageq = $a->con->prepare("insert into uniformmitigationfiles(reqid,imgno,name,file,user) values(?,?,?,?,?)");
                    $addimageq->execute([$reqid, $icn, $_POST['imagename' . $icn], $path, $_SESSION['user']]);
                    copy($img, $path);
                }
            }
            $icn++;
        }

        $_SESSION['form'] = 'added';
        echo '<script> window.location = "uniform-mitigation-form.php?reqid=' . $_GET['reqid'] . '&x=' . $_GET['reqid'] . '&download=pdf"; </script>';
    }
}
if (isset($_POST['update'])) {
    $reqid = $_GET['reqid'];
    $query = $a->con->prepare("update uniformmitigation set inspectiondate=?, ownername=?, contactperson=?, address=?, homephone=?, city=?, zip=?, workphone=?, country=?, cellphone=?, insurancecompany=?, policyno=?, yearofhome=?, storiesno=?, email=?, buildingcodea=?, bcyearbuilt=?, bcyearpermitdatemonth=?, bcyearpermitdateday=?, bcyearpermitdateyear=?, buildingcodeb=?, bcyearbuilt2=?, bcyearpermitdatemonth2=?, bcyearpermitdateday2=?, bcyearpermitdateyear2=?, buildingcodec=?, rcdescription=?, roofcoveringtype1=?, rcasphaltfiberglassshingledatemonth=?, rcasphaltfiberglassshingledateday=?, rcasphaltfiberglassshingledateyear=?, rcasphaltfiberglassshingleapprovalno=?, rcasphaltfiberglassshingleinstallationyear=?, roofcoveringconspliance1=?, roofcoveringtype2=?, rcconcreteclattiledatemonth=?, rcconcreteclattiledateday=?, rcconcreteclattiledateyear=?, rcconcreteclattileapprovalno=?, rcconcreteclattileinstallationyear=?, roofcoveringconspliance2=?, roofcoveringtype3=?, rcmetaldatemonth=?, rcmetaldateday=?, rcmetaldateyear=?, rcmetalapprovalno=?, rcmetalinstallationyear=?, roofcoveringconspliance3=?, roofcoveringtype4=?, rcbuiltupdatemonth=?, rcbuiltupdateday=?, rcbuiltupdateyear=?, rcbuiltupapprovalno=?, rcbuiltupinstallationyear=?, roofcoveringconspliance4=?, roofcoveringtype5=?, rcmembranedatemonth=?, rcmembranedateday=?, rcmembranedateyear=?, rcmembraneapprovalno=?, rcmembraneinstallationyear=?, roofcoveringconspliance5=?, roofcoveringtype6=?, rcotherdatemonth=?, rcotherdateday=?, rcotherdateyear=?, rcotherapprovalno=?, rcotherinstallationyear=?, roofcoveringconspliance6=?, roofcoveringa=?, roofcoveringb=?, roofcoveringc=?, roofcoveringd=?, roofdecka=?, roofdeckb=?, roofdeckc=?, inspectorsinitials=?, propertyaddress=?, roofdeckd=?, roofdecke=?, roofdeckf=?, roofdeckg=?, rooftowalla=?, rooftowalla1=?, rooftowalla2=?, rooftowalla3=?, rooftowalla4=?, rooftowallb=?, rooftowallb1=?, rooftowallb2=?, rooftowallc=?, rooftowalld=?, rooftowalle=?, rooftowallf=?, rooftowallg=?, rooftowallh=?, roofgeometrya=?, hrtotalfeatures=?, hrtotalperimeter=?, roofgeometryb=?, frroofslope=?, frtotalarea=?, roofgeometryc=?, waterresistancea=?, waterresistanceb=?, waterresistancec=?, inspectorsinitials2=?, propertyaddress2=?, windowdoors1=?, garagedoors1=?, skylights1=?, glassback1=?, entrydoors1=?, garagedoors21=?, windowdoors2=?, garagedoors2=?, skylights2=?, glassback2=?, entrydoors2=?, garagedoors22=?, windowdoors3=?, garagedoors3=?, skylights3=?, glassback3=?, entrydoors3=?, garagedoors23=?, windowdoors4=?, garagedoors4=?, skylights4=?, glassback4=?, entrydoors4=?, garagedoors24=?, windowdoors5=?, garagedoors5=?, skylights5=?, glassback5=?, entrydoors5=?, garagedoors25=?, windowdoors6=?, garagedoors6=?, skylights6=?, glassback6=?, entrydoors6=?, garagedoors26=?, windowdoors7=?, garagedoors7=?, skylights7=?, glassback7=?, entrydoors7=?, garagedoors27=?, windowdoors8=?, garagedoors8=?, skylights8=?, glassback8=?, entrydoors8=?, garagedoors28=?, openingprotectiona=?, openingprotectiona1=?, openingprotectiona2=?, openingprotectiona3=?, openingprotectionb=?, openingprotectionb1=?, openingprotectionb2=?, openingprotectionb3=?, openingprotectionc=?, openingprotectionc1=?, openingprotectionc2=?, openingprotectionc3=?, inspectorsinitials3=?, propertyaddress3=?, openingprotectionn=?, openingprotectionn1=?, openingprotectionn2=?, openingprotectionn3=?, openingprotectionx=?, miinspectorname=?, milicensetype=?, milicenseno=?, inspectorsinitials4=?, propertyaddress4=?, qualifiedinspector1=?, qualifiedinspector2=?, qualifiedinspector3=?, qualifiedinspector4=?, qualifiedinspector5=?, qualifiedinspector6=?, printname=?, inspectorprintname=?, qualifiedinspectorsign=?, inspectorsigndate=?, homeownersign=?, homeownersigndate=?, inspectorsinitials5=?, propertyaddress5=?,lastupdatedat=?,lastupdatedby=? where reqid = ?");
    $status = $query->execute([$_POST['inspectiondate'], $_POST['ownername'], $_POST['contactperson'], $_POST['address'], $_POST['homephone'], $_POST['city'], $_POST['zip'], $_POST['workphone'], $_POST['country'], $_POST['cellphone'], $_POST['insurancecompany'], $_POST['policyno'], $_POST['yearofhome'], $_POST['storiesno'], $_POST['email'], $_POST['buildingcodea'], $_POST['bcyearbuilt'], $_POST['bcyearpermitdatemonth'], $_POST['bcyearpermitdateday'], $_POST['bcyearpermitdateyear'], $_POST['buildingcodeb'], $_POST['bcyearbuilt2'], $_POST['bcyearpermitdatemonth2'], $_POST['bcyearpermitdateday2'], $_POST['bcyearpermitdateyear2'], $_POST['buildingcodec'], $_POST['rcdescription'], $_POST['roofcoveringtype1'], $_POST['rcasphaltfiberglassshingledatemonth'], $_POST['rcasphaltfiberglassshingledateday'], $_POST['rcasphaltfiberglassshingledateyear'], $_POST['rcasphaltfiberglassshingleapprovalno'], $_POST['rcasphaltfiberglassshingleinstallationyear'], $_POST['roofcoveringconspliance1'], $_POST['roofcoveringtype2'], $_POST['rcconcreteclattiledatemonth'], $_POST['rcconcreteclattiledateday'], $_POST['rcconcreteclattiledateyear'], $_POST['rcconcreteclattileapprovalno'], $_POST['rcconcreteclattileinstallationyear'], $_POST['roofcoveringconspliance2'], $_POST['roofcoveringtype3'], $_POST['rcmetaldatemonth'], $_POST['rcmetaldateday'], $_POST['rcmetaldateyear'], $_POST['rcmetalapprovalno'], $_POST['rcmetalinstallationyear'], $_POST['roofcoveringconspliance3'], $_POST['roofcoveringtype4'], $_POST['rcbuiltupdatemonth'], $_POST['rcbuiltupdateday'], $_POST['rcbuiltupdateyear'], $_POST['rcbuiltupapprovalno'], $_POST['rcbuiltupinstallationyear'], $_POST['roofcoveringconspliance4'], $_POST['roofcoveringtype5'], $_POST['rcmembranedatemonth'], $_POST['rcmembranedateday'], $_POST['rcmembranedateyear'], $_POST['rcmembraneapprovalno'], $_POST['rcmembraneinstallationyear'], $_POST['roofcoveringconspliance5'], $_POST['roofcoveringtype6'], $_POST['rcotherdatemonth'], $_POST['rcotherdateday'], $_POST['rcotherdateyear'], $_POST['rcotherapprovalno'], $_POST['rcotherinstallationyear'], $_POST['roofcoveringconspliance6'], $_POST['roofcoveringa'], $_POST['roofcoveringb'], $_POST['roofcoveringc'], $_POST['roofcoveringd'], $_POST['roofdecka'], $_POST['roofdeckb'], $_POST['roofdeckc'], $_POST['inspectorsinitials'], $_POST['propertyaddress'], $_POST['roofdeckd'], $_POST['roofdecke'], $_POST['roofdeckf'], $_POST['roofdeckg'], $_POST['rooftowalla'], $_POST['rooftowalla1'], $_POST['rooftowalla2'], $_POST['rooftowalla3'], $_POST['rooftowalla4'], $_POST['rooftowallb'], $_POST['rooftowallb1'], $_POST['rooftowallb2'], $_POST['rooftowallc'], $_POST['rooftowalld'], $_POST['rooftowalle'], $_POST['rooftowallf'], $_POST['rooftowallg'], $_POST['rooftowallh'], $_POST['roofgeometrya'], $_POST['hrtotalfeatures'], $_POST['hrtotalperimeter'], $_POST['roofgeometryb'], $_POST['frroofslope'], $_POST['frtotalarea'], $_POST['roofgeometryc'], $_POST['waterresistancea'], $_POST['waterresistanceb'], $_POST['waterresistancec'], $_POST['inspectorsinitials2'], $_POST['propertyaddress2'], $_POST['windowdoors1'], $_POST['garagedoors1'], $_POST['skylights1'], $_POST['glassback1'], $_POST['entrydoors1'], $_POST['garagedoors21'], $_POST['windowdoors2'], $_POST['garagedoors2'], $_POST['skylights2'], $_POST['glassback2'], $_POST['entrydoors2'], $_POST['garagedoors22'], $_POST['windowdoors3'], $_POST['garagedoors3'], $_POST['skylights3'], $_POST['glassback3'], $_POST['entrydoors3'], $_POST['garagedoors23'], $_POST['windowdoors4'], $_POST['garagedoors4'], $_POST['skylights4'], $_POST['glassback4'], $_POST['entrydoors4'], $_POST['garagedoors24'], $_POST['windowdoors5'], $_POST['garagedoors5'], $_POST['skylights5'], $_POST['glassback5'], $_POST['entrydoors5'], $_POST['garagedoors25'], $_POST['windowdoors6'], $_POST['garagedoors6'], $_POST['skylights6'], $_POST['glassback6'], $_POST['entrydoors6'], $_POST['garagedoors26'], $_POST['windowdoors7'], $_POST['garagedoors7'], $_POST['skylights7'], $_POST['glassback7'], $_POST['entrydoors7'], $_POST['garagedoors27'], $_POST['windowdoors8'], $_POST['garagedoors8'], $_POST['skylights8'], $_POST['glassback8'], $_POST['entrydoors8'], $_POST['garagedoors28'], $_POST['openingprotectiona'], $_POST['openingprotectiona1'], $_POST['openingprotectiona2'], $_POST['openingprotectiona3'], $_POST['openingprotectionb'], $_POST['openingprotectionb1'], $_POST['openingprotectionb2'], $_POST['openingprotectionb3'], $_POST['openingprotectionc'], $_POST['openingprotectionc1'], $_POST['openingprotectionc2'], $_POST['openingprotectionc3'], $_POST['inspectorsinitials3'], $_POST['propertyaddress3'], $_POST['openingprotectionn'], $_POST['openingprotectionn1'], $_POST['openingprotectionn2'], $_POST['openingprotectionn3'], $_POST['openingprotectionx'], $_POST['miinspectorname'], $_POST['milicensetype'], $_POST['milicenseno'], $_POST['inspectorsinitials4'], $_POST['propertyaddress4'], $_POST['qualifiedinspector1'], $_POST['qualifiedinspector2'], $_POST['qualifiedinspector3'], $_POST['qualifiedinspector4'], $_POST['qualifiedinspector5'], $_POST['qualifiedinspector6'], $_POST['printname'], $_POST['inspectorprintname'], $_POST['qualifiedinspectorsign'], $_POST['inspectorsigndate'], $_POST['homeownersign'], $_POST['homeownersigndate'], $_POST['inspectorsinitials5'], $_POST['propertyaddress5'], date("Y-m-d H:i:s"), $_SESSION['user'], $_GET['reqid']]);
    if ($status) {

        $doctypearr = ['Photo Attachments'];
        $icn = 0;
        $class = '';
        while ($icn < (count($doctypearr) * 4)) {
            if ($_POST['imagename' . $icn] != '') {
                $img = $_FILES['img' . $icn]['tmp_name'];
                $pic = $_FILES['img' . $icn]['name'];
                if ($pic != '') {
                    $ext = pathinfo($pic, PATHINFO_EXTENSION);
                    $path = 'assets/images/uniformmitigation/' . sha1($pic . date("Y-m-d H:i:s")) . '.' . $ext;
                    $addimageq = $a->con->prepare("insert into uniformmitigationfiles(reqid,imgno,name,file,user) values(?,?,?,?,?)");
                    $addimageq->execute([$reqid, $icn, $_POST['imagename' . $icn], $path, $_SESSION['user']]);
                    copy($img, $path);
                }
            }
            $icn++;
        }

        $_SESSION['contact'] = 'updated';
        echo '<script> window.location = "uniform-mitigation-form.php?reqid=' . $_GET['reqid'] . '&x=' . $_GET['reqid'] . '&download=pdf"; </script>';
    }
}
?>

<script>
    $(".nav.uniformmitigation").addClass('active-link');

    $(".file-upload-img input").change(function (e) {
        var namex = $(this).attr("name");
        var name = namex.slice(3);
        $(".image-box" + name).addClass('active');
        var filename = $(this).val();
        if (/^\s*$/.test(filename)) {
            $(".image-box" + name).removeClass('active');
            $(".image-box" + name + " img").attr('src', 'https://static.thenounproject.com/png/971625-200.png');
        } else {
            $(".image-box" + name).addClass('active');
            var files = e.target.files,
                filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function (e) {
                    var file = e.target;
                    $(".image-box" + name + " img").attr('src', e.target.result);
                    $(".image-box" + name + " .remove-extra").css('display', 'flex');
                });
                fileReader.readAsDataURL(f);
            }
        }
    });

    $(".remove-extra").click(function () {
        var i = $(this).attr('id');
        $(".image-box" + i).removeClass('active');
        $(".image-box" + i + " img").attr('src', 'https://static.thenounproject.com/png/971625-200.png');
        $("#" + i).css('display', 'none');

    });

</script>
<?php if ($reqq->rowCount() > 0 && isset($_GET['download'])) { ?>
    <style>
        .main-border {
            border: 0px !important;
            padding: 0px !important;
        }

        .file-upload-img.active .file-select .file-select-name img {
            height: 240px !important;
        }

        label {
            color: black !important;
            font-size: 10px;
        }

        label.border {
            color: black !important;
            font-size: 10px;
        }

        input.form-control {
            height: 16px !important;
            padding: .0rem .35rem !important;
        }

        label.form-control {
            padding: .1rem .01rem !important;
        }

        label {
            padding-bottom: 0px !important;
        }

        label.p-2 {
            padding-top: .1rem !important;
            padding-bottom: .1rem !important;
            margin-bottom: 0px !important;
        }

        .p-1 {
            padding: .15rem !important;
        }

        .table td,
        .table th {
            padding: .25rem !important;
        }

        .input-group-prepend {
            height: 16px !important;
        }

        .input-group-text {
            color: black !important;
            font-size: 12px;
        }

        .extra {
            font-size: 85% !important;
            color: black !important;
        }

        input,
        textarea {
            font-size: 12px !important;
        }

        .form-group {
            margin-bottom: 0.3rem !important;
        }

        .col-12.p-3 {
            padding: 0.4rem !important;
        }

        .p-3.bg-secondary.text-black {
            padding: 0.4rem !important;
        }

        label.px-2 {
            margin-bottom: 0px !important;
            font-weight: 400 !important;
            font-size: 11px;
            color: #495057;
        }


        .main-border table {
            font-size: 0.7rem !important;
        }

        .main-border h5 {
            font-size: 0.9rem !important;
        }

        .main-border h4 {
            font-size: 1rem !important;
        }

        .main-border h6,
        .main-border .h6 {
            font-size: 0.74rem;
        }

        /*
        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        .images input {
            border: 0px !important;
            border-bottom: 1px solid #ccc !important;
        }
    */

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js"></script>
    <script type="text/javascript">
        $(".prepare-progress").css('display', 'block');
        filename = 'Uniform-Mitigation-Inspection-<?php echo date("d-m-Y"); ?>.pdf'
        var element = document.getElementById('pdfcontent');
        html2pdf(element, {
            margin: [10, 0, 10, 0], //top, left, buttom, right
            filename: filename,
            image: {
                type: 'jpeg',
                quality: 1
            },
            html2canvas: {
                scale: 2,
                bottom: 20,
                letterRendering: true
            },
            jsPDF: {
                unit: 'mm',
                format: 'a3',
                orientation: 'portrait'
            },
            pagebreak: {
                mode: ['avoid-all', 'css', 'legacy']
            }
        });

    </script>
<?php } ?>
