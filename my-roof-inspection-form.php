<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Agency') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>Roof Inspection Form | <?php echo $site['name'] ?></title>
    <style>
        #main-page,
        #first-page,
        #second-page,
        #third-page,
        #forth-page {
            background: white;
        }

        .input-group-text {
            background-color: transparent;
            padding: .275rem .55rem;
            border: 0px;
            font-size: 14px;
        }

        label.border {
            background-color: transparent;
            border: 0px !important;
            font-size: 14px;
        }

        input {
            font-size: 15px !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        input[type="text"] {
            border: 0px !important;
            border-bottom: 1px solid #707070 !important;
        }

        .images input {
            border: 0px !important;
            border-bottom: 1px solid #ccc !important;
        }

        label.px-2 {
            font-weight: 400 !important;
            font-size: 14px;
            color: #495057;
        }

        input,
        textarea,
        .form-control {
            pointer-events: none !important;
        }

        .main-border {
            border: 0px !important;
            padding: 0px !important;
        }

        .file-upload-img.active .file-select .file-select-name img {
            height: 260px !important;
            object-fit: contain;
        }

        .file-upload-img .file-select {
            height: 260px !important;
        }

        .images input.form-control {
            height: 40px !important;
            padding: .25rem .55rem !important;
            font-size: 15px !important;
        }

        .big-inputs input.form-control {
            height: 30px !important;
            padding: .25rem .55rem !important;
            font-size: 15px !important;
        }

        .big-inputs label {
            color: #313131 !important;
            font-size: 13px;
            font-weight: 400 !important;
        }

        label {
            color: black !important;
            font-size: 10px;
        }

        label.border {
            color: black !important;
            font-size: 10px;
        }

        input.form-control {
            height: 16px !important;
            padding: .0rem .35rem !important;
        }

        label.form-control {
            padding: .1rem .01rem !important;
        }

        label {
            padding-bottom: 0px !important;
        }

        label.p-2 {
            padding-top: .1rem !important;
            padding-bottom: .1rem !important;
            margin-bottom: 0px !important;
        }

        .p-1 {
            padding: .15rem !important;
        }

        .table td,
        .table th {
            padding: .25rem !important;
        }

        .input-group-prepend {
            height: 16px !important;
        }

        .input-group-text {
            color: black !important;
            font-size: 12px;
        }

        .extra {
            font-size: 85% !important;
            color: black !important;
        }

        input,
        textarea {
            font-size: 12px !important;
        }

        .form-group {
            margin-bottom: 0.3rem !important;
        }

        .col-12.p-3 {
            padding: 0.4rem !important;
        }

        .p-3.bg-secondary.text-black {
            padding: 0.4rem !important;
        }

        label.px-2 {
            margin-bottom: 0px !important;
            font-weight: 400 !important;
            font-size: 11px;
            color: #495057;
        }


        .main-border table {
            font-size: 0.7rem !important;
        }

        .main-border h5 {
            font-size: 0.9rem !important;
        }

        .main-border h4 {
            font-size: 1rem !important;
        }

        .main-border h6,
        .main-border .h6 {
            font-size: 0.74rem;
        }

    </style>
</head>

<body>
<?php
$reqq = $a->con->prepare("select * from roofinspection where reqid = ?");
$reqq->execute([$_GET['reqid']]);
$r = $reqq->fetch();
if (isset($_GET['reqid'])) {
    $requestq = $a->con->prepare("select * from requests where reqid = ?");
    $requestq->execute([$_GET['reqid']]);
    $request = $requestq->fetch();

} else {
    echo '<script> window.location = "/index.php" </script>';
}
?>
<div class="px-4 mb-3 prepare-progress" style="display:none;">
    <div class="progress">
        <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:10%">Preparing PDF</div>
    </div>
    <style>
        .progress .progress-bar {
            animation: preparing 5s forwards;
        }

        @keyframes preparing {
            from {
                width: 10%;
            }

            to {
                width: 100%;
            }
        }

    </style>
</div>
<div class="px-4 py-3" id="pdfcontent">
    <div class="nav justify-content-between text-center mb-3">
        <div>
            <img src="assets/images/favicon.jpg" width="50">
        </div>
        <div>
            <h2 class="text-main font-weight-bold mb-0">Windmitigations.com, LLC</h2>
            <h3 class="text-black font-weight-bold mb-0">Roof Inspection Form</h3>
        </div>
        <div>
            <img src="assets/images/favicon.jpg" width="50">
        </div>
    </div>
    <div class="col-12 p-0 p-3 bg-white py-4 border rounded-10 shadow-sm main-border">
        <form method="post" enctype="multipart/form-data" class="nav bg-white pt-3 justify-content-between">
            <div id="main-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group nav justify-content-start">
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Insured/Applicant Name:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['applicantname'];
                        } else if (isset($_POST['applicantname'])) {
                            echo $_POST['applicantname'];
                        } ?>" maxlength="255" name="applicantname">
                    </div>
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Application / Policy #:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['policyno'];
                        } else if (isset($_POST['policyno'])) {
                            echo $_POST['policyno'];
                        } ?>" maxlength="255" name="policyno">
                    </div>
                    <div class="form-group input-group col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Address Inspected:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['addressinspected'];
                        } else if (isset($_POST['addressinspected'])) {
                            echo $_POST['addressinspected'];
                        } ?>" maxlength="255" name="addressinspected">
                    </div>
                    <div class="form-group input-group col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Date of Inspection:</span>
                        </div>
                        <input class="form-control" type="text" value="<?php if ($reqq->rowCount() > 0) {
                            echo $r['dateinspected'];
                        } else if (isset($_POST['dateinspected'])) {
                            echo $_POST['dateinspected'];
                        } ?>" maxlength="255" name="dateinspected">
                    </div>
                </div>
                <div class="form-group col-md-12 mt-3">
                    <div class="col-12 px-3 py-2 border h6">This sample Roof Inspection Form (or a similar form) must be
                        completed and signed by a Florida-licensed professional. The form will not be accepted without
                        the dated signature of one of the following appropriately licensed inspectors:
                        <div class="container text-center mt-2"><span class="font-weight-900">&bull;</span> General,
                            residential, building or roofing contractor • Building code inspector • Florida-licensed
                            home inspector
                        </div>
                        <br>Note: This form does not verify loss mitigation features. Use Uniform Mitigation
                        Verification Inspection Form 01R-B1-1802.
                    </div>
                </div>
                <div class="form-group col-md-12 mt-1">
                    <div class="col-12 px-3 py-2 border h6">Be advised that Underwriting will rely on the information in
                        this sample form, or a similar form, that is obtained from the Florida licensed professional if
                        yiur choice. The information is used only to determine insurablitiy and is not a warranty or
                        assurance of the suitability, fitness or longevity of the roof inspected.
                    </div>
                </div>
                <br>
            </div>
            <div id="first-page" class="nav justify-content-between col-12 p-0">
                <div class="form-group mt-4 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Roof <small>(with photos of each roof slope, this section
                                    can take the place of the Roof Inspection Form.)</small></h4>
                        </div>
                        <div class="col-12">
                            <div class="row border-bottom border-secondary">
                                <div class="col-md-6 p-3 border-right border-secondary">
                                    <h5 class="font-weight-600">Predominant Roof</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Covering Material:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofcoveringmaterial'];
                                               } else if (isset($_POST['roofcoveringmaterial'])) {
                                                   echo $_POST['roofcoveringmaterial'];
                                               } ?>" maxlength="255" name="roofcoveringmaterial">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Roof Age (Years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofage'];
                                               } else if (isset($_POST['roofage'])) {
                                                   echo $_POST['roofage'];
                                               } ?>" maxlength="255" name="roofage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Remaining useful life (years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofusefullife'];
                                               } else if (isset($_POST['roofusefullife'])) {
                                                   echo $_POST['roofusefullife'];
                                               } ?>" maxlength="255" name="roofusefullife">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last roofing permit:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['dateoflastroofing'];
                                               } else if (isset($_POST['dateoflastroofing'])) {
                                                   echo $_POST['dateoflastroofing'];
                                               } ?>" maxlength="255" name="dateoflastroofing">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last update:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['rooflastupdate'];
                                               } else if (isset($_POST['rooflastupdate'])) {
                                                   echo $_POST['rooflastupdate'];
                                               } ?>" maxlength="255" name="rooflastupdate">
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">If updated (check one):</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofreplacementtype"
                                                                                      value="Full replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofreplacementtype'] == 'Full replacement') ? 'checked' : '';
                                                    } ?>> Full replacement</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofreplacementtype"
                                                                                      value="Partial replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofreplacementtype'] == 'Partial replacement') ? 'checked' : '';
                                                    } ?>> Partial replacement</label>
                                            </div>
                                            <div class="form-group input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">% of replacement:</span>
                                                </div>
                                                <input class="form-control" type="text"
                                                       value="<?php if ($reqq->rowCount() > 0) {
                                                           echo $r['roofreplacementprcntg'];
                                                       } else if (isset($_POST['roofreplacementprcntg'])) {
                                                           echo $_POST['roofreplacementprcntg'];
                                                       } ?>" maxlength="255" name="roofreplacementprcntg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">Overall condition:</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofoverallcondition"
                                                                                      value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofoverallcondition'] == 'Satisfactory') ? 'checked' : '';
                                                    } ?>> Satisfactory</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofoverallcondition"
                                                                                      value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofoverallcondition'] == 'Unsatisfactory') ? 'checked' : '';
                                                    } ?>> Unsatisfactory</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5 class="font-weight-600">Any visible signs of damage / deterioration?</h5>
                                        <h6 class="font-weight-400 font-95">(check all that apply and explain
                                            below)</h6>
                                        <?php
                                        $generalconditionarr = ['Cracking', 'Cupping/curling', 'Excessive granule loss', 'Exposed asphalt', 'Exposed felt', 'Missing/loose/cracked tabs or tiles', 'Soft spots in decking', 'Visible hail damage'];
                                        $gcc = 0;
                                        while ($gcc != count($generalconditionarr)) {
                                            ?>
                                            <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                name="roofvisibledamagecheck"
                                                                                value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                    echo ((strpos($r['roofvisibledamage'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                            <br>
                                            <?php
                                            $gcc++;
                                        }
                                        ?>
                                        <input type="hidden" name="roofvisibledamage"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['roofvisibledamage'];
                                               } ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Any visible signs of leaks</b>:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofvisibleleaks"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofvisibleleaks'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofvisibleleaks"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofvisibleleaks'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Attic/underside of decking:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox" name="roofdecking"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofdecking'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox" name="roofdecking"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofdecking'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Interior ceilings:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofceilings"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofceilings'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="roofceilings"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['roofceilings'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-3">
                                    <h5 class="font-weight-600">Secondary Roof</h5>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Covering Material:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofcoveringmaterial'];
                                               } else if (isset($_POST['sroofcoveringmaterial'])) {
                                                   echo $_POST['sroofcoveringmaterial'];
                                               } ?>" maxlength="255" name="sroofcoveringmaterial">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Roof Age (Years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofage'];
                                               } else if (isset($_POST['sroofage'])) {
                                                   echo $_POST['sroofage'];
                                               } ?>" maxlength="255" name="sroofage">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Remaining useful life (years):</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofusefullife'];
                                               } else if (isset($_POST['sroofusefullife'])) {
                                                   echo $_POST['sroofusefullife'];
                                               } ?>" maxlength="255" name="sroofusefullife">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last roofing permit:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sdateoflastroofing'];
                                               } else if (isset($_POST['sdateoflastroofing'])) {
                                                   echo $_POST['sdateoflastroofing'];
                                               } ?>" maxlength="255" name="sdateoflastroofing">
                                    </div>
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Date of last update:</span>
                                        </div>
                                        <input class="form-control" type="text"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['srooflastupdate'];
                                               } else if (isset($_POST['srooflastupdate'])) {
                                                   echo $_POST['srooflastupdate'];
                                               } ?>" maxlength="255" name="srooflastupdate">
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">If updated (check one):</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofreplacementtype"
                                                                                      value="Full replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofreplacementtype'] == 'Full replacement') ? 'checked' : '';
                                                    } ?>> Full replacement</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofreplacementtype"
                                                                                      value="Partial replacement" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofreplacementtype'] == 'Partial replacement') ? 'checked' : '';
                                                    } ?>> Partial replacement</label>
                                            </div>
                                            <div class="form-group input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">% of replacement:</span>
                                                </div>
                                                <input class="form-control" type="text"
                                                       value="<?php if ($reqq->rowCount() > 0) {
                                                           echo $r['sroofreplacementprcntg'];
                                                       } else if (isset($_POST['sroofreplacementprcntg'])) {
                                                           echo $_POST['sroofreplacementprcntg'];
                                                       } ?>" maxlength="255" name="sroofreplacementprcntg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="input-group-prepend col-12 p-0">
                                                <span class="input-group-text col-12">Overall condition:</span>
                                            </div>
                                            <div class="mt-2">
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofoverallcondition"
                                                                                      value="Satisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofoverallcondition'] == 'Satisfactory') ? 'checked' : '';
                                                    } ?>> Satisfactory</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofoverallcondition"
                                                                                      value="Unsatisfactory" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofoverallcondition'] == 'Unsatisfactory') ? 'checked' : '';
                                                    } ?>> Unsatisfactory</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5 class="font-weight-600">Any visible signs of damage / deterioration?</h5>
                                        <h6 class="font-weight-400 font-95">(check all that apply and explain
                                            below)</h6>
                                        <?php
                                        $generalconditionarr = ['Cracking', 'Cupping/curling', 'Excessive granule loss', 'Exposed asphalt', 'Exposed felt', 'Missing/loose/cracked tabs or tiles', 'Soft spots in decking', 'Visible hail damage'];
                                        $gcc = 0;
                                        while ($gcc != count($generalconditionarr)) {
                                            ?>
                                            <label class="p-1 px-2 mb-2"><input type="checkbox"
                                                                                name="sroofvisibledamagecheck"
                                                                                value="<?php echo $generalconditionarr[$gcc]; ?>" <?php if ($reqq->rowCount() > 0) {
                                                    echo ((strpos($r['sroofvisibledamage'], $generalconditionarr[$gcc]) !== false)) ? 'checked' : '';
                                                } ?>> <?php echo $generalconditionarr[$gcc]; ?></label>
                                            <br>
                                            <?php
                                            $gcc++;
                                        }
                                        ?>
                                        <input type="hidden" name="sroofvisibledamage"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['sroofvisibledamage'];
                                               } ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Any visible signs of leaks</b>:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofvisibleleaks"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofvisibleleaks'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofvisibleleaks"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofvisibleleaks'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Attic/underside of decking:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofdecking"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofdecking'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofdecking"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofdecking'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Interior ceilings:</span>
                                            </div>
                                            <div>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofceilings"
                                                                                      value="Yes" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofceilings'] == 'Yes') ? 'checked' : '';
                                                    } ?>> Yes</label>
                                                <label class="border p-2 px-3"><input type="checkbox"
                                                                                      name="sroofceilings"
                                                                                      value="No" <?php if ($reqq->rowCount() > 0) {
                                                        echo ($r['sroofceilings'] == 'No') ? 'checked' : '';
                                                    } ?>> No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 bg-secondary text-black">
                            <h4 class="font-weight-600 mb-0">Additional Comments/Observations <small>(use additional
                                    pages if needed)</small></h4>
                        </div>
                        <div class="col-12 border-bottom border-secondary mt-3">
                            <div class="form-group">
                                <textarea class="form-control" rows="5"
                                          name="additionalcomments"><?php if ($reqq->rowCount() > 0) {
                                        echo $r['additionalcomments'];
                                    } ?></textarea>
                            </div>
                        </div>
                        <div class="col-12 border-secondary mt-3">
                            <div class="form-group">
                                <h6>All Roof Inspection Forms must be completed and signed by a verifiable
                                    Florida-licensed inspector.</h6>
                                <h6>I certify that the above statement are true and correct.</h6>
                            </div>
                            <div class="form-group">
                                <div class="row big-inputs">
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="inspectorsignature"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['inspectorsignature'];
                                               } ?>">
                                        <label>Inspector Signature</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="title"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['title'];
                                               } ?>">
                                        <label>Title</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="licensenumber"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['licensenumber'];
                                               } ?>">
                                        <label>License Number</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="date"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['date'];
                                               } ?>">
                                        <label>Date</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="companyname"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['companyname'];
                                               } ?>">
                                        <label>Company Name</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="licensetype"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['licensetype'];
                                               } ?>">
                                        <label>License Type</label>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input class="form-control" name="workphone"
                                               value="<?php if ($reqq->rowCount() > 0) {
                                                   echo $r['workphone'];
                                               } ?>">
                                        <label>Work Phone</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="second-page" class="nav justify-content-between col-12 mt-5 pt-5 p-0">
                <div class="form-group col-md-12 mt-5">
                    <div class="col-12 px-3 py-2 border h6">
                        <b style="font-size:20px;" class="text-black">Special Instructions: </b> The sample Roof
                        Inspection Roof includes the minimum data needed for Underwiting to properly evaluate a property
                        application. While this specific form is not required, any other inspection report submitted for
                        consideration must include at least this level of detail to be acceptable.
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 text-black border-bottom border-secondary">
                            <h4 class="font-weight-600 mb-0">Photo Requirements</h4>
                        </div>
                        <div class="col-12 border-secondary mt-3">
                            <div class="form-group">
                                <h6>Photos must accompany each Roof Inspection Form. The minimum photo requirements
                                    include:</h6>
                                <ul class="h6">
                                    <li>Roof: Each slope</li>
                                    <li>All hazards or deficiencies</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 text-black border-bottom border-secondary">
                            <h4 class="font-weight-600 mb-0">Documenting the Condition of Each System </h4>
                        </div>
                        <div class="col-12 border-secondary mt-3">
                            <div class="form-group">
                                <h6>The Florida-licensed inspector is required to certify the condition of the roofing
                                    system. Acceptable Condition means that each system is working as intended and there
                                    are no visible hazards or deficiencies.</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 text-black border-bottom border-secondary">
                            <h4 class="font-weight-600 mb-0">Additional Comments or Observations</h4>
                        </div>
                        <div class="col-12 border-secondary mt-3">
                            <div class="form-group">
                                <h6>This section of the Roof Inspection Form must be completed with full
                                    details/descriptions if any of the following are noted on the inspection:</h6>
                                <ul class="h6">
                                    <li>Updates: Identify the types of updates, dates completed and by whom</li>
                                    <li>Any visible hazards or deficiencies</li>
                                    <li>Any roof determined not to be in good working order</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3 col-md-12">
                    <div class="col-12 p-0 border border-secondary">
                        <div class="p-3 text-black border-bottom border-secondary">
                            <h4 class="font-weight-600 mb-0">Note to All Agents </h4>
                        </div>
                        <div class="col-12 border-secondary mt-3">
                            <div class="form-group">
                                <h6>The writing agent must review in advance each Roof Inspection Form submitted with an
                                    application for coverage. It is the agent's responsibility to ensure that all rules
                                    and requirements are met before the application is bound. Agents may not submit
                                    applications for properties with roof(s) not in good working order or with existing
                                    hazards/deficiencies. </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="third-page" class="nav justify-content-between col-12 p-0">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center mt-4 nav justify-content-start images">
                    <div class="col-12 mt-4 mb-4">
                        <h3 class="mb-0 text-black">Photo Attachments</h3>
                    </div>
                    <?php
                    $filesq = $a->con->prepare("select * from rooffiles where reqid = ? order by id asc");
                    $filesq->execute([$_GET['reqid']]);
                    while ($files = $filesq->fetch()) {
                        ?>
                        <div class="col-4 mb-5">
                            <img class="col-12 border p-0 h-100" src="<?php echo $files['file']; ?>">
                            <h6 class="mt-3 text-center font-weight-500"><?php echo $files['name']; ?></h6>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

</html>

<script>
    $(".nav.roofinspection").addClass('active-link');

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js"></script>
<script type="text/javascript">
    $(".prepare-progress").css('display', 'block');
    filename = 'Roof-Inspection-<?php echo date("d-m-Y"); ?>.pdf'
    var element = document.getElementById('pdfcontent');
    html2pdf(element, {
        margin: [10, 0, 10, 0], //top, left, buttom, right
        filename: filename,
        image: {
            type: 'jpeg',
            quality: 1
        },
        html2canvas: {
            scale: 2,
            bottom: 20,
            letterRendering: true
        },
        jsPDF: {
            unit: 'mm',
            format: 'a3',
            orientation: 'portrait'
        },
        pagebreak: {
            mode: ['avoid-all', 'css', 'legacy']
        }
    });

</script>
