<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>Agency Accounts | <?php echo $site['name'] ?></title>
</head>

<body>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">
        All Agency Accounts
    </h4>
</div>
<?php
$query = $a->con->prepare("SELECT * FROM users WHERE type = 'Agency' AND userid <> ? AND deleted = 0");
$query->execute([$_SESSION['user']]);
?>
<div class="pl-4 pr-4 p-3 text-dark">
    <?php
    if (isset($_SESSION['useraccount'])) {
        if ($_SESSION['useraccount'] == 'added') {
            ?>
            <div class="alert alert-success font-weight-bold">
                Agency Added Successfully
            </div>
            <?php
        }
        if ($_SESSION['useraccount'] == 'updated') {
            ?>
            <div class="alert alert-success font-weight-bold">
                Agency Updated Successfully
            </div>
            <?php
        }
        unset($_SESSION['useraccount']);
    } ?>
    <div class="p-3 border bg-white rounded-10 shadow-sm">
        <table class="table col-12 p-0 mb-5 text-center" id="table">
            <thead class="bg-black text-light font-weight-normal">
            <tr>
                <th>Company Name</th>
                <th>Company Address</th>
                <th>Company Phone Number</th>
                <th>Agent Full Name</th>
                <th>Agent Direct Number</th>
                <th>Email Address</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($res = $query->fetch()) { ?>
                <tr>
                    <td> <?php echo $res['company'] ?> </td>
                    <td> <?php echo $res['address'] ?> </td>
                    <td> <?php echo $res['companyphone'] ?> </td>
                    <td> <?php echo $res['name'] ?> </td>
                    <td> <?php echo $res['phone'] ?> </td>
                    <td> <?php echo $res['email'] ?> </td>
                    <td> <?php if ($res['status'] == 'Review') { ?>
                            <span class="btn btn-sm btn-danger btn-<?php echo $res['status']; ?> active-status py-0"
                                  style="cursor:pointer" id="<?php echo $res[0] ?>"></span>
                            <?php
                        } else {
                            ?> <span
                                    class="btn btn-sm <?php echo ($res['status'] == 'Active') ? 'btn-success' : 'btn-secondary'; ?> btn-<?php echo $res['status']; ?> status py-0"
                                    style="cursor:pointer" id="<?php echo $res[0] ?>"></span>
                        <?php } ?>
                    </td>
                    <td>
                        <a class="btn btn-sm btn-primary py-0" style="cursor:pointer"
                           href="/user?x=<?php echo($res["userid"]); ?>"> Edit
                        </a>
                        <button class="btn btn-sm btn-danger py-0 my-1" style="cursor:pointer"
                                onclick="deleteUser('<?php echo($res["userid"]) ?>');">
                            Delete
                        </button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php include('includes/footer.php'); ?>
</body>

</html>

<script>
    $(".nav.allagency").addClass('active-link');
</script>
<script>
    function deleteUser(userId) {
        swal({
            title: 'Confirmation',
            text: "Are you sure you want to delete this agency?",
            icon: "warning",
            className: "text-center",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
        }, function (Proceed) {
            if (Proceed) {
                $.ajax({
                    url: '/adminfunctions',
                    type: 'post',
                    data: {
                        t: 'deleteUser',
                        userId: userId
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        });
    }

    $(".filter-status").change(function () {
        window.location = "?status=" + $(".filter-status").val();
    });
    $(".active-status").click(function () {
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'activeagencyaccount',
                d: 'users',
                i: $(this).attr('id')
            },
            success: function (data) {
                window.location = window.location.href;
            }
        });
    });
    $(".status").click(function () {
        if ($(this).hasClass('btn-Active')) {
            var txt = 'Inactive';
        } else {
            var txt = 'Active';
        }
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'changestatus',
                d: 'users',
                i: $(this).attr('id'),
                txt: txt
            },
            success: function (data) {
                window.location = window.location.href;
            }
        });
    });

</script>
