<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>Inspector Accounts | <?php echo $site['name'] ?></title>
</head>

<body>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">Create new inspector account
    </h4>
</div>
<div class="px-4 py-3">
    <?php
    if (isset($_SESSION['useraccount'])) {
        if ($_SESSION['useraccount'] == 'added') {
            ?>
            <div class="alert alert-success">
                Inspector Added Successfully
            </div>
            <?php
        }
        if ($_SESSION['useraccount'] == 'updated') {
            ?>
            <div class="alert alert-success">
                Inspector Updated Successfully
            </div>
            <?php
        }
        unset($_SESSION['useraccount']);
    } ?>
    <?php
    if (isset($_GET['x'])) {
        $q = $a->con->prepare("select * from users where userid = ?");
        $q->execute([$_GET['x']]);
        $r = $q->fetch();
        if (isset($_GET['link'])) {
            $linkq = $a->con->prepare("select * from customlinks where inspector = ? and id = ?");
            $linkq->execute([$_GET['x'], $_GET['link']]);
            if ($linkq->rowCount() > 0) {
                $link = $linkq->fetch();
            }
        }
        ?>
        <div class="modal fade" id="linkmodal" tabindex="-1" role="dialog" aria-labelledby="linkmodal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-black text-center p-4 text-main">
                        <div class="text-center col-12 p-0">
                            <i class="fas fa-link fa-2x"></i>
                        </div>
                    </div>
                    <div class="modal-body text-center py-4 text-dark">
                        <h5 class="text-center text-black">Please fill the form to add a new button</h5>
                        <div class="col-md-12 mx-auto mt-4">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Button Title</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" value="<?php if (isset($_GET['link'])) {
                                            echo $link['title'];
                                        } ?>" maxlength="100" name="title" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Button Link</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control" type="url" name="link"
                                               value="<?php if (isset($_GET['link'])) {
                                                   echo $link['link'];
                                               } ?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center mt-4">
                            <button type="button" class="btn text-secondary border btn-light" data-dismiss="modal"
                                    aria-label="Close">
                                Close ✕
                            </button>
                            &nbsp;&nbsp;
                            <button type="submit" name="submitbutton" class="btn bg-main text-white">
                                <?php echo (isset($_GET['link'])) ? 'Update' : 'Submit'; ?> <i
                                        class="fas fa-save fa-sm pl-1"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#linkmodal button[name='submitbutton']").click(function () {
                var title = $("#linkmodal input[name='title']").val();
                var link = $("#linkmodal input[name='link']").val();
                if (title != '' && link != '') {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'addnewbutton',
                            title: title,
                            link: link,
                            inspector: '<?php echo $_GET['x'] ?>',
                            linkid: '<?php echo (isset($_GET['link'])) ? $_GET['link'] : ''; ?>'
                        },
                        success: function (data) {
                            $("#linkmodal").modal('hide');
                            $("#linkmodal input[name='title']").val('');
                            $("#linkmodal input[name='link']").val('');
                            $.ajax({
                                url: 'adminfunctions',
                                type: 'post',
                                data: {
                                    t: 'getinspectorlinks',
                                    inspector: '<?php echo $_GET['x'] ?>'
                                },
                                success: function (data) {
                                    $(".custom-links-table").html(data);
                                }
                            });
                        }
                    });
                } else {
                    $("#linkmodal h5").html('Please fill the form to add a new button<br><span class="text-danger">Both Fields are Required!</span>');
                }
            });

        </script>
    <?php
    if (isset($_GET['link'])){
    if ($linkq->rowCount() > 0){
    ?>
        <script>
            $("#linkmodal").modal('show');

        </script>
        <?php
    }
    }
    }
    ?>
    <div class="col-12 p-0 p-3 bg-white py-4  border rounded-10 shadow-sm">
        <form method="post" enctype="multipart/form-data" class="nav bg-white mt-3 justify-content-between">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Inspector Name</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                            echo $r['name'];
                        } else if (isset($_POST['name'])) {
                            echo $_POST['name'];
                        } ?>" maxlength="255" name="name" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Phone Number</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                            echo $r['phone'];
                        } else if (isset($_POST['phone'])) {
                            echo $_POST['phone'];
                        } ?>" maxlength="255" name="phone">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Login Email</label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="email" value="<?php if (isset($_GET['x'])) {
                            echo $r['email'];
                        } else if (isset($_POST['email'])) {
                            echo $_POST['email'];
                        } ?>" maxlength="255" name="email" required <?php if (isset($_GET['x'])) {
                            echo 'readonly';
                        } ?>>
                    </div>
                </div>
                <?php if (!(isset($_GET['x']))) { ?>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Login Password </label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                                echo $r['password'];
                            } else if (isset($_POST['password'])) {
                                echo $_POST['password'];
                            } ?>" name="password" maxlength="255" required>
                            <small class="text-secondary">atleast 5 characters</small>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Company Name</label>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                            echo $r['company'];
                        } else if (isset($_POST['company'])) {
                            echo $_POST['company'];
                        } ?>" maxlength="100" name="company" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>License Number</label>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                            echo $r['licensenumber'];
                        } else if (isset($_POST['licensenumber'])) {
                            echo $_POST['licensenumber'];
                        } ?>" maxlength="100" name="licensenumber" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Area of Coverage</label>
                    </div>
                    <div class="col-md-8">
                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                            echo $r['coveragearea'];
                        } else if (isset($_POST['coveragearea'])) {
                            echo $_POST['coveragearea'];
                        } ?>" maxlength="100" name="coveragearea" required>
                    </div>
                </div>
            </div>
            <?php if (!(isset($_GET['x']))) { ?>
                <div class="col-12 p-3">
                    <hr>
                    <h5 class="mb-0">Custom Links</h5>
                    <span class="font-85 font-weight-500 text-secondary">The Custom Links are optional and will only be stored if both Title and Link is inputted.</span>
                    <br>
                    <br>
                    <?php
                    $clc = 1;
                    while ($clc <= 5) {
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Button <?php echo $clc; ?> Title</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control" type="text" value="<?php if (isset($_GET['x'])) {
                                            echo $r['title' . $clc];
                                        } else if (isset($_POST['title' . $clc])) {
                                            echo $_POST['title' . $clc];
                                        } ?>" maxlength="100" name="title<?php echo $clc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>Button <?php echo $clc; ?> Link</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control" type="url" value="<?php if (isset($_GET['x'])) {
                                            echo $r['link' . $clc];
                                        } else if (isset($_POST['link' . $clc])) {
                                            echo $_POST['link' . $clc];
                                        } ?>" name="link<?php echo $clc; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $clc++;
                    }
                    ?>
                </div>
            <?php } ?>
            <?php if (isset($_GET['x'])) { ?>
                <div class="col-12 p-3">
                    <hr>
                    <div class="nav justify-content-between mb-2">
                        <div>
                            <h5 class="font-weight-bold text-black">Custom Links</h5>
                        </div>
                        <div>
                            <button type="button" class="btn btn-sm border btn-light add-link font-weight-600">Add New
                                Link <i class="fas fa-plus fa-sm pl-1"></i></button>
                        </div>
                    </div>
                    <div class="custom-links-table">
                        <table class="table table-sm">
                            <tr>
                                <th>#</th>
                                <th>Button Title</th>
                                <th>Link</th>
                                <th></th>
                            </tr>
                            <?php
                            $customlinksq = $a->con->prepare("select * from customlinks where inspector = ?");
                            $customlinksq->execute([$_GET['x']]);
                            $clc = 1;
                            while ($customlinks = $customlinksq->fetch()) {
                                ?>
                                <tr>
                                    <td><?php echo $clc; ?></td>
                                    <td><?php echo $customlinks['title']; ?></td>
                                    <td><a target="_blank"
                                           href="<?php echo $customlinks['link']; ?>"><?php echo $customlinks['link']; ?></a>
                                    </td>
                                    <td>
                                        <a class="font-weight-500 pointer text-primary"
                                           href="?x=<?php echo $_GET['x'] ?>&link=<?php echo $customlinks[0]; ?>">edit</a>
                                        &nbsp; | &nbsp;
                                        <span class="font-weight-500 pointer delete-link text-danger"
                                              id="<?php echo $customlinks[0]; ?>">delete</span>
                                    </td>
                                </tr>
                                <?php
                                $clc++;
                            }
                            ?>
                        </table>
                    </div>
                </div>
            <?php } ?>
            <div class="col-12"></div>
            <?php if (isset($_GET['x'])) {
                ?>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="form-group mt-3">
                        <button type="submit" name="update" class="btn btn-dark px-3 btn-update border shadow bg-main">
                            Update User
                        </button>
                    </div>
                </div>
                <?php
            } else { ?>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="form-group row mt-3">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-9">
                            <button type="submit" name="submit"
                                    class="btn btn-dark px-4 btn-submit border shadow bg-main">Submit
                            </button>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
<?php
if (isset($_GET['x'])) { ?>
    <div class="pl-4 pr-4 p-3 text-dark nav justify-content-between">
        <h4 class="mb-0 font-weight-bold text-black">Change Password
        </h4>
    </div>
    <div class="pl-4 pr-4 pb-3 text-dark">
        <div class="col-12 p-0 p-3 bg-white py-4 border">
            <form method="post">
                <div class="nav justify-content-between">
                    <div class="form-group mb-3 col-md-6 col-sm-12">
                        <label>New Password </label>
                        <div class="input-group">
                            <input class="form-control" name="pass" required type="password">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white change-pass-type" id="pass"
                                      style="cursor:pointer;"><i class="far fa-eye pass-eye-pass"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3 col-md-6 col-sm-12">
                        <label>Confirm Password </label>
                        <div class="input-group">
                            <input class="form-control" name="cpass" required type="password">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-white change-pass-type" id="cpass"
                                      style="cursor:pointer;"><i class="far fa-eye pass-eye-cpass"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3">
                    <div>
                        <button type="submit" name="changepass" class="btn btn-dark btn-update border bg-main">Update
                            Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
<br><br>
<?php if ($user['type'] == 'Admin') { ?>
    <div class="modal fade" id="filemodal" tabindex="-1" role="dialog" aria-labelledby="filemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-main text-center p-4 text-light">
                    <div class="text-center col-12 p-0">
                        <i class="fas fa-upload fa-3x"></i>
                    </div>
                </div>
                <div class="modal-body text-center py-4 text-dark">
                    <h3 class="text-center">Please Select File</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="col-md-9 mx-auto">
                            <input type="file" name="file" class="p-2 col-12 border" required>
                            <input class="form-control mt-3" placeholder="File Title" name="name" required>
                        </div>
                        <div class="text-center mt-4">
                            <button type="button" class="btn text-secondary border btn-light" data-dismiss="modal"
                                    aria-label="Close">
                                Close ✕
                            </button>
                            &nbsp;&nbsp;
                            <button type="submit" name="uploadfile" class="btn bg-main text-white">
                                Submit <i class="fas fa-upload fa-sm pl-1"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($_POST['uploadfile'])) {
        $fileid = substr(sha1(strtotime("now") . rand(1111, 9999)), 0, 6);
        $img = $_FILES['file']['tmp_name'];
        $pic = $_FILES['file']['name'];
        $ext = pathinfo($pic, PATHINFO_EXTENSION);
        $roughmix = 'assets/images/content/' . $_GET['reqid'] . sha1($pic . strtotime(("now") . rand(1111, 9999))) . '.' . $ext;
        $addfileq = $a->con->prepare("insert into content(fileid,inspector,file,name,insertby) values(?,?,?,?,?)");
        $st2 = $addfileq->execute([$fileid, $_GET['x'], $roughmix, $_POST['name'], $_SESSION['user']]);
        if ($st2) {
            move_uploaded_file($img, $roughmix);
        }
        $_SESSION['file'] = 'added';
        echo '<script> window.location = window.location.href; </script>';
        exit();
    }
    ?>
<?php } ?>
<?php
if (isset($_GET['x'])) {
    ?>
    <div class="pl-4 pr-4 pb-3 mb-5 text-dark">
        <div class="nav justify-content-between">
            <div class="nav justify-content-start mb-2">
                <div>
                    <h4 class="font-weight-bold mb-0 text-black">Content</h4>
                </div>
                <?php if ($user['type'] == 'Admin') { ?>
                    <button class="ml-3 btn btn-primary btn-sm border font-weight-500 add-file" data-toggle="modal"
                            data-target="#filemodal">Add New File <i class="fas fa-plus fa-sm pl-1"></i></button>
                <?php } ?>
            </div>
        </div>
        <div class="row px-2">
            <?php
            $query = $a->con->prepare("select * from content where inspector = ? order by id desc");
            $query->execute([$_GET['x']]);
            $cc = 1;
            while ($res = $query->fetch()) {
                ?>
                <div class="col-lg-2 col-md-3 col-sm-12 p-2 file-hover">
                    <div class="col-12 bg-white p-0 text-center h-100 border rounded-10 shadow-sm">
                        <a class="col-12 p-0 text-dark" href="<?php echo $res['file']; ?>" style="text-decoration:none;"
                           download="<?php echo $res['name']; ?>">
                            <div class="col-12 bg-white px-3 pt-3 pb-2 rounded-10 text-center">
                                <img class="col-12 p-0 mb-2" src="assets/images/icons/file.png">
                                <div style="font-weight:600;"><?php echo $res['name']; ?></div>
                            </div>
                        </a>
                        <?php if ($user['type'] == 'Admin') { ?>
                            <p class="mb-0 text-danger delete-file pb-1 font-90 font-weight-500 pointer"
                               id="<?php echo $res['id']; ?>">Delete this file</p>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>
<br><br>
</body>

</html>
<?php
if (isset($_GET['x'])) {
    ?>
    <script>
        function deletelink(i) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to revert this action!",
                icon: "warning",
                className: "text-center",
                buttons: true, showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'deletelink',
                            inspector: '<?php echo $_GET['x'] ?>',
                            i: i
                        },
                        success: function (data) {
                            $.ajax({
                                url: 'adminfunctions',
                                type: 'post',
                                data: {
                                    t: 'getinspectorlinks',
                                    inspector: '<?php echo $_GET['x'] ?>'
                                },
                                success: function (data) {
                                    $(".custom-links-table").html(data);
                                }
                            });
                        }
                    });
                } else {

                }
            });
        }

        $(".delete-link").click(function () {
            deletelink($(this).attr('id'));
        });

        $(".delete-file").click(function () {
            swal({
                title: "Are you sure?",
                text: "You will not be able to revert this action!",
                icon: "warning",
                className: "text-center",
                buttons: true, showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'deletefile',
                            reqid: '<?php echo $_GET['reqid'] ?>',
                            i: $(this).attr('id')
                        },
                        success: function (data) {
                            window.location = window.location.href;
                        }
                    });
                } else {

                }
            });


        });

    </script>
    <?php
}
?>
<?php
if (!(isset($_GET['x']))) {
    $userid = substr(sha1(strtotime("now")), 0, 7);
    $_SESSION['inspectoraccount'] = $userid;
}
?>
<?php
if (isset($_POST['submit'])) {
    if (strlen($_POST['password']) >= 5) {
        $querycheck = $a->con->prepare("select id from users where email = ?");
        $querycheck->execute([$_POST['email']]);
        if ($querycheck->rowCount() > 0) {
            echo "<script>
    swal('Email Taken Already', 'Must be taken by a staff or a inspector', 'error');
</script>";
        } else {
            $userid = substr(sha1($_POST['email'] . strtotime("now") . rand(1111, 9999)), 0, 6);
            $query = $a->con->prepare("insert into users(userid,name,email,password,phone,licensenumber,company,coveragearea,type,status) values (?,?,?,?,?,?,?,?,?,?)");
            $status = $query->execute([$userid, $_POST['name'], $_POST['email'], password_hash($_POST['password'], PASSWORD_BCRYPT), $_POST['phone'], $_POST['licensenumber'], $_POST['company'], $_POST['coveragearea'], 'Inspector', 'Active']);
            if ($status) {


                $clc = 1;
                while ($clc <= 5) {
                    if ($_POST['title' . $clc] != '' && $_POST['title' . $clc] != '') {
                        $addlinkq = $a->con->prepare("insert into customlinks(inspector,title,link,insertby) values(?,?,?,?)");
                        $addlinkq->execute([$userid, $_POST['title' . $clc], $_POST['link' . $clc], $_SESSION['user']]);
                    }
                    $clc++;
                }

                $_SESSION['useraccount'] = 'added';
                echo '<script> window.location = "all-inspector.php"; </script>';
            }

        }
    } else {
        ?>
        <script>
            swal("Password too short", "Password must have atleast 5 characters", "error");

        </script>
        <?php
    }

}
if (isset($_POST['update'])) {
    $querycheck = $a->con->prepare("select id from users where email = ? and userid <> ?");
    $querycheck->execute([$_POST['email'], $_GET['x']]);
    if ($querycheck->rowCount() > 0) {
        echo "<script> swal('Email Taken Already', 'Must be taken by a staff or a inspector', 'error'); </script>";
    } else {
        $query = $a->con->prepare("update users set name = ?,phone=?,licensenumber=?,company=?,coveragearea=? where userid = ?");
        $status = $query->execute([$_POST['name'], $_POST['phone'], $_POST['licensenumber'], $_POST['company'], $_POST['coveragearea'], $_GET['x']]);
        if ($status) {
            $_SESSION['useraccount'] = 'updated';
            echo '<script> window.location = "all-inspector.php"; </script>';
        }
    }
}
?>

<script>
    $(".nav.addinspector").addClass('active-link');

    $(".add-link").click(function () {
        $("#linkmodal").modal('show');
    });

</script>


<script>
    $(".cancel-password").click(function () {
        $("input[name='oldpass'], input[name='pass'], input[name='cpass']").val('');
    });

</script>
<?php
if (isset($_POST['changepass'])) {
    if ($_POST['pass'] == $_POST['cpass']) {
        if (strlen($_POST['pass']) > 6) {
            $uq = $a->con->prepare("update users set password = ? where userid = ?");
            $status = $uq->execute([password_hash($_POST['pass'], PASSWORD_BCRYPT), $_GET['x']]);
            if ($status) {
                $_SESSION['password'] = 'updated';
                echo '<script> window.location = window.location.href; </script>';
                exit();
            }
        } else {
            ?>
            <script>
                swal("Password should not be less than 7 letters", "Please Try Again", "error");

            </script>
            <?php
        }

    } else {
        echo '
        <script>
        swal("Passwords Do Not Match", "Please check your input", "error");
    </script>
        ';
    }
}
?>
