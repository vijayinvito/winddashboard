<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('includes/essentials.php'); ?>
    <title>Reset Password | <?php echo $site['name']; ?> </title>
</head>

<body>
<?php if (isset($_SESSION['user'])) {
    echo "<script> window.location = '/index.php;' </script>";
} else { ?>
    <style>
        html,
        body {
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
            background-repeat: no-repeat;
        }

        .login-form label {
            margin-bottom: 0px;
            font-weight: 600;
            font-size: 90%;
            letter-spacing: 0.8px;
            color: #4B5258;
        }

        .login-form input,
        .login-form input:hover {
            color: black;
            background: transparent !important;
            border-bottom: 1px solid #6c757d !important;
            border: 0px;
            border-radius: 0px !important;
        }

        .input-group-text {
            color: black !important;
            background: transparent;
            width: 42px;
            border: 0px;
            text-align: center;
        }

        .form-bottom-border {
            border-bottom: 1px solid #4B5258 !important;
        }

        .form-bottom-border input::placeholder {
            color: #4B5258 !important;
        }

        .form-bottom-border input,
        .form-bottom-border input:-webkit-autofill {
            color: white !important;
            background: transparent !important;
            border: 0px !important;
            border-radius: 0px !important;
        }

    </style>
    <div class="col-12 h-100">
        <div class="container nav justify-content-between my-5">
            <div class="col-lg-6 col-md-6 col-sm-12 h-100">
                <div class="text-center py-5 bg-black shadow nav align-items-top h-100">
                    <div class="col-12 p-0">
                        <a href="/index.php"><img class="col-10 ml-auto mr-auto p-0" src="assets/images/logo.png"></a>
                        <h3 class="text-light mt-5 mb-4">Quick To Schedule<br>Accurate Same Day Reports</h3>
                        <br><br>
                        <br>
                        <h3 class="text-light">Insurance Agents/Agencies</h3>
                        <h3 class="text-light">Roofing Contractors</h3>
                        <h3 class="text-light">Realtors</h3>
                        <br>
                        <br>
                        <a href="/index.php" class="btn bg-main text-white font-weight-500 px-5 mt-2">Login Here</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 p-0 h-100">
                <?php 
if(isset($_GET['u']) && isset($_GET['t'])){
$e = $_GET['u'];
$t = $_GET['t'];
$res = $a->con->prepare("select * from users where email = ?");
$res->execute([$e]);
$rec = $res->fetch();
    if($rec['tp'] == $t){
        ?>
                <form method="post" class="col-12 px-5 pt-5 pb-5 border-dark text-dark rounded bg-white shadow login-form">
                    <h2 class="mb-0 text-black font-weight-bold" style="letter-spacing:0.8px;">Reset Password</h2>
                    <h6 class="mb-0 text-secondary" style="font-weight:600;">Enter your new password below</h6>
                    <hr>
                    <br>
                    <div>
                        <div class="form-group col-md-12 p-0">
                            <label>New Password</label>
                            <input maxlength="255" name="password" type="password" class="form-control" required>
                        </div>
                        <div class="form-group col-md-12 p-0">
                            <label>Confirm New Password</label>
                            <input maxlength="255" name="cpassword" type="password" class="form-control" required>
                        </div>
                        <br>
                        <button type="submit" name="submit" class="col-12 btn bg-main border-dark text-light">Reset Password</button>
                    </div>
                </form>
                <?php
        }
else{
    echo '<script>
     setTimeout(function () {
        	swal({
  title: "Token Expired !",
  text:"Redirecting . . .",
		icon:"error",
        time:1000,
		buttons:false,
	}, function(Proceed) {
      window.location = "login.php";
		
})
}, 1000);
        
        </script>';
}
}
    else{
        
    
    }

?>
            </div>
        </div>
    </div>
    <?php 
if(isset($_POST['submit'])){
    if($_POST['password'] == $_POST['cpassword']){
        $result = $a->con->prepare("update users set password = ? where email = ? and tp = ?");
        $result->execute([password_hash($_POST['password'], PASSWORD_BCRYPT), $_GET['u'], $_GET['t']]);
$rel = $a->con->prepare("select name from users where email = ? and tp = ?");
$rel->execute([$_GET['u'], $_GET['t']]);
$res = $rel->fetch();
        $r = $a->con->prepare("update users set tp = '' where email = ?");
        $st = $r->execute([$_GET['u']]);
        if($st){
$e = $_GET['u'];
$name = $res['name'];
$message = '
Hi '.$name.'<br>
This message is to confirm that your password has been successfully changed.  <br><br>
Thank You<br>
<b>'.$site['name'].'</b>
';
$sub = "Password Changed Successfully";
if(mail($e,$sub,$message,$headers)){
        echo '
        <script> 
 setTimeout(function() {
 swal({
 title: "Password Changed Successfully !",
 text: "Redirecting....",
 icon: "success",
 buttons: false,
 }, function(Proceed) {
     window.location = "/index.php;";
    
     })
     }, 1000);
        </script>';
}
        }
    }
    else{
        echo '
        <script> 
 swal({
 title: "Passwords do not match !",
 text: "Please enter password again",
 icon: "error",
 buttons: false,
 });
        </script>';
    }
}
?>
    <?php } ?>
</body>

</html>
