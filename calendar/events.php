<?php
session_start();
header('Content-Type: application/json');

error_reporting(0);
include("config.php");

$con = new PDO($SETTINGS['host'], $SETTINGS['mysql_user'], $SETTINGS['mysql_pass']);
$userq = $con->prepare("SELECT * FROM users WHERE userid = ?");
$userq->execute([$_SESSION['user']]);
$user = $userq->fetch();

$sql_additional = $user['type'] == 'Inspector' ? ' requests.assigned = \'' . $_SESSION['user'] . '\'' : ' 0 = 0 ';

$sql = "SELECT requests.reqid, requests.name, requests.address, requests.city, requests.state, requests.zipcode, requests.appointment_start, requests.appointment_end, users.name as inspector_name FROM requests INNER JOIN users on users.userid = requests.assigned WHERE $sql_additional AND UNIX_TIMESTAMP(`appointment_start`) >=" . strtotime($mysqli->real_escape_string($_GET['start'])) . " AND UNIX_TIMESTAMP(`appointment_start`)<=" . strtotime($mysqli->real_escape_string($_GET['end']));

$arr = array();

if ($result = $mysqli->query($sql)) {
    $counter = 0;
    while ($row = $result->fetch_assoc()) {
        $arr[$counter] = $row;
        $counter++;
    }

    echo json_encode($arr);
} else {
    printf("Error: %s\n", $mysqli->sqlstate);
    exit;
}