<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php;";</script>';
    } ?>
    <title>Inspector Messages | <?php echo $site['name'] ?></title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
</head>

<body>
<?php
$status = (isset($_GET['status'])) ? '%' . $_GET['status'] . '%' : '%%';
$company = (isset($_GET['company'])) ? '%' . $_GET['company'] . '%' : '%%';
?>
<div class="px-4 nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">All Inspector Messages |
        <span class="text-secondary" style="font-size:70%;"><?php
            $totalq = $a->con->prepare("select count(*) from users where userid <> ? and type = 'Inspector' and status LIKE ?");
            $totalq->execute([$_SESSION['user'], $status]);
            $total = $totalq->fetch();
            echo 'Total: ' . number_format($total[0]);
            ?></span>
    </h4>
</div>
<div class="px-2 pt-3 text-dark nav justify-content-start">
    <div class="col-lg-4 col-md-5 col-sm-12">
        <div class="input-group">
                <span class="input-group-text">
                    Filter Status:
                </span>
            <select class="border form-control rounded-0 p-1 filter-status">
                <option selected value="">All Accounts</option>
                <option <?php if (isset($_GET['status']) && $_GET['status'] == 'Active') {
                    echo 'selected';
                } ?> value="Active">Active Only
                </option>
                <option <?php if (isset($_GET['status']) && $_GET['status'] == 'Inactive') {
                    echo 'selected';
                } ?> value="Inactive">Inactive Only
                </option>
            </select>
        </div>

    </div>
</div>
<?php $query = $a->con->prepare("select * from users where type = 'Inspector' and userid <> ? and status LIKE ?");
$query->execute([$_SESSION['user'], $status]);
?>
<div class="pl-4 pr-4 p-3 text-dark">
    <?php
    if (isset($_SESSION['useraccount'])) {
        if ($_SESSION['useraccount'] == 'added') {
            ?>
            <div class="alert alert-success font-weight-bold">
                Inspector Added Successfully
            </div>
            <?php
        }
        if ($_SESSION['useraccount'] == 'updated') {
            ?>
            <div class="alert alert-success font-weight-bold">
                Inspector Updated Successfully
            </div>
            <?php
        }
        unset($_SESSION['useraccount']);
    } ?>
    <div class="p-3 border bg-white rounded-10 shadow-sm">
        <table class="table table-bordered col-12 p-0 mb-5 table-striped border bg-white text-left" id="content">
            <thead class="bg-black text-light font-weight-normal">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th class=" text-center">Unread</th>
                <th class="text-center">Messages</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($res = $query->fetch()) { ?>
                <tr>
                    <td><a style="font-weight:500;" class="text-dark"
                           href="inspector-profile.php?cust=<?php echo $res['userid']; ?>"><?php echo $res['name'] ?></a>
                    </td>
                    <td> <?php echo $res['email'] ?> </td>
                    <td class=" text-center font-weight-bold"> <?php
                        $unreadq = $a->con->prepare("select red from messages where user = ? and red = 0");
                        $unreadq->execute([$res['userid']]);
                        if ($unreadq->rowCount() > 0) {
                            echo $unreadq->rowCount();
                        } else {
                            echo '0';
                        }
                        ?> </td>
                    <td class="p-2 text-center">
                        <a class="btn btn-sm btn-info" style="cursor:pointer"
                           href="inspector-message?cust=<?php echo $res['userid'] ?>"> Open Chat
                            <?php
                            $unreadq = $a->con->prepare("select red from messages where user = ? and red = 0");
                            $unreadq->execute([$res['userid']]);
                            if ($unreadq->rowCount() > 0) {
                                echo '<span class="badge badge-dark">' . $unreadq->rowCount() . '</span>';
                            } else {
                                echo '';
                            }
                            ?> </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</body>

</html>

<script>
    $(".filter-status").change(function () {
        window.location = "?company=" + $(".filter-company").val() + "&status=" + $(".filter-status").val();
    });

    $(".filter-company").change(function () {
        window.location = "?company=" + $(".filter-company").val() + "&status=" + $(".filter-status").val();
    });

</script>
<script>
    $(document).ready(function () {
        $('#content').DataTable({
            "order": [
                [2, "desc"]
            ]
        });
    });

</script>
<script>
    $(".nav.inspectormessages").addClass('active-link');

</script>
<style>
    table.dataTable {
        clear: both;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        max-width: none !important;
        border-collapse: separate !important;
    }

    table.dataTable thead > tr > th.sorting_asc,
    table.dataTable thead > tr > th.sorting_desc,
    table.dataTable thead > tr > th.sorting,
    table.dataTable thead > tr > td.sorting_asc,
    table.dataTable thead > tr > td.sorting_desc,
    table.dataTable thead > tr > td.sorting {
        padding-right: .75rem;
    }

    table.dataTable thead .sorting::after,
    table.dataTable thead .sorting_asc::after {
        display: none;
    }

    table.dataTable thead .sorting_desc::after {
        display: none;
    }

    table.dataTable thead .sorting {
        background-image: url(https://datatables.net/media/images/sort_both.png);
        background-repeat: no-repeat;
        background-position: center right;
    }

    table.dataTable thead .sorting_asc {
        background-image: url(https://datatables.net/media/images/sort_asc.png);
        background-repeat: no-repeat;
        background-position: center right;
    }

    table.dataTable thead .sorting_desc {
        background-image: url(https://datatables.net/media/images/sort_desc.png);
        background-repeat: no-repeat;
        background-position: center right;
    }

</style>
<script>
    $(".filter-status").change(function () {
        window.location = "?status=" + $(".filter-status").val();
    });
    $(".status").click(function () {
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'changestatus',
                d: 'users',
                i: $(this).attr('id'),
                txt: $(this).text().trim()
            },
            success: function (data) {
                window.location = window.location.href;
            }
        });


    });

</script>
<script src="https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js"></script>
<script src="https://unpkg.com/jspdf-autotable@2.3.2"></script>
<script>
    function generate() {
        $(".hide-in-report").css('display', 'none');
        var doc = new jsPDF('l', 'pt', 'a4');

        var res = doc.autoTableHtmlToJson(document.getElementById("content"));

        var header = function (data) {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            doc.text("All Inspectors", data.settings.margin.left, 50);
        };

        var options = {
            beforePageContent: header,
            margin: {
                top: 80
            }
        };

        doc.autoTable(res.columns, res.data, options);

        doc.save("All-Inspectors-<?php echo date("d-m-Y"); ?>.pdf");
        window.location = window.location.href;
    }

</script>
