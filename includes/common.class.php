<?php

class common
{
    public $connection;

    function __construct()
    {
        include('includes/config.php');
        $this->connection = new PDO($SETTINGS['host'], $SETTINGS['mysql_user'], $SETTINGS['mysql_pass']);
    }

    public function sendEmailToGroup($userType, $subject, $message, $headers)
    {
        $users = $this->connection->query("SELECT id, email FROM users WHERE type = '$userType' AND status = 'Active'");

        foreach ($users as $user) {
            mail($user["email"], $subject, $message, $headers);
        }
    }

    public function sendEmailToUser($userId, $subject, $message, $headers)
    {
        $users = $this->connection->query("SELECT id, email FROM users WHERE userid = '$userId'");

        if ($users) {
            $result = $users->fetch(PDO::FETCH_ASSOC);
            mail($result["email"], $subject, $message, $headers);
        }
    }

    public function sendEmail($toEmailAddress, $subject, $message, $headers)
    {
        mail($toEmailAddress, $subject, $message, $headers);
    }

}