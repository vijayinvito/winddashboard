<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">

<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

<?php

if (isset($_SESSION['user'])) {

    $userq = $a->con->prepare("SELECT * FROM users WHERE userid = ?");

    $userq->execute([$_SESSION['user']]);

    if ($userq->rowCount() > 0) {

        $user = $userq->fetch();

        if ($user['status'] == 'Inactive') {

            echo "<script> window.location = '/signout.php' </script>";

        }

    } else {

        echo "<script> window.location = '/signout.php' </script>";

    }

    $sitelogo = $site['logo'];

    if ($user['type'] == 'Agency') {

        $sitelogo = ($user['logo'] != '') ? $user['logo'] : $site['logo'];

    }

} else {

    echo "<script> window.location = '/index.php' </script>";

}





?>

<div class="d-flex" id="wrapper">

    <!--Navbar-->

    <div class="border-right" id="sidebar-wrapper">

        <div class="sidebar-heading text-secondary py-4 pb-5 px-2 col-12 text-center">

            <img src="<?php echo "/$sitelogo"; ?>"

                 style="width:14rem;"

                 class="p-0 mt-2">

        </div>

        <div class="list-group list-group-flush mb-5">

            <a href="../index.php" class="col-12 nav-main index nav justify-content-between p-2">

                <div class="nav-hide"><i class="fas fa-home"></i> Dashboard</div>

                <div class="nav-icon"><i class="fas fa-home"></i></div>

            </a>

            <?php

            if ($user['type'] == 'Inspector') {

                ?>

                <a href="../assigned-requests.php"

                   class="col-12 nav-main assignedrequests nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> Requests</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="../calendar-requests" class="col-12 nav-main calendar nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-calendar"></i> Job Calendar</div>

                    <div class="nav-icon"><i class="fas fa-calendar"></i></div>

                </a>

                <a href="../assigned-requests.php?status=Completed"

                   class="col-12 nav-main assignedrequestscompleted nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> Completed Requests</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="../content.php" class="col-12 nav-main content nav justify-content-between p-2">

                    <div class="nav-hide"><i class="far fa-file-alt"></i> Content</div>

                    <div class="nav-icon"><i class="far fa-file-alt"></i></div>

                </a>

                <a href="../my-messages.php" class="col-12 nav-main messages nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-comments"></i> Messages

                        <?php

                             $unreadq = $a->con->prepare("select red from messages where replyto = ? and red = 0");

                        $unreadq->execute([$_SESSION['user']]);


                        if ($unreadq->rowCount() > 0) {

                            ?>

                            &nbsp;<span class="badge badge-dark"><?php echo $unreadq->rowCount(); ?></span>

                            <?php

                        }

                        ?>

                    </div>

                    <div class="nav-icon"><i class="fas fa-comments"></i></div>

                </a>

            <?php } ?>

            <?php

            if ($user['type'] == 'Agency') {

                ?>

                <a href="my-requests" class="col-12 nav-main myrequests nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> My Requests</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="my-requests?status=Completed"

                   class="col-12 nav-main myrequestscompleted nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> Requests History</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="final-reports" class="col-12 nav-main finalreports nav justify-content-between p-2">

                    <div class="nav-hide"><i class="far fa-file-alt"></i> Final Reports</div>

                    <div class="nav-icon"><i class="far fa-file-alt"></i></div>

                </a>

            <?php } ?>

            <?php

            if ($user['type'] == 'Admin') {

                ?>

                <a href="../all-requests.php" class="col-12 nav-main allrequests nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> All Requests</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="../calendar-requests" class="col-12 nav-main calendar nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-calendar"></i> Job Calendar</div>

                    <div class="nav-icon"><i class="fas fa-calendar"></i></div>

                </a>

                <a href="../add-request" class="col-12 nav-main myrequests nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-align-left"></i> Add Request</div>

                    <div class="nav-icon"><i class="fas fa-align-left"></i></div>

                </a>

                <a href="../all-agency.php" class="col-12 nav-main allagency nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-users"></i> View All Agencies <span class="review-badge"><?php

                            $unreadq = $a->con->prepare("select id from users where status = 'Review' and type = 'Agency'");

                            $unreadq->execute();

                            if ($unreadq->rowCount() > 0) {

                                ?>

                                &nbsp;<span

                                        class="badge badge-dark text-white"><?php echo $unreadq->rowCount(); ?></span>

                                <?php

                            }

                            ?></span></div>

                    <div class="nav-icon"><i class="fas fa-users"></i></div>

                </a>

                <br>

                <a href="../user?type=inspector" class="col-12 nav-main addinspector nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-users"></i> Add New Inspector</div>

                    <div class="nav-icon"><i class="fas fa-users"></i></div>

                </a>

                <a href="../all-inspector.php" class="col-12 nav-main allinspector nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-users"></i> View All Inspectors</div>

                    <div class="nav-icon"><i class="fas fa-users"></i></div>

                </a>

                <a href="../messages-inspector.php"

                   class="col-12 nav-main messages-inspector nav justify-content-between p-2">

                    <div class="nav-hide"><i class="far fa-comments"></i> Inspector Messages <span

                                class="messages-badge"><?php

                            $unreadq = $a->con->prepare("SELECT red FROM messages INNER JOIN users ON messages.convid = users.userid WHERE type = 'Inspector' AND replyto = '' AND red = 0");

                            $unreadq->execute();

                            if ($unreadq->rowCount() > 0) {

                                ?>

                                &nbsp;<span class="badge badge-dark"><?php echo $unreadq->rowCount(); ?></span>

                                <?php

                            }

                            ?></span>

                    </div>

                    <div class="nav-icon"><i class="far fa-comments"></i></div>

                </a>

                <a href="../messages-agency.php"

                   class="col-12 nav-main messages-agency nav justify-content-between p-2">

                    <div class="nav-hide"><i class="far fa-comments"></i> Agency Messages

                        <span class="messages-badge">

                            <?php

                            $unreadq = $a->con->prepare("SELECT red FROM messages INNER JOIN users ON messages.convid = users.userid WHERE type = 'Agency' AND red = 0");

                            $unreadq->execute();



                            if ($unreadq->rowCount() > 0) { ?>

                                &nbsp;<span

                                        class="badge badge-dark text-white"><?php echo $unreadq->rowCount(); ?></span>

                                <?php

                            }

                            ?>

                        </span>

                    </div>

                    <div class="nav-icon"><i class="far fa-comments"></i></div>

                </a>

                <br>

            <?php } ?>

            <?php if ($user['type'] == 'Agency') { ?>

                <a href="../message-admin.php"

                   class="col-12 nav-main messages-inspector nav justify-content-between p-2">

                    <div class="nav-hide"><i class="far fa-comments"></i> Messages <span class="messages-badge">

                            <?php

                           $unreadq = $a->con->prepare("select red from messages where replyto = ? and red = 0");

                           $unreadq->execute([$_SESSION['user']]);


                            if ($unreadq->rowCount() > 0) {

                                ?>

                                &nbsp;<span class="badge badge-dark"><?php echo $unreadq->rowCount(); ?></span>

                                <?php

                            }

                            ?></span>

                    </div>

                    <div class="nav-icon"><i class="far fa-comments"></i></div>

                </a>

                <a href="../update-profile.php" class="col-12 nav-main updateprofile nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-user-cog"></i> Agency Profile</div>

                    <div class="nav-icon"><i class="fas fa-user-cog"></i></div>

                </a>

            <?php } else {

                ?>

                <a href="../update-profile.php" class="col-12 nav-main updateprofile nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-user-cog"></i> Profile</div>

                    <div class="nav-icon"><i class="fas fa-user-cog"></i></div>

                </a>

                <?php

            } ?>

            <a href="../update-password.php" class="col-12 nav-main updatepassword nav justify-content-between p-2">

                <div class="nav-hide"><i class="fas fa-key"></i> Change Password</div>

                <div class="nav-icon"><i class="fas fa-key"></i></div>

            </a>

            <?php

            if ($user['type'] == 'Admin') {

                ?>

                <br>

                <p style="font-size:90%;" class="pl-3 pr-3 text-light"><i class="fas fa-chevron-right fa-sm"></i>&nbsp;

                    CONFIGURATION</p>

                <a href="../setup.php" class="col-12 nav-main setup nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-cogs"></i> Portal Setup</div>

                    <div class="nav-icon"><i class="fas fa-cogs"></i></div>

                </a>

                <!--

                <a href="edit-marketing-types.php" class="col-12 nav-main marketingtypes nav justify-content-between p-2">

                    <div class="nav-hide"><i class="fas fa-cogs"></i> Marketing Types</div>

                    <div class="nav-icon"><i class="fas fa-cogs"></i></div>

                </a>

    -->

            <?php } ?>

            <a href="../signout.php" class="col-12 nav-main nav justify-content-between p-2">

                <div class="nav-hide"><i class="fas fa-sign-out-alt"></i> Logout</div>

                <div class="nav-icon"><i class="fas fa-sign-out-alt"></i></div>

            </a>





        </div>

    </div>



    <div id="page-content-wrapper">

        <div class="col-12 p-4">

            <nav class="navbar navbar-expand-lg navbar-dark bg-white border shadow-sm rounded-10">

                <button class="btn bg-transparent text-black p-2" id="menu-toggle"><i class="fas fa-bars fa-lg"></i>

                </button>

                <div class="ml-auto">

                    <div style="font-weight:500;">

                        <?php

                        if (isset($_SESSION['user'])) {

                            ?>

                            <div class="btn-group">

                                <?php

                                if ($user['type'] == 'Inspector') {

                                    ?>

                                    <a type="button"

                                       class="btn btn-dark bg-light btn-sm text-main border-main font-weight-500 px-4 mr-2 rounded-10 text-capitalize"

                                       data-toggle="modal" data-target="#linksModal">

                                        My Forms

                                    </a>

                                    <div class="modal fade" id="linksModal" tabindex="-1" role="dialog"

                                         aria-labelledby="linksModal" aria-hidden="true">

                                        <div class="modal-dialog" role="document">

                                            <div class="modal-content">

                                                <div class="modal-header bg-black text-center p-4 text-main">

                                                    <div class="text-center col-12 p-0">

                                                        <i class="fas fa-link fa-2x"></i>

                                                    </div>

                                                </div>

                                                <div class="modal-body text-center py-4 text-dark">

                                                    <?php

                                                    $mylinksq = $a->con->prepare("select * from customlinks where inspector = ?");

                                                    $mylinksq->execute([$_SESSION['user']]);

                                                    if ($mylinksq->rowCount() > 0) {

                                                        while ($mylinks = $mylinksq->fetch()) {

                                                            ?>

                                                            <div class=" mb-2">

                                                                <a class="btn btn-light font-95 col-10 border font-weight-500"

                                                                   target="_blank"

                                                                   href="<?php echo $mylinks['link'] ?>"><?php echo $mylinks['title'] ?>

                                                                    <i class="fas fa-long-arrow-alt-right fa-sm"></i></a>

                                                            </div>

                                                            <?php

                                                        }

                                                    } else {

                                                        ?>

                                                        <h5 class="text-center text-black">No Links Found</h5>

                                                        <?php

                                                    }

                                                    ?>

                                                    <div class="text-center mt-5">

                                                        <button type="button"

                                                                class="btn text-secondary px-5 border btn-light"

                                                                data-dismiss="modal" aria-label="Close">

                                                            Close ✕

                                                        </button>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <?php

                                }

                                ?>

                                <a type="button"

                                   class="btn btn-dark bg-black btn-sm text-light px-4 rounded-10 text-capitalize"

                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    <?php echo $user['name']; ?>

                                </a>

                            </div>

                            <?php

                        }

                        ?>

                    </div>

                </div>

            </nav>

        </div>



        <div class="modal fade" id="modal-default">

            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">

                        <h4 class="modal-title text-uppercase"></h4>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>

                    </div>

                    <div class="modal-body">

                        <p class="modal-text"></p>

                    </div>

                </div>

            </div>

        </div>



        <!-- Menu Toggle Script -->

        <script>

            $("#menu-toggle").click(function (e) {

                e.preventDefault();

                $("#wrapper").toggleClass("toggled");

            });



            $(".list-group").mouseover(function () {

                $("#wrapper").removeClass("toggled");

            });



            //$('.nav-div .nav-submenu').hide();

            //$(".nav-div a").click(function () {

            //	$(this).parent(".nav-div").children(".nav-submenu").slideToggle("100");

            ////	$(this).find(".right").toggleClass("fa-caret-up fa-caret-down");

            //});



            $('.sub-menu ul').hide();

            $(".sub-menu .nav").click(function () {

                $(this).parent(".sub-menu").children("ul").slideToggle("100");

                $(this).find(".right").toggleClass("fa-angle-up fa-angle-down");

            });



        </script>

        <?php

        if (isset($_SESSION['message'])) {

        if ($_SESSION['message'] == 'websiteupdated'){

            ?>

            <div class="alert alert-secondary fade show fixed-bottom alert-box mb-0"

                 style="font-size:90%; font-weight:500" role="alert">

                Website Data Updated Successfully.

            </div>

            <script>

                setTimeout(function () {

                    $(".alert-box").alert('close');

                }, 2000);



            </script>

        <?php

        }

        if ($_SESSION['message'] == 'profileupdated'){

        ?>

            <div class="alert alert-secondary fade show fixed-bottom alert-box mb-0"

                 style="font-size:90%; font-weight:500" role="alert">

                Your Profile has been Updated Successfully.

            </div>

            <script>

                setTimeout(function () {

                    $(".alert-box").alert('close');

                }, 2000);



            </script>

        <?php

        }

        if ($_SESSION['message'] == 'inboxed'){

        ?>

            <div class="alert alert-secondary fade show fixed-bottom alert-box mb-0"

                 style="font-size:90%; font-weight:500" role="alert">

                Moved to the Inbox Successfully.

            </div>

            <script>

                setTimeout(function () {

                    $(".alert-box").alert('close');

                }, 2000);



            </script>

        <?php

        }

        if ($_SESSION['message'] == 'password'){

        ?>

            <div class="alert alert-secondary fade show fixed-bottom alert-box mb-0"

                 style="font-size:90%; font-weight:500" role="alert">

                Password Updated Successfully.

            </div>

            <script>

                setTimeout(function () {

                    $(".alert-box").alert('close');

                }, 3000);



            </script>

        <?php

        }

        if ($_SESSION['message'] == 'profile'){

        ?>

            <div class="alert alert-secondary fade show fixed-bottom alert-box mb-0"

                 style="font-size:90%; font-weight:500" role="alert">

                Profile Updated Successfully.

            </div>

            <script>

                setTimeout(function () {

                    $(".alert-box").alert('close');

                }, 3000);



            </script>

            <?php

        }

            unset($_SESSION['message']);

        }

        ?>

