<?php
ini_set('session.gc_maxlifetime', 8 * 60 * 60); // Set session timeout to 8 hours.
ini_set('session.gc_probability', 1);
ini_set('session. gc_divisor', 1);

include('includes/common.class.php');
$common = new common();
?>
<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <?php include('includes/essentials.php'); ?>
</head>

<body>
<?php
if (!(isset($_SESSION['user']))) { ?>
    <div class="col-12 h-100">
        <div class="container nav justify-content-between p-0 py-5 h-100">
            <div class="col-lg-6 col-md-6 col-sm-12 h-100">
                <div class="text-center py-5 bg-black shadow nav align-items-top h-100">
                    <div class="col-12 p-0">
                        <a href="index.php"><img class="col-10 ml-auto mr-auto p-0" src="assets/images/logo.png"></a>
                        <h3 class="text-light mt-5 mb-4">Quick To Schedule<br>Accurate Same Day Reports</h3>
                        <br><br>
                        <br>
                        <h3 class="text-light">Insurance Agents/Agencies</h3>
                        <h3 class="text-light">Roofing Contractors</h3>
                        <h3 class="text-light">Realtors</h3>
                        <br>
                        <br>
                        <a href="register.php" class="btn bg-main text-white font-weight-500 px-5 mt-2">Register
                            Here</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <form method="post" class="col-12 p-5 pt-5 pb-5 text-dark rounded bg-white border login-form h-100"
                      enctype="multipart/form-data">
                    <h2 class="mb-0 text-black font-weight-bold" style="letter-spacing:0.8px;">Welcome Back !</h2>
                    <h6 class="mb-0 text-secondary" style="font-weight:600;">Sign in to continue</h6>
                    <hr>
                    <?php
                    if (isset($_SESSION['register']) && $_SESSION['register'] == 'successful') {
                        ?>
                        <div class="alert col-12 alert-light text-dark rounded-0 alert-dismissible fade show border"
                             role="alert" style="border-left:5px solid #FF0000 !important">
                            <strong>Account Registered Successfully! </strong>
                            <br>
                            Please check your email for account verification (check spam or junk mail). If you don't
                            receive an email, login to receive a new verification email. Thank You!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                        unset($_SESSION['register']);
                    }
                    ?>
                    <div>
                        <div class="form-group mb-3">
                            <label>Email Address</label>
                            <input name="email" required type="email" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label>Password</label>
                            <input name="password" required type="password" class="form-control">
                        </div>
                        <p class="text-right col-12 p-0 mt-3 mb-0"><a class=" text-accent" href="forgot-password.php">Forgot
                                Password ? </a></p>
                        <br>
                        <button type="submit" name="submit" class="col-12 btn bg-main border-dark text-light">Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } else { ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] == 'Admin') { ?>
        <div class="px-4 pt-2 pb-2 nav justify-content-between">
            <h3 class="mb-0 font-weight-bold text-black text-capitalize">Welcome, <span
                        class="text-main"><?php echo $user['name']; ?></span>
            </h3>
        </div>
        <div class="col-12 px-3 nav justify-content-start">
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-inspector">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Total Inspectors</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from users where type = 'Inspector'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0];
                            ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-agency">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Total Agency</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from users where type = 'Agency'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0];
                            ?></h2>
                    </div>
                </a>
            </div>
        </div>
        <br><br>
        <div class="px-4 pt-2 pb-2 nav justify-content-between">
            <h3 class="mb-0 font-weight-bold text-black text-capitalize">Requests
            </h3>
        </div>
        <div class="col-12 px-3 nav justify-content-start">
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-requests.php?status=Pending">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Inspection Requests<br>Received</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right mb-0 mt-3"><?php $q = $a->con->prepare("select count(*) from requests where status = 'Pending'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-requests.php?status=Assigned">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Inspection Requests<br>Assigned</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right mb-0 mt-3"><?php $q = $a->con->prepare("select count(*) from requests where status = 'Assigned'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-requests.php?status=Review">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Completed Inspections<br>For Review</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right mb-0 mt-3"><?php $q = $a->con->prepare("select count(*) from requests where status = 'Review'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="all-requests.php?status=Completed">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Completed &amp; Delivered<br>Inspections</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right mb-0 mt-3"><?php $q = $a->con->prepare("select count(*) from requests where status = 'Completed'");
                            $q->execute();
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
        </div>
        <?php
    } ?>
    <?php if ($user['type'] == 'Inspector') {
        ?>
        <div class="px-4 pt-2 pb-2 nav justify-content-between">
            <h3 class="mb-0 font-weight-bold text-black text-capitalize">Welcome, <span
                        class="text-main"><?php echo $user['name']; ?></span>
            </h3>
        </div>
        <div class="col-12 px-3 nav justify-content-start">
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="assigned-requests.php?status=Assigned">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Assigned Requests</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from requests where assigned = ? and status = 'Assigned'");
                            $q->execute([$_SESSION['user']]);
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="assigned-requests.php?status=Scheduled">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Scheduled Requests</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from requests where assigned = ? and status = 'Scheduled'");
                            $q->execute([$_SESSION['user']]);
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="assigned-requests.php?status=Review">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Submitted for Review Requests</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from requests where assigned = ? and status = 'Review'");
                            $q->execute([$_SESSION['user']]);
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 p-2">
                <a style="text-decoration:none;" href="assigned-requests.php?status=Completed">
                    <div class="col-12 p-4 bg-white shadow-sm border rounded-10 text-light">
                        <h6 class="text-black" style="font-weight:600;">Completed Requests</h6>
                        <h2 class="col-12 font-weight-bold text-black text-right"><?php $q = $a->con->prepare("select count(*) from requests where assigned = ? and status = 'Completed'");
                            $q->execute([$_SESSION['user']]);
                            $r = $q->fetch();
                            echo $r[0]; ?></h2>
                    </div>
                </a>
            </div>
        </div>
        <?php
    } ?>
    <?php if ($user['type'] == 'Agency') {
        ?>
        <style>
            .form-group > label {
                font-size: 110%;
                font-weight: 600;
            }

            .radios label {
                color: #525252;
                font-size: 100%;
            }

        </style>
        <div class="px-4 pt-2 pb-2 nav justify-content-between">
            <h3 class="mb-0 font-weight-bold text-black text-capitalize">Welcome, <span
                        class="text-main"><?php echo $user['name']; ?></span>
            </h3>
        </div>
        <div class="px-4 py-3 mb-5">
            <div class="row">
                <div class="col-md-8">
                    <div class="col-12 p-0 p-3 bg-white py-4 border rounded-10 shadow-sm">
                        <h4 class="text-main font-weight-bold">Submit New Request</h4>
                        <br>
                        <form method="post" class="col-12" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Select Inspection Type:</label>
                                <div class="row type">
                                    <?php
                                    $inspectiontypeq = $a->con->prepare("select * from inspectiontypes");
                                    $inspectiontypeq->execute();
                                    while ($inspectiontype = $inspectiontypeq->fetch()) {
                                        ?>
                                        <div class="col-md-6">
                                            <label class="px-2 py-1 border mb-2 border col-md-12">
                                                <input type="checkbox" value="<?php echo $inspectiontype['id']; ?>"
                                                       name="typecheck"> <?php echo $inspectiontype['name']; ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <input type="hidden" name="type" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Insured / Applicant Information</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="text" name="name" maxlength="255" class="form-control" required
                                               placeholder="Name">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="email" name="email" maxlength="255" class="form-control" required
                                               placeholder="Email Address">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="tel" name="phone" maxlength="30" class="form-control" required
                                               placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Subject Property Information</label>
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <input type="text" name="address" maxlength="255" class="form-control" required
                                               placeholder="Address">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="city" maxlength="30" class="form-control" required
                                               placeholder="City">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="state" maxlength="30" class="form-control" required
                                               placeholder="State">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="zipcode" maxlength="30" class="form-control" required
                                               placeholder="Zip Code">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Send Invoice(s) To:</label>
                                <div class="row invoices">
                                    <div class="col-md-6">
                                        <div>
                                            <label class="px-2 py-1 mb-1">
                                                <input type="checkbox" value="Homeowner" name="invoicehomeowner">
                                                Homeowner
                                            </label>
                                        </div>
                                        <div>
                                            <label class="px-2 py-1 mb-1">
                                                <input type="checkbox" value="Requester" name="invoicerequestor">
                                                Requester
                                            </label>
                                        </div>
                                        <div>
                                            <label class="px-2 py-1 mb-1 nav justify-content-start">
                                                <input type="checkbox" value="Other" name="invoiceother"> &nbsp;Other
                                                &nbsp;&nbsp;<input name="invoiceothertext"
                                                                   class="form-control-sm border-0" maxlength="50"
                                                                   style="border-bottom:1px solid #ccc !important;height:auto;padding-top:0px;">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="comments" class="px-2 py-1 mb-1 nav justify-content-start">Comments</label>
                                <textarea name="comments" class="form-control" rows="5" maxlength="2000"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="files" class="px-2 py-1 mb-1 nav justify-content-start">Related
                                    Files</label>
                                <input type="file" name="uploadfile"
                                       accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*"
                                       class="p-2 col-12 border"
                                       multiple/>

                            </div>
                            <div class="mt-5">
                                <button type="submit" name="submitrequest" class="btn bg-main text-white px-4">Submit
                                    Request
                                </button>
                            </div>
                        </form>
                        <script>
                            $('.type input[name="typecheck"]').change(function () {
                                var name = 'type';
                                if ($(this).prop("checked") == true) {
                                    var catg = $(this).val();
                                    if ($(".type input[name='" + name + "']").val().includes(catg) == true) {
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val());
                                    } else {
                                        if ($(".type input[name='" + name + "']").val() == '') {
                                            $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val() + catg);
                                        } else {
                                            $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val() + ' - ' + catg);
                                        }
                                    }
                                } else if ($(this).prop("checked") == false) {
                                    var catg = $(this).val();
                                    if ($(".type input[name='" + name + "']").val().includes(catg) == true) {
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(' - ' + catg, ''));
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(catg + ' - ', ''));
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(catg, ''));
                                    }
                                }
                            });

                        </script>
                        <?php
                        if (isset($_POST['submitrequest'])) {
                            // Add the new request to the database.
                            $reqid = substr(sha1($_POST['email'] . strtotime("now") . rand(1111, 9999)), 0, 6);
                            $invoicehomeowner = (isset($_POST['invoicehomeowner'])) ? 'Yes' : 'No';
                            $invoicerequestor = (isset($_POST['invoicerequestor'])) ? 'Yes' : 'No';
                            $invoiceother = (isset($_POST['invoiceother'])) ? 'Yes' : 'No';
                            $invoiceothertext = (isset($_POST['invoiceother'])) ? $_POST['invoiceothertext'] : '';

                            $query = $a->con->prepare("INSERT INTO requests(reqid, user, type, name, email, phone, address, city, state, zipcode,invoicehomeowner,invoicerequestor,invoiceother,invoiceothertext, comments) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                            $status = $query->execute([$reqid, $_SESSION['user'], $_POST['type'], $_POST['name'], $_POST['email'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zipcode'], $invoicehomeowner, $invoicerequestor, $invoiceother, $invoiceothertext, $_POST['comments']]);

                            if ($status) {
                                // Add any associated files to the database.
                                if (isset($_FILES) && $_FILES['uploadfile']['name'] != '') {
                                    $fileid = substr(sha1(strtotime("now") . rand(1111, 9999)), 0, 6);
                                    $img = $_FILES['uploadfile']['tmp_name'];
                                    $pic = $_FILES['uploadfile']['name'];
                                    $ext = pathinfo($pic, PATHINFO_EXTENSION);
                                    $roughmix = 'assets/images/requests/' . $reqid . sha1($pic . strtotime(("now") . rand(1111, 9999))) . '.' . $ext;

                                    $addfileq = $a->con->prepare("INSERT INTO requestfiles(fileid,request,agency,file,name,insertby) VALUES(?,?,?,?,?,?)");
                                    $st2 = $addfileq->execute([$fileid, $reqid, $_SESSION['user'], $roughmix, $pic, $_SESSION['user']]);

                                    if ($st2) {
                                        move_uploaded_file($img, $roughmix);
                                    }
                                }

                                // Send a notification to administrators.
                                $email_message = '
Hello There,<br />
<br />

A new request has been submitted on ' . $site['name'] . '<br />
Inspection Type: ' . $_POST[''] . '<br/>
Applicant Name: ' . $_POST['name'] . '<br />
Applicant Email: ' . $_POST['email'] . '<br />
Applicant Phone: ' . $_POST['phone'] . '<br />
<br />
Subject Address: ' . $_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $_POST['zipcode'] . '<br />
<br />
Comments: ' . $_POST['comments'] . '                
Send Invoices To: <br />';

                                if ($invoicehomeowner == 'Yes') {
                                    $email_message .= "Home Owner<br />";
                                }
                                if ($invoicerequestor == 'Yes') {
                                    $email_message .= "Requestor <br/>";
                                }
                                if ($invoiceother == 'Yes') {
                                    $email_message .= "Other: $invoiceothertext<br />";
                                }

                                $common->sendEmailToGroup("Admin", "New Request Created", $email_message, $headers);
                                $_SESSION['request'] = 'added';
                                echo ' <script> window.location = "/my-requests.php;";</script >';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <a href="my-requests.php?status=Pending">
                        <div class="col-12 p-3 py-5 text-center bg-white border rounded-10 shadow-sm mb-4">
                            <h4 class="mb-0 font-weight-bold text-black">Pending Requests</h4>
                        </div>
                    </a>
                    <a href="my-requests.php?status=Completed">
                        <div class="col-12 p-3 py-5 text-center bg-white border rounded-10 shadow-sm mb-4">
                            <h4 class="mb-0 font-weight-bold text-black">Completed Requests</h4>
                        </div>
                    </a>
                    <a href="final-reports.php">
                        <div class="col-12 p-3 py-5 text-center bg-white border rounded-10 shadow-sm mb-4">
                            <h4 class="mb-0 font-weight-bold text-black">Final Reports</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <?php
    } ?>
    <?php
}
?>
</body>

<?php include('includes/footer.php'); ?>

</html>

<script>
    $(".nav-main.index").addClass('active - link');

</script>
<?php
if (isset($_POST['submit'])) {
    $lr = $a->con->prepare("select status,userid,password,name from users where email = ?");
    $lr->execute([$_POST['email']]);
    if ($lr->rowCount() > 0) {
        $rl = $lr->fetch();
        $validPassword = password_verify($_POST['password'], $rl['password']);
        if ($validPassword) {
            if ($rl['status'] == 'Active') {
                $_SESSION['user'] = $rl['userid'];
                echo "<script> window.location = '/index.php'; </script>";
            } else if ($rl['status'] == 'Review') {
                echo "<script>
                        swal('Account Under Review', 'You will be notified once it is ready to use. Thank You!', 'error'); 
                        setTimeout(function(){
                        swal.close();
                        }, 5000);
                        </script>";
            } else if ($rl['status'] == 'Email') {

                $token = sha1(strtotime(date("Y-m-d H:i:s") . rand(1111, 9999)));
                $userdataq = $a->con->prepare("update users set token = ? where userid = ?");
                $userdataq->execute([$token, $rl['userid']]);

                $name = $rl['name'];
                $e = $_POST['email'];
                $message = 'Hi ' . $name . ', <br ><br >
                                            Thank your registering at ' . $site['name'] . ' .<br >
                                            Please click the link to verify your Account: <a href = "http://' . $_SERVER['SERVER_NAME'] . '/verify?e=' . $e . '&t=' . $token . '">Verify Account </a >
                                <br ><br >
                                or copy and paste this link in your browser: http://' . $_SERVER['SERVER_NAME'] . '/verify?e=' . $e . '&t=' . $token . '

                                <br ><br >
            Thank You!<br>
<b > ' . $site['name'] . ' </b >
            ';
                $sub = 'Email Verification | ' . $site['name'] . '';
                mail($e, $sub, $message, $headers);


                echo ' < script> swal("Your account must be verified and approved for access!", "Check your email for a new account verification. (Look in junk or spam folder)", "error"); </script > ';
            } else if ($rl['status'] == 'Inactive') {
                echo "<script>
swal('Account Inactive', 'Please contact admin . Thank You!', 'error');
setTimeout(function(){
swal.close();
}, 5000);
</script>";
            }
        } else {
            echo "<script>
swal('No Account Found!', 'Please check your input', 'error'); 
setTimeout(function(){
swal.close();
}, 5000);
</script>";
        }
    } else {
        echo "<script>
swal('No Account Found!', 'Please check your input', 'error'); 
setTimeout(function(){
swal.close();
}, 5000);
</script>";
    }
}
?>
