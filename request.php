<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <title>Request | <?php echo $site['name'] ?></title>
    <?php if ($user['type'] != 'Admin' && $user['type'] != 'Agency') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
   
    <?php
    if (isset($_GET['reqid'] ) ) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && ($_POST['address'] != '')) {
            // Update the request.
            $invoicehomeowner = (isset($_POST['invoicehomeowner'])) ? 'Yes' : 'No';
            $invoicerequestor = (isset($_POST['invoicerequestor'])) ? 'Yes' : 'No';
            $invoiceother = (isset($_POST['invoiceother'])) ? 'Yes' : 'No';
            $invoiceothertext = (isset($_POST['invoiceother'])) ? $_POST['invoiceothertext'] : '';

            $q2 = $a->con->prepare("UPDATE requests SET name = ?, email = ?, phone = ?, address = ?, city = ?, state = ?, zipcode = ?, comments = ?, invoicehomeowner = ?, invoicerequestor = ?, invoiceother = ?, invoiceothertext = ? WHERE reqid = ?");
            $st2 = $q2->execute([$_POST['name'], $_POST['email'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zipcode'], $_POST['comments'], $invoicehomeowner, $invoicerequestor, $invoiceother, $invoiceothertext, $_GET['reqid']]);
        }

        $requestq = $a->con->prepare("select * from requests where reqid = ?");
        $requestq->execute([$_GET['reqid']]);
        if ($requestq->rowCount() > 0) {
            $request = $requestq->fetch();
            $agencyq = $a->con->prepare("select * from users where userid = ?");
            $agencyq->execute([$request['user']]);
            $agency = $agencyq->fetch();
            if ($request['status'] == 'Submitted') {
                echo '<script> window.location = "/index.php" </script>';
            }
        } else {
            echo '<script> window.location = "/index.php" </script>';
        }
    }
    ?>
</head>

<body>
<div class="px-4 nav justify-content-between">
    <div>
        <h3 class="mb-1 font-weight-bold text-black">Request Information</h3>
        <h6 class="mb-0 font-95 col-12 px-2 p-1 bg-dark font-weight-600"><span
                    class="text-white">Request # <?php echo $_GET['reqid']; ?></span></h6>
    </div>
    <div>
        <?php if ($request['status'] == 'Pending') { ?>
            <div class="status-label">
                <h4 class="mb-1 font-weight-bold text-primary">Request Submitted</h4>
                <?php if ($user['type'] == 'Agency') { ?>
                    <h6 class="mb-0 font-95 col-12 px-2 p-1 bg-dark font-weight-600 text-white">Processing...</h6>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if ($request['status'] == 'Assigned') { ?>
            <div class="status-label">
                <h4 class="mb-1 font-weight-bold text-warning">Assigned</h4>
            </div>
        <?php } ?>
        <?php if ($request['status'] == 'Scheduled') { ?>
            <div class="status-label">
                <h4 class="mb-1 font-weight-bold text-danger">Scheduled</h4>
            </div>
        <?php } ?>
        <?php if ($request['status'] == 'Review') { ?>
            <div class="status-label">
                <h4 class="mb-1 font-weight-bold text-black">Under Review</h4>
            </div>
        <?php } ?>
        <?php if ($request['status'] == 'Completed') { ?>
            <div class="status-label">
                <h4 class="mt-12 font-weight-bold text-success">Completed <i class="fas fa-check-double fa-sm"></i></h4>
            </div>
        <?php } ?>
        <?php if ($request['assigned'] != '') {
            $inspectorq = $a->con->prepare("select * from users where userid = ?");
            $inspectorq->execute([$request['assigned']]);
            $inspector = $inspectorq->fetch();
            ?>
            <h6 class="mb-0 font-95 col-12 px-2 p-1 bg-dark font-weight-600 text-white"><span class="font-weight-500">Assigned To:</span> <?php echo $inspector['name']; ?>
            </h6>
            <?php
        }
        ?>
    </div>
</div>
<style>
    .timeline .single-timeline:not(:last-child) h5::after {
        position: absolute;
        display: inline-block;
        content: "";
        border-top: .2rem solid black;
        width: 8%;
        margin: 0 2%;
        transform: translateY(0.6rem);
    }

    .timeline .single-timeline {
        width: 20%;
    }

    .opacity-50 {
        opacity: 50%;
    }
</style>

<div class="pl-4 pr-4 p-3 text-dark">
    <?php
    if (isset($_SESSION['file'])) {
        if ($_SESSION['file'] == 'added') {
            ?>
            <div class="alert alert-success font-weight-bold">
                File(s) Added Successfully
            </div>
            <?php
        }
        if ($_SESSION['file'] == 'deleted') {
            ?>
            <div class="alert alert-success font-weight-bold">
                File Deleted Successfully
            </div>
            <?php
        }
        unset($_SESSION['file']);
    } ?>
    <div class="col-12 p-4 bg-white shadow-sm brequest rounded-10">
        <div class="row">
            <?php if ($request['status'] == 'Cancelled') {
                $cancelbyq = $a->con->prepare("select name,type from users where userid = ?");
                $cancelbyq->execute([$request['cancelby']]);
                $cancelby = $cancelbyq->fetch();
                ?>
                <div class="col-12">
                    <div class="alert font-weight-600 mb-3 alert-light text-black p-3 rounded-10 h6 border"
                         style="border-left:5px solid red !important;">
                        <h5 class="text-danger font-weight-bold mb-0">CANCELLED</h5>
                        <hr class="my-2">
                        This request has been Cancelled by
                        the <?php echo $cancelby['type'] . ' <span class="text-dark">[' . $cancelby['name'] . ']</span>'; ?>
                        <p class="mb-0 font-weight-500 mt-1 font-95 text-dark">
                            Reason: <?php echo $request['cancel']; ?></p>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-12 mb-3 nav justify-content-between timeline" <?php if ($request['status'] == 'Cancelled') {
                echo 'style="opacity:0.8;background:rgba(000,000,000,0.3);padding-top:30px;padding-bottom:20px;border-radius:20px;"';
            } ?>>
                <div class="text-center single-timeline">
                    <i class="fas fa-check-circle text-success fa-3x"></i>
                    <h5 class="font-weight-600 mt-2 text-black">Submitted</h5>
                    <h6 class="font-weight-500 mt-2 text-secondary font-95"><?php echo date("M dS, Y", strtotime($request['inserton'])); ?></h6>
                </div>
                <div class="text-center single-timeline <?php echo ($request['assigndate'] != '') ? '' : 'opacity-50'; ?>">
                    <i class="<?php echo ($request['assigndate'] != '') ? 'fas fa-check-circle' : 'far fa-clock'; ?> text-success fa-3x"></i>
                    <h5 class="font-weight-600 mt-2 text-black">Assigned</h5>
                    <h6 class="font-weight-500 mt-2 text-secondary font-95"><?php echo ($request['assigndate'] != '') ? date("M dS, Y", strtotime($request['assigndate'])) : '---- ---- ------'; ?></h6>
                </div>
                <div class="text-center single-timeline <?php echo ($request['appointment_start'] != '') ? '' : 'opacity-50'; ?>">
                    <?php
                    $getschecq = $a->con->prepare("SELECT * FROM SCHEDULE where reqid = ? and id <> (SELECT MAX(ID) FROM schedule WHERE reqid = ?)");
                    $getschecq->execute([$request['reqid'], $request['reqid']]);
                    $scheduletooltip = '<b>Scheduled History</b><hr>';

                    while ($getschec = $getschecq->fetch()) {
                        $scheduletooltip .= '<div><i></i>' . date("M dS, Y h:i A", strtotime($getschec["appointment_start"])) . '</div>';
                    }
                    ?>
                    <i class="<?php echo ($request['appointment_start'] != '') ? 'fas fa-check-circle' : 'far fa-clock'; ?> text-success fa-3x"></i>
                    <h5 class="font-weight-600 mt-2 text-black">Scheduled <?php if ($getschecq->rowCount() > 0) { ?> <i
                                class="fas fa-history pl-1" data-toggle="tooltip" data-placement="top"
                                title="<?php echo $scheduletooltip; ?><b><?php echo ($request['appointment_start'] != '') ? date("M dS, Y h:i A", strtotime($request['appointment_start'])) : ''; ?></b>"></i> <?php } ?>
                    </h5>
                    <h6 class="font-weight-500 mt-2 text-secondary font-95"><?php echo ($request['appointment_start'] != '') ? date("M dS, Y h:i A", strtotime($request['appointment_start'])) : '---- ---- ------'; ?></h6>
                </div>
                <div class="text-center single-timeline <?php echo ($request['reviewdate'] != '') ? '' : 'opacity-50'; ?>">
                    <i class="<?php echo ($request['reviewdate'] != '') ? 'fas fa-check-circle' : 'far fa-clock'; ?> text-success fa-3x"></i>
                    <h5 class="font-weight-600 mt-2 text-black">Under Review</h5>
                    <h6 class="font-weight-500 mt-2 text-secondary font-95"><?php echo ($request['reviewdate'] != '') ? date("M dS, Y", strtotime($request['reviewdate'])) : '---- ---- ------'; ?></h6>
                </div>
                <div class="text-center single-timeline <?php echo ($request['completedate'] != '') ? '' : 'opacity-50'; ?>">
                    <i class="<?php echo ($request['completedate'] != '') ? 'fas fa-check-circle' : 'far fa-clock'; ?> text-success fa-3x"></i>
                    <h5 class="font-weight-600 mt-2 text-black">Completed</h5>
                    <h6 class="font-weight-500 mt-2 text-secondary font-95"><?php echo ($request['completedate'] != '') ? date("M dS, Y", strtotime($request['completedate'])) : '---- ---- ------'; ?></h6>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <form name="requestForm" id="requestForm" method="post">
                    <table class="table table-responsive brequest">
                        <tr>
                            <td>Applicant Name</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="name" type="text" class="form-control"
                                           value="<?php echo($request['name']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['name']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Applicant Email</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="email" type="text" class="form-control"
                                           value="<?php echo($request['email']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['email']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Applicant Phone</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="phone" type="text" class="form-control"
                                           value="<?php echo($request['phone']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['phone']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="address" type="text" class="form-control"
                                           value="<?php echo($request['address']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['address']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>City</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="city" type="text" class="form-control"
                                           value="<?php echo($request['city']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['city']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>State</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="state" type="text" class="form-control"
                                           value="<?php echo($request['state']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['state']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Zip Code</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <input name="zipcode" type="text" class="form-control"
                                           value="<?php echo($request['zipcode']); ?>"/>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['zipcode']) ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Send Invoice(s) To:</td>
                            <td>
                                <?php if ($user['type'] == 'Admin') { ?>
                                    <div>
                                        <label class="px-2 py-1 mb-1">
                                            <input type="checkbox" value="Homeowner"
                                                   name="invoicehomeowner" <?php echo($request['invoicehomeowner'] == 'Yes' ? 'checked' : '') ?>>
                                            Homeowner
                                        </label>
                                    </div>
                                    <div>
                                        <label class="px-2 py-1 mb-1">
                                            <input type="checkbox" value="Requester"
                                                   name="invoicerequestor" <?php echo($request['invoicerequestor'] == 'Yes' ? 'checked' : '') ?>>
                                            Requester
                                        </label>
                                    </div>
                                    <div>
                                        <label class="px-2 py-1 mb-1 nav justify-content-start">
                                            <input type="checkbox" value="Other"
                                                   name="invoiceother" <?php echo($request['invoiceother'] == 'Yes' ? 'checked' : '') ?> />
                                            &nbsp;Other
                                            <input name="invoiceothertext"
                                                   class="form-control-sm border-0" maxlength="50"
                                                   style="border-bottom:1px solid #ccc !important;height:auto;padding-top:0px;"
                                                   value="<?php echo($request['invoiceothertext']) ?>">
                                        </label>
                                    </div>
                                <?php } else { ?>
                                    <?php echo ($request['invoicehomeowner'] == 'Yes') ? '<div>Homeowner</div>' : ''; ?><?php echo ($request['invoicerequestor'] == 'Yes') ? '<div>Requestor</div>' : ''; ?><?php echo ($request['invoiceother'] == 'Yes') ? '<div>Other: ' . $request['invoiceothertext'] . '</div>' : ''; ?>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Comments:</td>
                            <?php
                            if ($user['type'] == 'Admin') {
                                ?>
                                <td>
                                    <textarea name="comments" type="text"
                                              class="form-control"><?php echo($request['comments']); ?></textarea>
                                </td>
                                <?php
                            } else {
                                ?>
                                <td><?php echo($request['comments']) ?></td>
                            <?php } ?>
                        </tr>
                        <?php if ($user['type'] == 'Admin') { ?>
                            <tfoot>
                            <tr>
                                <td>
                                    <input type="submit" class="btn btn-primary" value="Update"/>
                                    <input id="btn-delete" type="button" class="btn btn-danger" value="Delete"/>
                                </td>
                            </tr>
                            </tfoot>
                        <?php } ?>
                    </table>
                </form>
            </div>
            <div class="col-md-6 mb-3">
                <!--                    <h5 class="py-2 bg-white font-weight-600 text-black mb-0">Clinic Details</h5>-->
                <table class="table brequest">
                    <tr>
                        <td>Company Name</td>
                        <td><?php echo $agency['company']; ?></td>
                    </tr>
                    <tr>
                        <td>Company Address</td>
                        <td><?php echo $agency['address']; ?></td>
                    </tr>
                    <tr>
                        <td>Company Phone</td>
                        <td><?php echo $agency['companyphone']; ?></td>
                    </tr>
                    <tr>
                        <td>Agent Name</td>
                        <td><?php echo $agency['name']; ?></td>
                    </tr>
                    <tr>
                        <td>Agent Direct Number</td>
                        <td><?php echo $agency['phone']; ?></td>
                    </tr>
                    <tr>
                        <td>Agent Email</td>
                        <td><?php echo $agency['email']; ?></td>
                    </tr>
                </table>
                <!---xxx--->
                <?php if ($user['type'] == 'Admin') { ?>
                <?php if ($request['status'] == 'Assigned') { ?>
                    <div class="mt-3 mb-3 scheduled<?php echo $request['reqid']; ?>">
                        <span class="btn btn-danger col-12 shadow-sm font-weight-600 btn-sm pointer">
                            Schedule Inspection &nbsp;
                            <i class="fas fa-arrow-down fa-sm"></i></span>
                        <br>
                        <div class="input-group">
                            <input type="date" class="border p-2 col-7" name="date"
                                   min="<?php echo date("Y-m-d"); ?>" required>
                            <input type="time" class="border p-2 col-5" name="time" required>
                        </div>
                        <button class="btn btn-sm col-12 btn-light font-weight-500 btn-schedule border"
                                id="<?php echo $request['reqid']; ?>">
                            Submit &nbsp;
                            <i class="fas fa-save fa-sm"></i>
                        </button>
                    </div>
                <?php } ?>
                <?php if ($request['status'] == 'Scheduled') { ?>
                <div class="status-label mt-2">
                    <span class="btn btn-sm btn-danger font-weight-500 py-0">Scheduled</span>
                    <div id="reschedule<?php echo $request['reqid']; ?>" class="collapse">
                        <div class="input-group">
                            <input type="date" class="border p-2 col-7" name="date"
                                   min="<?php echo date("Y-m-d"); ?>" required>
                            <input type="time" class="border p-2 col-5" name="time" required>
                        </div>
                        <button class="btn btn-sm col-12 btn-light font-weight-500 btn-reschedule border"
                                id="<?php echo $request['reqid']; ?>">Submit &nbsp;<i
                                    class="fas fa-save fa-sm"></i></button>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <?php if ($request['status'] == 'Scheduled') { ?>
                        <span id="btn-calendar" class="btn btn-sm btn-dark font-weight-500 py-0 shadow pointer"><i
                                    class="fas fa-calendar"></i> Add to Calendar </span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <?php if ($user['type'] == 'Admin') { ?>
        <div class="modal fade" id="filemodal" tabindex="-1" role="dialog" aria-labelledby="filemodal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-main text-center p-4 text-light">
                        <div class="text-center col-12 p-0">
                            <i class="fas fa-upload fa-3x"></i>
                        </div>
                    </div>
                    <div class="modal-body text-center py-4 text-dark">
                        <h3 class="text-center">Please Select File</h3>
                        <form method="post" enctype="multipart/form-data">
                            <div class="col-md-9 mx-auto">
                                <input type="file" name="file" class="p-2 col-12 border" required>
                                <input class="form-control mt-3" placeholder="File Title" name="name" required>
                            </div>
                            <div class="text-center mt-4">
                                <button type="button" class="btn text-secondary border btn-light" data-dismiss="modal"
                                        aria-label="Close">
                                    Close ✕
                                </button>
                                &nbsp;&nbsp;
                                <button type="submit" name="uploadfile" class="btn bg-main text-white">
                                    Submit <i class="fas fa-upload fa-sm pl-1"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($_POST['uploadfile'])) {
            $fileid = substr(sha1(strtotime("now") . rand(1111, 9999)), 0, 6);
            $img = $_FILES['file']['tmp_name'];
            $pic = $_FILES['file']['name'];
            $ext = pathinfo($pic, PATHINFO_EXTENSION);
            $roughmix = 'assets/images/requests/' . $_GET['reqid'] . sha1($pic . strtotime(("now") . rand(1111, 9999))) . '.' . $ext;
            $addfileq = $a->con->prepare("insert into requestfiles(fileid,request,agency,file,name,insertby) values(?,?,?,?,?,?)");
            $st2 = $addfileq->execute([$fileid, $_GET['reqid'], $request['user'], $roughmix, $pic, $_SESSION['user']]);

            if ($st2) {
                move_uploaded_file($img, $roughmix);
            }

            $_SESSION['file'] = 'added';
            echo '<script> window.location = window.location.href; </script>';
            exit();
        }
        ?>
    <?php } ?>
    <?php if ($user['type'] == 'Agency' && $request['status'] == 'Completed' || $user['type'] == 'Admin') { ?>
    <div class="col-12 p-4 mb-5 bg-white shadow-sm border rounded-10">
        <div class="nav justify-content-between">
            <div class="nav justify-content-start mb-2">
                <div>
                    <h4 class="font-weight-bold mt-1 mb-0 text-black">File Uploads</h4>
                </div>
                <?php if ($user['type'] == 'Admin') { ?>
                    <button class="ml-3 btn btn-primary btn-sm border font-weight-500 add-file" data-toggle="modal"
                            data-target="#filemodal">Add New File <i class="fas fa-plus fa-sm pl-1"></i></button>
                <?php } ?>
            </div>
            <div class="mb-2">
                <?php
                if ($user['type'] == 'Admin') {
                    if ($request['status'] == 'Review') { ?>
                        <div class="mark-box<?php echo $request['reqid']; ?>">
                            <button id="<?php echo $request['reqid']; ?>"
                                    class="btn btn-success col-12 shadow-sm font-weight-500 pointer btn-complete">
                                Mark Completed <i class="fas fa-check-double fa-sm"></i></button>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
        <?php } ?>
        <div class="row px-2">
            <?php
            $query = $a->con->prepare("select * from requestfiles where request = ? order by id desc");
            $query->execute([$_GET['reqid']]);
            $cc = 1;

            while ($res = $query->fetch()) {
                ?>
                <div class="col-lg-2 col-md-3 col-sm-12 p-2 file-hover">
                    <div class="col-12 bg-white p-0 text-center h-100 border rounded-10 shadow-sm">
                        <a class="col-12 p-0 text-dark" href="<?php echo $res['file']; ?>"
                           style="text-decoration:none;" download="<?php echo $res['name']; ?>">
                            <div class="col-12 bg-white px-3 pt-3 pb-2 rounded-10 text-center">
                                <img class="col-12 p-0 mb-2" src="assets/images/icons/file.png">
                                <div style="font-weight:600;"><?php echo $res['name']; ?></div>
                            </div>
                        </a>
                        <?php if ($user['type'] == 'Admin') { ?>
                            <p class="mb-0 text-danger delete-file pb-1 font-90 font-weight-500 pointer"
                               id="<?php echo $res['id']; ?>">Delete this file</p>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php include('includes/footer.php'); ?>
</body>

</html>
<?php if ($user['type'] == 'Admin') { ?>
    <script>
        $(".btn-complete").click(function () {
            var i = $(this).attr('id');
            swal({
                title: "Are you sure to Mark it as Completed?",
                text: "You will not be able to revert this action!",
                icon: "warning",
                className: "text-center",
                buttons: true,
                showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'markcompleted',
                            i: i
                        },
                        success: function (data) {
                            window.location = window.location.href;
                        }
                    });
                }
            });
        });
        $('#btn-delete').click(function () {
            swal({
                title: "Confirmation",
                text: "Are you sure you want to delete this request?",
                icon: "warning",
                className: "text-center",
                buttons: true,
                showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'deleteRequest',
                            reqid: '<?php echo $_GET['reqid'] ?>'
                        },
                        success: function (data) {
                            window.location.reload();
                        }
                    });
                } else {

                }
            });
        });
        $('.delete-file').click(function () {
            swal({
                title: "Confirmation",
                text: "Are you sure you want to delete this file? This action is irreversible.",
                icon: "warning",
                className: "text-center",
                buttons: true,
                showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'deletefile',
                            reqid: '<?php echo $_GET['reqid'] ?>',
                            i: $(this).attr('id')
                        },
                        success: function (data) {
                            window.location = window.location.href;
                        }
                    });
                } else {

                }
            });
        });
        $(".btn-schedule").click(function () {
            var i = $(this).attr('id');
            var date = $("input[name='date']").val();
            var time = $("input[name='time']").val();
            if (date != '' && time != '') {
                swal({
                    title: "Schedule it for " + date + " " + time + "?",
                    text: "You will not be able to revert this action!",
                    icon: "warning",
                    className: "text-center",
                    buttons: true, showCancelButton: true,
                    dangerMode: true,
                }, function (Proceed) {
                    if (Proceed) {
                        $.ajax({
                            url: '/adminfunctions',
                            type: 'post',
                            data: {
                                t: 'schedulerequest',
                                date: date,
                                time: time,
                                i: i
                            },
                            success: function (data) {
                                if (data != '') {
                                    $(".scheduled" + i).html('<hr class="my-2"><span class="font-weight-600">Scheduled To</span><div class="font-weight-500"><i class="far fa-clock fa-sm pr-1"></i> ' + data + '</div>');
                                    $(".row" + i + " .status-label").html('<span class="btn btn-sm btn-danger font-weight-500 py-0">Scheduled</span>');
                                }
                            }
                        });
                    }
                });
            }
        });
        $(".btn-reschedule").click(function () {
            var i = $(this).attr('id');
            var date = $("input[name='date']").val();
            var time = $("input[name='time']").val();
            if (date != '' && time != '') {
                swal({
                    title: "Reschedule for " + date + " " + time + "?",
                    text: "The appointment will be scheduled for the above time.",
                    icon: "warning",
                    className: "text-center",
                    buttons: true,
                    showCancelButton: true,
                    dangerMode: true,
                }, function (Proceed) {
                    if (Proceed) {
                        $.ajax({
                            url: '/adminfunctions',
                            type: 'post',
                            data: {
                                t: 'schedulerequest',
                                date: date,
                                time: time,
                                i: i
                            },
                            success: function (data) {
                                if (data != '') {
                                    $(".scheduledhistory" + i).html(data);
                                    $('#reschedule' + i).collapse('hide');
                                }
                            }
                        });
                    }
                });
            }
        });
        $('#btn-calendar').click(function () {
            $.ajax({
                url: '/operationsx.php',
                type: 'post',
                data: {
                    t: 'addCalendar',
                    reqId: '<?php echo $_GET['reqid'] ?>'
                },
                success: function (data) {
                    window.open("data:text/calendar;charset=utf8," + escape(data));
                }
            });
        });
    </script>
<?php } ?>
