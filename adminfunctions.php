<?php
session_start();
include('includes/config.php');
include('includes/common.class.php');

$common = new common();
$con = new PDO($SETTINGS['host'], $SETTINGS['mysql_user'], $SETTINGS['mysql_pass']);
$t = $_POST['t'];
$sitequery = $con->prepare("select * from website");
$sitequery->execute();
$site = $sitequery->fetch();
$sitename = $site['name'];
$siteurl = "http://windmitigations.developmint.xyz/";
$sitemail = "donot-reply@windmitigations.developmint.xyz";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: $sitename <$sitemail>\nX-Mailer: PHP/";
if (isset($_SESSION['user'])) {
    $userq = $con->prepare("SELECT * FROM users WHERE userid = ?");
    $userq->execute([$_SESSION['user']]);
    $user = $userq->fetch();
    if ($user['status'] == 'Inactive') {
        echo "<script> window.location = 'signout.php' </script>";
    }
    if ($user['type'] == 'Admin') {
        switch ($t) {
            case "assigninspector":
                break;
            case "activeagencyaccount":
                break;
            case "changestatus":
                break;
            case "deleterecord":
                break;
            case "deleteRequest":
                $q = $con->prepare("UPDATE requests SET deleted = true, deleted_by = ?, deleted_date = ? WHERE reqid = ?");
                $st = $q->execute([$_SESSION['user'], date('Y-m-d H:i:s'), $_POST['reqid'],]);

                if ($st) {
                    echo 'deleted';
                }
                break;
            case "deleteUser":
                $q = $con->prepare("UPDATE users SET deleted = true, deleted_by = ?, deleted_date = ? WHERE userid = ?");
                $st = $q->execute([$_SESSION['user'], date('Y-m-d H:i:s'), $_POST['userId'],]);

                if ($st) {
                    echo 'deleted';
                }
                break;
            case "markcompleted":
                break;
            case "addnewbutton":
                break;
            case "deletelink":
                break;
            case "deletefile":
                break;
            case "getinspectorlinks":
                break;
        }
        if ($t == "assigninspector") {
            $q = $con->prepare("UPDATE requests SET assigned = ?, assigndate = ?,status=? WHERE reqid = ?");
            $st = $q->execute([$_POST['val'], date("Y-m-d H:i:s"), 'Assigned', $_POST['i']]);
            if ($st) {
                $addnotif = $con->prepare("INSERT INTO notifs(user,notif,reqid) VALUES(?,?,?)");
                $addnotif->execute([$_POST['val'], 'A request has been assigned to you.', $_POST['i']]);

                // Send email notification to the selected inspector.
                $message = '
A new request has been assigned to you on ' . $site['name'] . '. <br /><br>
You can view the request here: https://windmitigationlogin.com/request?reqid=' . $_POST['i'];

                $common->sendEmailToUser($_POST['val'], "A new request has been assigned to you!", $message, $headers);
                echo 'assigned';
            }
        }
        if ($t == "activeagencyaccount") {
            $getuserq = $con->prepare("select name,email from users where id = ?");
            $getuserq->execute([$_POST['i']]);
            $getuser = $getuserq->fetch();
            $q = $con->prepare("update users set status = ? where id = ?");
            $q->execute(['Active', $_POST['i']]);
            $e = $getuser['email'];
            $name = $getuser['name'];
            $message = '
    Hi ' . $name . '<br>
    This is to inform you that your account has been approved and successfully activated.<br>Welcome to the ' . $site['name'] . ' family. You can now login and access your account. <br><br>
    <a href="http://' . $_SERVER['SERVER_NAME'] . '/index">Login to you account</a>
    <br><br>
    Thank You!<br>
    <b>' . $site['name'] . '</b>';
            $sub = "Account Activated Successfully";
            if (mail($e, $sub, $message, $headers)) {
            }
        }
        if ($t == "changestatus") {
            $d = $_POST['d'];
            $q = $con->prepare("update $d set status = ? where id = ?");
            $q->execute([$_POST['txt'], $_POST['i']]);
        }
        if ($t == "deleterecord") {
            $d = $_POST['d'];
            $q = $con->prepare("delete from $d where id = ?");
            $q->execute([$_POST['i']]);
        }
        if ($t == "markcompleted") {
            $q = $con->prepare("update requests set completedate = ?,status=? where reqid = ?");
            $st = $q->execute([date("Y-m-d H:i:s"), 'Completed', $_POST['i']]);
            if ($st) {
                $addnotif = $con->prepare("insert into notifs(user,notif,reqid) values(?,?,?)");
                $addnotif->execute([$_SESSION['user'], 'You have marked a request completed.', $_POST['i']]);
                echo date("M dS, Y h:i A");
            }
        }
        if ($t == "addnewbutton") {
            if ($_POST['linkid'] != '') {
                $linkq = $con->prepare("select * from customlinks where inspector = ? and id = ?");
                $linkq->execute([$_POST['inspector'], $_POST['linkid']]);
                if ($linkq->rowCount() > 0) {
                    $link = $linkq->fetch();
                    $updatelinkq = $con->prepare("update customlinks set title = ?, link = ?,updatedat=? where id = ?");
                    $updatelinkq->execute([$_POST['title'], $_POST['link'], date("Y-m-d H:i:s"), $link['id']]);
                }
            } else {
                $addlinkq = $con->prepare("insert into customlinks(inspector,title,link,insertby) values(?,?,?,?)");
                $addlinkq->execute([$_POST['inspector'], $_POST['title'], $_POST['link'], $_SESSION['user']]);
            }
        }
        if ($t == "getinspectorlinks") {
            ?>
            <table class="table table-sm">
                <tr>
                    <th>#</th>
                    <th>Button Title</th>
                    <th>Link</th>
                </tr>
                <?php
                $customlinksq = $con->prepare("select * from customlinks where inspector = ?");
                $customlinksq->execute([$_POST['inspector']]);
                $clc = 1;
                while ($customlinks = $customlinksq->fetch()) {
                    ?>
                    <tr>
                        <td><?php echo $clc; ?></td>
                        <td><?php echo $customlinks['title']; ?></td>
                        <td><a target="_blank"
                               href="<?php echo $customlinks['link']; ?>"><?php echo $customlinks['link']; ?></a></td>
                        <td>
                            <a class="font-weight-500 pointer text-primary"
                               href="?x=<?php echo $_GET['x'] ?>&link=<?php echo $customlinks[0]; ?>">edit</a>
                            &nbsp; | &nbsp;
                            <span class="font-weight-500 pointer delete-link text-danger"
                                  onclick="deletelink(<?php echo $customlinks[0]; ?>)">delete</span>
                        </td>
                    </tr>
                    <?php
                    $clc++;
                }
                ?>
            </table>
            <?php
        }
        if ($t == "deletelink") {
            $d = $_POST['d'];
            $q = $con->prepare("delete from customlinks where id = ? and inspector = ?");
            $q->execute([$_POST['i'], $_POST['inspector']]);
        }
        if ($t == "deletefile") {
            $q = $con->prepare("delete from requestfiles where id = ? and reqid = ?");
            $q->execute([$_POST['i'], $_POST['reqid']]);
            $_SESSION['file'] = 'deleted';
        }
    }

    if ($t == "schedulerequest") {
        $q = $con->prepare("UPDATE requests SET appointment_start = ?, appointment_end = ?, scheduledate = ?,status=? WHERE reqid = ?");
        $appointment_start = $_POST['date'] . ' ' . $_POST['time'];
        $appointment_end = strtotime('+1 hour', $appointment_start);

        $st = $q->execute([$appointment_start, $appointment_end, date("Y-m-d H:i:s"), 'Scheduled', $_POST['i']]);
        if ($st) {
            $addschedule = $con->prepare("INSERT INTO schedule(reqid,date,addedby) values(?,?,?)");
            $addschedule->execute([$_POST['i'], $_POST['date'] . ' ' . $_POST['time'], $_SESSION['user']]);
            $addnotif = $con->prepare("insert into notifs(user,notif,reqid) values(?,?,?)");
            $addnotif->execute([$_SESSION['user'], 'You have scheduled a request.', $_POST['i']]);

            $getschecq = $con->prepare("select * from schedule where reqid = ? and id <> (SELECT MAX(ID) FROM schedule where reqid = ?)");
            $getschecq->execute([$_POST['i'], $_POST['i']]);
            while ($getschec = $getschecq->fetch()) {

                echo '<div style="opacity:0.5;"><i class="fas fa-history fa-sm pr-1"></i> ' . date("M dS, Y h:i A", strtotime($getschec["date"])) . '</div>';
            }

            echo '<div class="font-weight-500"><i class="far fa-clock fa-sm pr-1"></i> ' . date("M dS, Y h:i A", strtotime($_POST['date'] . ' ' . $_POST['time'])) . '</div>';
        }
    }
    if ($t == "submitreviewrequest") {
        $q = $con->prepare("update requests set reviewdate = ?,status=? where reqid = ?");
        $st = $q->execute([date("Y-m-d H:i:s"), 'Review', $_POST['i']]);
        if ($st) {
            $addnotif = $con->prepare("insert into notifs(user,notif,reqid) values(?,?,?)");
            $addnotif->execute([$_SESSION['user'], 'You have submitted a request for a review.', $_POST['i']]);
            echo date("M dS, Y h:i A");
        }
    }
}
?>
