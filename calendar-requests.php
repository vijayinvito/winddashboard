<?php

include('includes/common.class.php');

$common = new common();

?>

<?php session_start(); ?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="UTF-8">

    <title>Request Calendar</title>



    <script src='calendar/fullcalendar/packages/core/main.js'></script>

    <script src='calendar/fullcalendar/packages/interaction/main.js'></script>

    <script src='calendar/fullcalendar/packages/daygrid/main.js'></script>

    <script src='calendar/fullcalendar/packages/timegrid/main.js'></script>

    <script src='calendar/fullcalendar/packages/list/main.js'></script>

    <script src="https://unpkg.com/popper.js@1.16.1/dist/umd/popper.min.js"></script>



    <?php include('includes/essentials.php'); ?>



    <link href='calendar/fullcalendar/packages/core/main.css' rel='stylesheet'/>

    <link href='calendar/fullcalendar/packages/daygrid/main.css' rel='stylesheet'/>

    <link href='calendar/fullcalendar/packages/timegrid/main.css' rel='stylesheet'/>

    <link href='calendar/fullcalendar/packages/list/main.css' rel='stylesheet'/>

</head>



<body>

<?php include('includes/header.php'); ?>



<?php if ($user['type'] == 'Admin' || $user['type'] == 'Inspector') { ?>

    <div class="container">

        <div class="row">

            <div id="calendar-container" class="col-12 px-3 nav justify-content-start" style="display:block;">

                <div id='loading'>loading...</div>

                <div id='calendar'></div>

            </div>

        </div>

    </div>



    <script>

        document.addEventListener('DOMContentLoaded', function () {

            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {

                plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],

                header: {

                    left: 'prev,next today',

                    center: 'title',

                    right: 'dayGridMonth,timeGridWeek,timeGridDay'

                },

                editable: false,

                navLinks: true, // can click day/week names to navigate views

                eventLimit: true, // allow "more" link when too many events

                selectable: false,

                eventRender: function (info) {

                    let eventTitle = info.event.title;

                    let location = "at " + info.event.extendedProps.location;

                    let eventTest = "Test";



                    $(info.el).popover({

                        title: info.event.title,

                        placement: 'top',

                        trigger: 'focus click',

                        content: `${location}<br />${info.event.extendedProps.description}`,

                        container: 'body',

                        html: true

                    }).popover('show');

                },

                events: {

                    url: 'calendar/events.php',

                    failure: function () {

                        document.getElementById('script-warning').style.display = 'block';

                    },

                    success: function (eventSource) {

                        calendar.removeAllEvents();



                        eventSource.forEach(element => {

                            newEvent = {

                                title: element.name,

                                start: element.appointment_start,

                                end: element.appointment_end,

                                allDay: false,

                                html: true,

                                location: `${element.address}<br />${element.city}, ${element.state}, ${element.zipcode}`,

                                description: `Inspector: ${element.inspector_name}<br /><?php if($user['type'] == 'Admin') {?> <a target="_blank" href="../request?reqid=${element.reqid}">View Request</a><?php } ?>`

                            };



                            calendar.addEvent(newEvent);

                        })

                    }

                },

                loading: function (bool) {

                    document.getElementById('loading').style.display =

                        bool ? 'block' : 'none';

                }

            });



            calendar.render();

        });

    </script>

<?php } ?>

</body>

</html>