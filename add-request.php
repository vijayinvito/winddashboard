<?php
include('includes/common.class.php');
$common = new common();
?>
<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <?php include('includes/essentials.php'); ?>
</head>

<body>
<?php include('includes/header.php'); ?>
<?php if ($user['type'] == 'Admin') { ?>

    <div class="px-4 py-3 mb-5">
        <div class="row">
            <div class="col-md-8">
                <div class="col-12 p-0 p-3 bg-white py-4 border rounded-10 shadow-sm">
                    <h4 class="text-main font-weight-bold">Submit New Request</h4>
                    <br>
                    <form method="post" class="col-12" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Select Inspection Type:</label>
                            <div class="row type">
                                <?php
                                $inspectiontypeq = $a->con->prepare("select * from inspectiontypes");
                                $inspectiontypeq->execute();
                                while ($inspectiontype = $inspectiontypeq->fetch()) {
                                    ?>
                                    <div class="col-md-6">
                                        <label class="px-2 py-1 border mb-2 border col-md-12">
                                            <input type="checkbox" value="<?php echo $inspectiontype['id']; ?>"
                                                   name="typecheck"> <?php echo $inspectiontype['name']; ?>
                                        </label>
                                    </div>
                                    <?php
                                }
                                ?>
                                <input type="hidden" name="type" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Insured / Applicant Information</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="name" maxlength="255" class="form-control" required
                                           placeholder="Name">
                                </div>
                                <div class="col-md-4">
                                    <input type="email" name="email" maxlength="255" class="form-control" required
                                           placeholder="Email Address">
                                </div>
                                <div class="col-md-4">
                                    <input type="tel" name="phone" maxlength="30" class="form-control" required
                                           placeholder="Phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Subject Property Information</label>
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <input type="text" name="address" maxlength="255" class="form-control" required
                                           placeholder="Address">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="city" maxlength="30" class="form-control" required
                                           placeholder="City">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="state" maxlength="30" class="form-control" required
                                           placeholder="State">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="zipcode" maxlength="30" class="form-control" required
                                           placeholder="Zip Code">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Send Invoice(s) To:</label>
                            <div class="row invoices">
                                <div class="col-md-6">
                                    <div>
                                        <label class="px-2 py-1 mb-1">
                                            <input type="checkbox" value="Homeowner" name="invoicehomeowner">
                                            Homeowner
                                        </label>
                                    </div>
                                    <div>
                                        <label class="px-2 py-1 mb-1">
                                            <input type="checkbox" value="Requester" name="invoicerequestor">
                                            Requester
                                        </label>
                                    </div>
                                    <div>
                                        <label class="px-2 py-1 mb-1 nav justify-content-start">
                                            <input type="checkbox" value="Other" name="invoiceother"> &nbsp;Other
                                            &nbsp;&nbsp;<input name="invoiceothertext"
                                                               class="form-control-sm border-0" maxlength="50"
                                                               style="border-bottom:1px solid #ccc !important;height:auto;padding-top:0px;">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comments" class="px-2 py-1 mb-1 nav justify-content-start">Comments</label>
                            <textarea name="comments" class="form-control" rows="5" maxlength="2000"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="files" class="px-2 py-1 mb-1 nav justify-content-start">Related
                                Files</label>
                            <input type="file" name="uploadfile"
                                   accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*"
                                   class="p-2 col-12 border"
                                   multiple/>

                        </div>
                        <div class="mt-5">
                            <button type="submit" name="submitrequest" class="btn bg-main text-white px-4">Submit
                                Request
                            </button>
                        </div>
                    </form>
                    <script>
                        $('.type input[name="typecheck"]').change(function () {
                            var name = 'type';
                            if ($(this).prop("checked") == true) {
                                var catg = $(this).val();
                                if ($(".type input[name='" + name + "']").val().includes(catg) == true) {
                                    $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val());
                                } else {
                                    if ($(".type input[name='" + name + "']").val() == '') {
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val() + catg);
                                    } else {
                                        $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val() + ' - ' + catg);
                                    }
                                }
                            } else if ($(this).prop("checked") == false) {
                                var catg = $(this).val();
                                if ($(".type input[name='" + name + "']").val().includes(catg) == true) {
                                    $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(' - ' + catg, ''));
                                    $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(catg + ' - ', ''));
                                    $(".type input[name='" + name + "']").val($(".type input[name='" + name + "']").val().replace(catg, ''));
                                }
                            }
                        });

                    </script>
                    <?php
                    if (isset($_POST['submitrequest'])) {
                        // Add the new request to the database.
                        $reqid = substr(sha1($_POST['email'] . strtotime("now") . rand(1111, 9999)), 0, 6);
                        $invoicehomeowner = (isset($_POST['invoicehomeowner'])) ? 'Yes' : 'No';
                        $invoicerequestor = (isset($_POST['invoicerequestor'])) ? 'Yes' : 'No';
                        $invoiceother = (isset($_POST['invoiceother'])) ? 'Yes' : 'No';
                        $invoiceothertext = (isset($_POST['invoiceother'])) ? $_POST['invoiceothertext'] : '';

                        $query = $a->con->prepare("INSERT INTO requests(reqid, user, type, name, email, phone, address, city, state, zipcode,invoicehomeowner,invoicerequestor,invoiceother,invoiceothertext, comments) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                        $status = $query->execute([$reqid, $_SESSION['user'], $_POST['type'], $_POST['name'], $_POST['email'], $_POST['phone'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zipcode'], $invoicehomeowner, $invoicerequestor, $invoiceother, $invoiceothertext, $_POST['comments']]);

                        if ($status) {
                            // Add any associated files to the database.
                            if (isset($_FILES) && $_FILES['uploadfile']['name'] != '') {
                                $fileid = substr(sha1(strtotime("now") . rand(1111, 9999)), 0, 6);
                                $img = $_FILES['uploadfile']['tmp_name'];
                                $pic = $_FILES['uploadfile']['name'];
                                $ext = pathinfo($pic, PATHINFO_EXTENSION);
                                $roughmix = 'assets/images/requests/' . $reqid . sha1($pic . strtotime(("now") . rand(1111, 9999))) . '.' . $ext;

                                $addfileq = $a->con->prepare("INSERT INTO requestfiles(fileid,request,agency,file,name,insertby) VALUES(?,?,?,?,?,?)");
                                $st2 = $addfileq->execute([$fileid, $reqid, $_SESSION['user'], $roughmix, $pic, $_SESSION['user']]);

                                if ($st2) {
                                    move_uploaded_file($img, $roughmix);
                                }
                            }

                            // Send a notification to administrators.
                            $email_message = '
Hello There,<br />
<br />

A new request has been submitted on ' . $site['name'] . '<br />
Inspection Type: ' . $_POST[''] . '<br/>
Applicant Name: ' . $_POST['name'] . '<br />
Applicant Email: ' . $_POST['email'] . '<br />
Applicant Phone: ' . $_POST['phone'] . '<br />
<br />
Subject Address: ' . $_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $_POST['zipcode'] . '<br />
<br />
Comments: ' . $_POST['comments'] . '                
Send Invoices To: <br />';

                            if ($invoicehomeowner == 'Yes') {
                                $email_message .= "Home Owner<br />";
                            }
                            if ($invoicerequestor == 'Yes') {
                                $email_message .= "Requestor <br/>";
                            }
                            if ($invoiceother == 'Yes') {
                                $email_message .= "Other: $invoiceothertext<br />";
                            }

                            $common->sendEmailToGroup("Admin", "New Request Created", $email_message, $headers);
                            $_SESSION['request'] = 'added';
                            echo ' <script> window.location = "/my-requests.php;";</script >';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
</body>
</html>