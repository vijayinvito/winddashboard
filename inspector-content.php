<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <style>
        .file-upload-img {
            display: block;
            text-align: center;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 12px;
        }

        .file-upload-img .file-select {
            display: block;
            border: 2px solid #dce4ec;
            color: #34495e;
            cursor: pointer;
            line-height: 40px;
            text-align: left;
            background: #FFFFFF;
            overflow: hidden;
            position: relative;
        }

        .file-upload-img .file-select .file-select-button {
            background: #dce4ec;
            padding: 0 10px;
            display: inline-block;
            line-height: 30px;
            width: 100%;
        }

        .file-upload-img .file-select .file-select-name {
            height: auto;
            display: inline-block;
            padding: 20px;
            text-align: center;
        }

        .file-upload-img.active .file-select .file-select-name {
            height: auto;
            display: inline-block;
            padding: 20px;
            text-align: center;
        }

        .file-upload-img .file-select .file-select-name img {
            width: 10%;
            margin: auto;
        }

        .file-upload-img.active .file-select .file-select-name img {
            width: 30%;
            margin: auto;
        }

        .file-upload-img .file-select:hover .file-select-name {
            border: 3px dashed #34495e !important;
            transition: all .5s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
        }

        .file-upload-img .file-select:hover .file-select-button {
            background: #34495e;
            color: #FFFFFF;
            transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
        }

        .file-upload-img.active .file-select {
            border-color: #34495e;
            transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
        }

        .file-upload-img.active .file-select .file-select-button {
            background: #34495e;
            color: #FFFFFF;
            transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
        }

        .file-upload-img .file-select input[type=file] {
            z-index: 100;
            cursor: pointer;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            opacity: 0;
            filter: alpha(opacity=0);
        }

        .file-upload-img .file-select.file-select-disabled {
            opacity: 0.65;
        }

        .file-upload-img .file-select.file-select-disabled:hover {
            cursor: default;
            display: block;
            border: 2px solid #dce4ec;
            color: #34495e;
            cursor: pointer;
            height: 40px;
            line-height: 40px;
            margin-top: 5px;
            text-align: left;
            background: #FFFFFF;
            overflow: hidden;
            position: relative;
        }

        .file-upload-img .file-select.file-select-disabled:hover .file-select-button {
            background: #dce4ec;
            color: #666666;
            padding: 0 10px;
            display: inline-block;
            height: 40px;
            line-height: 40px;
        }

        .file-upload-img .file-select.file-select-disabled:hover .file-select-name {
            line-height: 40px;
            display: inline-block;
            padding: 0 10px;
        }

    </style>
    <?php if ($user['type'] != 'Admin') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <?php
    if (isset($_GET['cust'])) {
        $inspectorq = $a->con->prepare("select * from users where userid = ?");
        $inspectorq->execute([$_GET['cust']]);
        $inspector = $inspectorq->fetch();
    } else {
        echo '<script> window.location = "/index.php" </script>';
    }
    ?>
    <title><?php echo $inspector['name']; ?>'s Content | <?php echo $site['name'] ?></title>
</head>

<body>
<div class="pl-4 pr-4 pt-3 text-dark nav justify-content-between">
    <h4 class="mb-0 font-weight-bold "><?php echo $inspector['name']; ?>'s Content</h4>
</div>
<?php
$query = $a->con->prepare("select * from content where inspector = ? order by id desc");
$query->execute([$_GET['cust']]);
?>
<div class=" p-3 text-dark">
    <?php
    if (isset($_SESSION['file'])) {
        if ($_SESSION['file'] == 'added') {
            ?>
            <div class="col-12">
                <div class="alert alert-success font-weight-bold">
                    File(s) Added Successfully
                </div>
            </div>
            <?php
        }
        if ($_SESSION['file'] == 'deleted') {
            ?>
            <div class="col-12">
                <div class="alert alert-success font-weight-bold">
                    File Deleted Successfully
                </div>
            </div>
            <?php
        }
        if ($_SESSION['file'] == 'updated') {
            ?>
            <div class="alert alert-success font-weight-bold">
                File(s) Updated Successfully
            </div>
            <?php
        }
        unset($_SESSION['file']);
    } ?>
    <div class="col-12 px-2 mb-5">
        <form method="post" enctype="multipart/form-data"
              class="col-12 mb-3 bg-white p-3 border nav justify-content-between">
            <?php

            if (isset($_GET['x'])) {
                $query = $a->con->prepare("select * from content where id = ? and inspector = ?");
                $query->execute([$_GET['x'], $_GET['cust']]);
                if ($query->rowCount() > 0) {
                    $rec = $query->fetch();
                } else {
                    echo '<script> window.location = "manage-users.php"; </script>';
                }
            }
            ?>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label>File Name</label>
                <input class="form-control" type="text" name="name" required value="<?php if (isset($_GET['x'])) {
                    echo $rec['name'];
                } ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label>File Attachment</label>

                <div class="file-upload <?php if (isset($_GET['x'])) {
                    echo 'active';
                } ?>">
                    <div class="file-select">
                        <div class="file-select-button" id="fileName"> Browse</div>
                        <div class="file-select-name" id="noFile"><?php if (isset($_GET['x'])) {
                                echo substr($rec['file'], 29);
                            } else {
                                echo 'No File Choosen...';
                            } ?></div>
                        <input type="file" name="file" accept="application/pdf"
                               id="file" <?php if (!(isset($_GET['x']))) {
                            echo '';
                        } ?>>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 mt-2 text-right">
                <button type="submit" name="<?php if (isset($_GET['x'])) {
                    echo 'update';
                } else {
                    echo 'submit';
                } ?>" class="btn btn-dark m-0 btn-sm"><?php if (isset($_GET['x'])) {
                        echo 'Update';
                    } else {
                        echo 'Add New ';
                    } ?> File
                </button>
            </div>
        </form>
        <br>
        <div class="row px-2">
            <?php
            $query = $a->con->prepare("select * from content where inspector = ? order by id desc");
            $query->execute([$_GET['cust']]);
            $cc = 1;
            while ($res = $query->fetch()) {
                ?>
                <div class="col-lg-2 col-md-3 col-sm-12 p-2 file-hover">
                    <div class="col-12 bg-white p-0 text-center h-100 border">
                        <a class="col-12 p-0 text-dark" target="_blank" href="<?php echo $res['path']; ?>"
                           style="text-decoration:none;">
                            <div class="col-12 bg-white px-3 pt-3 pb-2 text-center">
                                <img class="col-12 p-0 mb-2" src="images/icons/pdf.png">
                                <div style="font-weight:600;"><?php echo $res['name']; ?></div>
                            </div>
                        </a>
                        <p class="mb-0 text-danger delete-file pb-1"
                           style="cursor:pointer;font-weight:500;font-size:90%;" id="<?php echo $res['id']; ?>">Delete
                            this file</p>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
</body>

<?php include('includes/footer.php'); ?>

</html>

<script>
    $('#file').bind('change', function () {
        var filename = $("#file").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").text("No file chosen...");
        } else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", "").substring(0, 25));
        }
    });

    $(".delete-file").click(function () {
        swal({
            title: "Are you sure?",
            text: "You will not be able to revert this action!",
            icon: "warning",
            className: "text-center",
            buttons: true,
            showCancelButton: true,
            dangerMode: true,
        }, function (Proceed) {
            if (Proceed) {
                $.ajax({
                    url: 'adminfunctions',
                    type: 'post',
                    data: {
                        t: 'deleterecord',
                        d: 'content',
                        i: $(this).attr('id')
                    },
                    success: function (data) {
                        window.location = window.location.href;
                    }
                });
            } else {

            }
        });


    });

</script>

<?php
if (isset($_POST['submit'])) {
    $img = $_FILES['file']['tmp_name'];
    $pic = $_FILES['file']['name'];
    $ext = pathinfo($pic, PATHINFO_EXTENSION);
    $path = "inspector/content/" . sha1($pic . strtotime("now")) . '.' . $ext;

    $query = $a->con->prepare("insert into content(inspector,name,path) values (?,?,?)");
    $status = $query->execute([$_GET['cust'], $_POST['name'], $path]);
    if ($status) {
        $getmailq = $a->con->prepare("select name,email from users where userid = ?");
        $getmailq->execute([$_GET['cust']]);
        $getmail = $getmailq->fetch();
        $subject = "New Invoice | " . $site['name'];
        $message = "Hello " . $getmail['name'] . ",<br><br>
A new invoice [" . $_POST['name'] . "] has been added to your account. Please login to your account to view it.
<br><br>
- Thank You<br>
- " . $site['name'] . " Team";
        mail($getmail['email'], $subject, $message, $headers);
        move_uploaded_file($img, $path);
        $_SESSION['content'] = 'added';
        echo '<script> window.location = window.location.href; </script>';
    }
}
if (isset($_POST['update'])) {


    $img = $_FILES['file']['tmp_name'];
    $pic = $_FILES['file']['name'];
    $ext = pathinfo($pic, PATHINFO_EXTENSION);
    if ($ext != '') {
        $path = "inspector/content/" . sha1($pic . strtotime("now")) . '.' . $ext;
        move_uploaded_file($img, $path);
    } else {
        $path = $rec['file'];
    }

    $query = $a->con->prepare("update content set name=?,path=? where id = ?");
    $status = $query->execute([$_POST['name'], $path, $_GET['x']]);
    if ($status) {
        $_SESSION['content'] = 'updated';
        echo '<script> window.location = "inspector-content?cust=' . $_GET['cust'] . '"; </script>';
    }
}
?>
