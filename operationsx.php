<?php

session_start();

include('includes/config.php');

include('includes/ics.class.php');

$con = new PDO($SETTINGS['host'], $SETTINGS['mysql_user'], $SETTINGS['mysql_pass']);

$t = $_POST['t'];

$sitequery = $con->prepare("select * from website");

$sitequery->execute();

$site = $sitequery->fetch();

$sitename = $site['name'];

$siteurl = "https://portfolio.stratprop.com.au/";

$sitemail = 'hello@stratprop.com.au';

$headers = "MIME-Version: 1.0" . "\r\n";

$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

$headers .= "From: $sitemail";

$bccmail = 'Strat.prop.ba@gmail.com';

$headers .= "Bcc: $bccmail\r\n";



$userq = $con->prepare("SELECT * FROM users WHERE userid = ?");

$userq->execute([$_SESSION['user']]);

$user = $userq->fetch();



switch ($t) {

    case 'addCalendar':

        // Get the request info.

        $requestq = $con->prepare("select * from requests where reqid = ?");

        $requestq->execute([$_POST['reqId']]);



        if ($requestq->rowCount() > 0) {

            $request = $requestq->fetch();

            $enddate = new DateTime($request['appointment_start']);

            $enddate = $enddate->add(new DateInterval("PT1H"));



            // Create the event.

            $ics = new ics();



            $ics->createICS($request['appointment_start'], $enddate->format('Y-m-d H:i:s'), "Inspection", "This is an event made by Wind Mitigations", $request['address'] . ' ' . $request['city'] . ' ' . $request['state'] . ' ' . $request['zipcode']);



            // Display the ICS to the user.

            $ics->show();

        }

        break;

    case 'getconversation':

        $read = $con->prepare("UPDATE messages SET red = 1 WHERE convid = ? AND user <> ?");

        $read->execute([$_SESSION['user'], $_SESSION['user']]);



        $disdateq = $con->prepare("SELECT DISTINCT date(inserton) FROM messages WHERE convid = ? ORDER BY date(inserton) ASC");

        $disdateq->execute([$_SESSION['user']]);



        if ($disdateq->rowCount() > 0) {

            while ($disdate = $disdateq->fetch()) {

                ?>

                <div class="col-12 p-0 text-center pt-3 pb-3">

                <span class="btn btn-light p-2 mb-0 text-secondary"

                      style="border-radius:25px; font-size:85%;"><?php echo date("dS", strtotime($disdate[0])) . ' of ' . date("M Y", strtotime($disdate[0])) ?></span>

                </div>

                <?php

                $query = $con->prepare("select * from messages where convid = ? and inserton LIKE ? order by id asc");

                $query->execute([$_SESSION['user'], $disdate[0] . '%']);

                $lastuser = '';



                while ($res = $query->fetch()) {

                    $getimgq = $con->prepare("select logo from users where userid = ?");

                    $getimgq->execute([$res['user']]);

                    $getimg = $getimgq->fetch();



                    if ($res['user'] == $_SESSION['user']) {

                        ?>

                        <div class="col-12 p-2  nav justify-content-end" style="<?php if ($res['user'] != $lastuser) {

                            echo 'padding-bottom:1px !important;';

                        } ?><?php if ($res['user'] == $lastuser) {

                            echo 'padding-bottom:1px !important;padding-top:1px !important;';

                        } ?>">

                            <div class="col-10 nav justify-content-end p-0">

                                <div class="p-0 pt-1 pb-1 pl-2 pr-2" style="background:#007BFF;

                                        border: 2px solid transparent;

                                        padding: 10px;

                                        color:white;

                                        width:auto;

                                        max-width:90%;border-top-left-radius:8px;border-bottom-left-radius:8px;border-top-right-radius:8px;">

                                    <div style="font-size:95%;"

                                         class="col-12 pr-3 p-0 text-right"><?php echo $res['message'] ?></div>

                                    <div style="font-size:10px;line-height:15px;margin-top:4px;"

                                         class="col-12 p-0 text-right message-time"><?php echo date("h:i A", strtotime($res['inserton'])) ?></div>

                                </div>

                            </div>

                        </div>

                        <?php

                    } else {

                        ?>

                        <div class="col-12 p-2 nav justify-content-start" style="<?php if ($res['user'] != $lastuser) {

                            echo 'padding-bottom:1px !important;';

                        } ?><?php if ($res['user'] == $lastuser) {

                            echo 'padding-bottom:1px !important;padding-top:1px !important;';

                        } ?>">

                            <div class="col-10 nav justify-content-start pl-2">

                                <div class="p-0 pt-1 pb-1 pl-2 pr-2" style="background:#E5E6EA;

                                            border: 1px solid lightgrey;

                                            color:#2d323e;

                                            padding: 10px;

                                            width:auto;

                                            max-width:90%;border-bottom-left-radius:8px;border-bottom-right-radius:8px;border-top-right-radius:8px;">

                                    <div style="font-size:95%;" class="pr-3"><?php echo $res['message'] ?></div>

                                    <div style="font-size:10px;line-height:15px;margin-top:4px;color:1C1C22;"

                                         class="col-12 p-0 text-right message-time"><?php echo date("h:i A", strtotime($res['inserton'])) ?></div>

                                </div>

                            </div>

                        </div>

                        <?php

                    }

                    ?>



                    <?php

                    $lastuser = $res['user'];

                }

            }

        } else {

            echo '<h6 class="p-5 text-secondary text-center"><i class="far fa-comments fa-5x text-secondary text-center"></i><br><br>Start a conversation now</h6>';

        }

        break;

    case 'getconversationadmin':

        $read = $con->prepare("UPDATE messages SET red = 1 WHERE convid = ? AND replyto = ''");

        $read->execute([$_POST['i']]);

        $disdateq = $con->prepare("SELECT DISTINCT DATE(inserton) FROM messages WHERE convid = ? ORDER BY DATE(inserton) ASC");

        $disdateq->execute([$_POST['i']]);

        if ($disdateq->rowCount() > 0) {

            while ($disdate = $disdateq->fetch()) {

                ?>

                <div class="col-12 p-0 text-center pt-3 pb-3">

                <span class=" btn-light p-2 mb-0 text-dark"

                      style="border-radius:25px; font-size:85%;"><?php echo date("dS", strtotime($disdate[0])) . ' of ' . date("M Y", strtotime($disdate[0])) ?></span>

                </div>

                <?php

                $query = $con->prepare("SELECT * FROM messages WHERE convid = ? AND inserton LIKE ? ORDER BY id ASC");

                $query->execute([$_POST['i'], $disdate[0] . '%']);

                $lastuser = '';

                while ($res = $query->fetch()) {

                    $getimgq = $con->prepare("select logo from users where userid = ?");

                    $getimgq->execute([$res['user']]);

                    $getimg = $getimgq->fetch();

                    if ($res['replyto'] != '') {

                        ?>

                        <div class="col-12 p-2  nav justify-content-end" style="<?php if ($res['user'] != $lastuser) {

                            echo 'padding-bottom:1px !important;';

                        } ?><?php if ($res['user'] == $lastuser) {

                            echo 'padding-bottom:1px !important;padding-top:1px !important;';

                        } ?>">

                            <div class="col-10 nav justify-content-end p-0">



                                <?php if (($user['type'] == 'Admin' || $user['type'] == 'Staff') && $res['user'] != $lastuser) {

                                    ?>

                                    <div class="col-12 p-0 text-right small" style="color:lightgrey;">

                                        <?php

                                        $addedbyq = $con->prepare("SELECT name FROM users WHERE userid = ?");

                                        $addedbyq->execute([$res['user']]);

                                        $addedby = $addedbyq->fetch();

                                        echo $addedby['name'];

                                        ?>

                                    </div>

                                    <?php

                                } ?>

                                <div class="p-0 pt-1 pb-1 pl-2 pr-2" style="background:#007BFF;

                    border: 2px solid transparent;

                    padding: 10px;

                    color:white;

                    width:auto;

                    max-width:90%;border-top-left-radius:8px;border-bottom-left-radius:8px;border-top-right-radius:8px;">

                                    <div style="font-size:95%;"

                                         class="col-12 p-0 text-right pr-3"><?php echo $res['message'] ?></div>

                                    <div style="font-size:10px;line-height:15px;"

                                         class="col-12 p-0 text-right message-time"><?php echo date("h:i A", strtotime($res['inserton'])) ?></div>

                                </div>

                            </div>

                        </div>

                        <?php

                    } else {

                        ?>

                        <div class="col-12 p-2 nav justify-content-start" style="<?php if ($res['user'] != $lastuser) {

                            echo 'padding-bottom:1px !important;';

                        } ?><?php if ($res['user'] == $lastuser) {

                            echo 'padding-bottom:1px !important;padding-top:1px !important;';

                        } ?>">

                            <div class="col-10 nav justify-content-start pl-2">

                                <div class="p-0 pt-1 pb-1 pl-2 pr-2" style="background:#E5E6EA;

                                    border: 1px solid lightgrey;

                                    color:#2d323e;

                                    padding: 10px;

                                    width:auto;

                                    max-width:90%;border-bottom-left-radius:8px;border-bottom-right-radius:8px;border-top-right-radius:8px;">

                                    <div style="font-size:95%;" class="pr-3"><?php echo $res['message'] ?></div>

                                    <div style="font-size:10px;line-height:15px;"

                                         class="col-12 p-0 pl-3 text-right text-secondary message-time"><?php echo date("h:i A", strtotime($res['inserton'])) ?></div>

                                </div>

                            </div>

                        </div>

                        <?php

                    }

                    ?>



                    <?php

                    $lastuser = $res['user'];

                }



            }

        } else {

            echo 'No Messages Found';

        }

        break;

    case 'getunreadmessages':

        $unreadmessageq = $con->prepare("select * from messages where receiver = ? and red = 0");

        $unreadmessageq->execute([$_SESSION['user']]);

        echo $unreadmessageq->rowCount();

        break;

    case 'lastmsg':

        if ($user['type'] == 'Admin') {

            $replyto = $_POST['i'];

            $conv = $_POST['i'];

        } else {

            $replyto = '';

            $conv = $_SESSION['user'];

        }



        $query = $con->prepare("SELECT id FROM messages WHERE convid = ? ORDER BY id DESC limit 1");

        $query->execute([$conv]);

        $result = $query->fetch();



        echo $result['id'];

        break;

    case 'sendtext':

        if ($user['type'] == 'Admin') {

            $replyto = $_POST['i'];

            $conv = $_POST['i'];

        } else {

            $replyto = '';

            $conv = $_SESSION['user'];

        }



        $img = $_FILES['attachment']['tmp_name'];

        $pic = $_FILES['attachment']['name'];

        $ext = pathinfo($pic, PATHINFO_EXTENSION);



        echo $ext;



        if ($ext != '') {

            $path = 'assets/images/messages/' . sha1($pic . date("Y-m-d H:i:s")) . '.' . $ext;

            $message = 'File: <a href="' . $path . '" target="_blank">' . $pic . '</a> ' . '<br>' . $_POST['txt'];

            $query = $con->prepare("INSERT INTO messages(convid,user, message,replyto) VALUES (?,?,?,?)");

            $status = $query->execute([$conv, $_SESSION['user'], $message, $replyto]);



            if ($status) {

                echo 'File Attached ';

                move_uploaded_file($img, $path);

            }

        } else {

            $query = $con->prepare("INSERT INTO messages(convid,user, message,replyto) VALUES (?,?,?,?)");

            $status = $query->execute([$conv, $_SESSION['user'], $_POST['txt'], $replyto]);



            if ($status) {

                echo '';

            }

        }

        break;

}

?>

