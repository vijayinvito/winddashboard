<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <title>Content | <?php echo $site['name'] ?></title>
    <?php if ($user['type'] != 'Inspector') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
</head>

<body>
<div class="px-4">
    <div>
        <h3 class="font-weight-bold text-black">Content</h3>
    </div>
</div>
<div class="pl-4 pr-4 mb-5 text-dark">
    <div class="row px-2">
        <?php
        $query = $a->con->prepare("select * from content where inspector = ? order by id desc");
        $query->execute([$_SESSION['user']]);
        $cc = 1;
        while ($res = $query->fetch()) {
            ?>
            <div class="col-lg-2 col-md-3 col-sm-12 p-2 file-hover">
                <div class="col-12 bg-white p-0 text-center h-100 border rounded-10 shadow-sm">
                    <a class="col-12 p-0 text-dark" href="<?php echo $res['file']; ?>" style="text-decoration:none;"
                       download="<?php echo $res['name']; ?>">
                        <div class="col-12 bg-white px-3 pt-3 pb-2 rounded-10 text-center">
                            <img class="col-12 p-0 mb-2" src="assets/images/icons/file.png">
                            <div style="font-weight:600;"><?php echo $res['name']; ?></div>
                        </div>
                    </a>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<?php include('includes/footer.php'); ?>
</body>

</html>
<script>
    $(".nav.content").addClass('active-link');

</script>
