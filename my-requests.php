<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Agency') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>My Requests | <?php echo $site['name'] ?></title>
</head>

<body>
<style>
    table {
        overflow-x: visible;
        }

        .table td {
            white-space: nowrap;
            vertical-align: middle;
        }

        .tooltip-inner {
            text-align: left;
            font-size: 90%;
        }

        .tooltip-inner hr {
            background: white;
            margin-top: 3px;
            margin-bottom: 3px;
        }

    </style>
    <?php
    if(isset($_GET['status'])){
    $crstatus = '';
    if($_GET['status'] == 'Completed'){
        $crstatus = 'completed';
    }
    $stcond = ($_GET['status'] != '') ? ' and status = "'.$_GET['status'].'"' : '' ;
    }
    else{
    $crstatus = '';
    $stcond = '';
    }
    ?>
    <div class="px-4 nav justify-content-between">
        <div>
            <h4 class="mb-0 font-weight-bold text-black">My Requests</h4>
        </div>
        <div>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Filter Status: </span>
                </div>
                <select class="form-control filter-status">
                    <option selected value="">All</option>
                    <?php 
                    $statusarr = ['Pending','Assigned','Scheduled','Review','Completed'];
                    $statusnamearr = ['Pending','Assigned','Scheduled','Submitted for Review','Completed'];
                    $sc = 0;
                    while($sc != count($statusarr)){
                        ?>
                    <option <?php if(isset($_GET['status']) && $_GET['status'] == $statusarr[$sc]){echo 'selected class="font-weight-bold"';} ?> value="<?php echo $statusarr[$sc]; ?>"><?php echo $statusnamearr[$sc]; ?></option>
                    <?php
                        $sc++;
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="pl-4 pr-4 p-3 text-dark">
        <?php
        if(isset($_SESSION['request'])){
    if($_SESSION['request'] == 'added'){
        ?>
        <div class="alert alert-success font-weight-bold">
            New Request Added Successfully
        </div>
        <?php
    }
    if($_SESSION['request'] == 'updated'){
        ?>
        <div class="alert alert-success font-weight-bold">
            Request Updated Successfully
        </div>
        <?php
    }
    unset($_SESSION['request']);
} ?>
        <div class="p-3 border bg-white rounded-10 shadow-sm">
            <table class="table col-12 p-0 mb-5 table-striped" id="table">
                <thead class="bg-black text-light font-weight-normal">
                <tr>
                    <th>Type</th>
                    <th>Applicant</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>ZipCode</th>
                    <th>Added At</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = $a->con->prepare("select * from requests where user = ? $stcond order by id desc");
                    $query->execute([$_SESSION['user']]);
                    while($res = $query->fetch()){ 
                    $usertooltip = '<div><div>Applicant Name: '.$res['name'].'</div><hr><div>Email: '.$res['email'].'</div><hr><div>Phone: '.$res['phone'].'</div></div>';
                    ?>
                    <tr>
                        <td><?php 
                            $itc = 0;
                            $itarr = explode(' - ', $res['type']);
                            $tforms = count($itarr);
                            $cforms = 0;
                            while($itc < count($itarr)){
                                $inspectiontypeq = $a->con->prepare("select * from inspectiontypes where id = ?");
                                $inspectiontypeq->execute([$itarr[$itc]]);
                                $inspectiontype = $inspectiontypeq->fetch();
                                ?>
                                <div>
                                    <?php echo $inspectiontype['name']; ?>
                                </div>
                                <?php
                                $itc++;
                            }
                                ?>
                        </td>
                        <td><?php echo $res['name'] ?> <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="<?php echo $usertooltip; ?>"></i></td>
                        <td> <?php echo $res['address'] ?> </td>
                        <td> <?php echo $res['city'] ?> </td>
                        <td> <?php echo $res['zipcode'] ?> </td>
                        <td> <?php echo date("M dS, Y", strtotime($res['inserton'])) ?> </td>
                        <td>
                            <?php if($res['status'] == 'Pending'){ ?> <span class="btn btn-sm btn-primary py-0 font-weight-500">Pending</span> <?php } ?>
                            <?php if($res['status'] == 'Assigned'){ ?> <span class="btn btn-sm btn-warning py-0 font-weight-500">Assigned</span> <?php } ?>
                            <?php if ($res['status'] == 'Scheduled') { ?> <span
                                    class="btn btn-sm btn-danger py-0 font-weight-500">Scheduled</span> <?php } ?>
                            <?php if ($res['status'] == 'Cancelled') { ?> <span
                                    class="btn btn-sm btn-danger py-0 font-weight-500">Cancelled</span>
                                <?php
                                $canceltooltip = '<div><div>Cancellation Reason:</div><hr><div>' . $res['cancel'] . '</div></div>';
                                ?>
                                <button class="btn btn-sm btn-light bg-white border font-weight-500 py-0 px-1"
                                        data-toggle="tooltip" data-placement="top"
                                        title="<?php echo $canceltooltip; ?>"><i class="fas fa-info-circle"></i>
                                </button> <?php } ?>
                            <?php if ($res['status'] == 'Review') { ?> <span
                                    class="btn btn-sm btn-warning text-black py-0 font-weight-500">Submitted for Review</span> <?php } ?>
                            <?php if ($res['status'] == 'Completed') { ?> <span
                                    class="btn btn-sm btn-success py-0 font-weight-500">Completed</span> <?php } ?>
                        </td>
                        <td>
                            <a href="request?reqid=<?php echo $res['reqid']; ?>"
                               class="btn btn-sm shadow btn-primary">View <i
                                        class="fas fa-long-arrow-alt-right fa-sm"></i></a>
                            <?php if ($res['status'] == 'Pending' || $res['status'] == 'Assigned' || $res['status'] == 'Scheduled') { ?>
                                <button class="btn btn-sm btn-danger btn-cancel font-weight-500"
                                        id="<?php echo $res['reqid']; ?>">Cancel <i class="fas fa-times fa-sm pl-1"></i>
                                </button>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php include('includes/footer.php'); ?>
</body>

</html>

<script>
    $(".nav.myrequests<?php echo $crstatus; ?>").addClass('active-link');

</script>
<script>
    $(".filter-status").change(function () {
        window.location = "?status=" + $(".filter-status").val();
    });
    $(".btn-cancel").click(function () {
        var i = $(this).attr('id');
        swal({
            title: "Confirmation",
            text: "Are you sure you want to cancel?",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Please write the reason"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false
            } else {
                $.ajax({
                    url: '/operations',
                    type: 'post',
                    data: {
                        t: 'cancelRequest',
                        note: inputValue,
                        i: i
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        });
    });
    $(".status").click(function () {
        if ($(this).hasClass('btn-Active')) {
            var txt = 'Inactive';
        } else {
            var txt = 'Active';
        }
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'changestatus',
                d: 'users',
                i: $(this).attr('id'),
                txt: txt
            },
            success: function (data) {
                window.location.reload();
            }
        });
    });

</script>
