<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <?php if ($user['type'] != 'Inspector') {
        echo '<script> window.location = "/index.php" </script>';
    } ?>
    <title>Assigned Requests | <?php echo $site['name'] ?></title>
</head>

<body>
<?php
if (isset($_GET['status'])) {
    $crstatus = '';
    if ($_GET['status'] == 'Completed') {
        $crstatus = 'completed';
    }
    $stcond = ($_GET['status'] != '') ? ' and status = "' . $_GET['status'] . '"' : '';
} else {
    $crstatus = '';
    $stcond = '';
}
?>
<div class="px-4 nav justify-content-between">
    <div>
        <h4 class="mb-0 font-weight-bold text-black">Assigned Requests</h4>
    </div>
    <div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Filter Status: </span>
            </div>
            <select class="form-control filter-status">
                <option selected value="">All</option>
                <?php
                $statusarr = ['Assigned', 'Scheduled', 'Review', 'Completed'];
                $statusnamearr = ['Assigned', 'Scheduled', 'Submitted for Review', 'Completed'];
                $sc = 0;
                while ($sc != count($statusarr)) {
                    ?>
                    <option <?php if (isset($_GET['status']) && $_GET['status'] == $statusarr[$sc]) {
                        echo 'selected';
                    } ?> value="<?php echo $statusarr[$sc]; ?>"><?php echo $statusnamearr[$sc]; ?></option>
                    <?php
                    $sc++;
                }
                ?>
            </select>
        </div>
    </div>
</div>
<div class="pl-4 pr-4 p-3 text-dark">
    <div class="modal fade" id="assignmodal" tabindex="-1" role="dialog" aria-labelledby="assignmodal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-main text-center p-4 text-light">
                    <div class="text-center col-12 p-0">
                        <h4 class="mb-0">Assign an Inspector</h4>
                    </div>
                </div>
                <div class="modal-body text-center py-4 text-dark">
                    <form method="post">
                        <h6 class="text-center">Select an inspector from below</h6>
                        <div class="col-lg-6 col-md-7 col-sm-12 mx-auto">
                            <select class="form-control select-inspector">
                                <?php
                                $inspectorsq = $a->con->prepare("select * from users where type = ?");
                                $inspectorsq->execute(['Inspector']);
                                while ($inspectors = $inspectorsq->fetch()) {
                                    ?>
                                    <option value="<?php echo $inspectors[0]; ?>"><?php echo $inspectors['name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="text-center mt-4">
                            <button type="button" class="btn btn-sm text-danger border-danger btn-light"
                                    data-dismiss="modal" aria-label="Close">
                                Close <i class="fas fa-times fa-sm"></i>
                            </button>
                            &nbsp;
                            <button type="submit" name="assigninspector"
                                    class="btn btn-sm btn-primary font-weight-500 px-3">Assign <i
                                        class="fas fa-check fa-sm pl-1"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="p-3 border bg-white rounded-10 shadow-sm">
        <table class="table col-12 p-0 mb-5 table-striped" id="table">
            <thead class="bg-black text-light font-weight-normal">
            <tr>
                <th>Agency Info</th>
                <th>Applicant Information</th>
                <th>Property Information</th>
                <th>Detailed Address</th>
                <th>Other Info</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $query = $a->con->prepare("select * from requests where assigned = ? $stcond order by assigndate desc");
            $query->execute([$_SESSION['user']]);
            while ($res = $query->fetch()) {
                $agencyq = $a->con->prepare("select * from users where userid = ?");
                $agencyq->execute([$res['user']]);
                $agency = $agencyq->fetch();
                ?>
                <tr class="row<?php echo $res['reqid']; ?>">
                    <td>
                        <span class="font-weight-600">Company Name</span>
                        <div><?php echo $agency['company'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">Company Phone</span>
                        <div><?php echo $agency['companyphone'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">Agent Name</span>
                        <div><?php echo $agency['name'] ?></div>
                    </td>
                    <td>
                        <span class="font-weight-600">Name</span>
                        <div><?php echo $res['name'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">Email Address</span>
                        <div><?php echo $res['email'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">Phone</span>
                        <div><?php echo $res['phone'] ?></div>
                    </td>
                    <td>
                        <span class="font-weight-600">Address</span>
                        <div><?php echo $res['address'] ?></div>
                    </td>
                    <td>
                        <span class="font-weight-600">City</span>
                        <div><?php echo $res['city'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">State</span>
                        <div><?php echo $res['state'] ?></div>
                        <hr class="my-2">
                        <span class="font-weight-600">Zip Code</span>
                        <div><?php echo $res['zipcode'] ?></div>
                    </td>
                    <td>
                        <span class="font-weight-600">Inspection Type</span>
                        <div>
                            <?php
                            $itc = 0;
                            $itarr = explode(' - ', $res['type']);
                            $tforms = count($itarr);
                            $cforms = 0;
                            while ($itc < count($itarr)) {
                                $inspectiontypeq = $a->con->prepare("select * from inspectiontypes where id = ?");
                                $inspectiontypeq->execute([$itarr[$itc]]);
                                $inspectiontype = $inspectiontypeq->fetch();
                                ?>
                                <div><?php echo $inspectiontype['name']; ?></div>
                                <?php
                                $itc++;
                            }
                            ?>
                        </div>
                        <div class="mt-1 pt-1">
                            <span class="font-weight-600">Added At</span>
                            <div><?php echo date("M dS, Y", strtotime($res['inserton'])) ?></div>
                        </div>
                    </td>
                    <td>
                        <?php if ($res['status'] == 'Assigned') { ?>
                            <div class="status-label">
                                <span class="btn btn-sm btn-warning font-weight-500 py-0">Assigned</span>
                            </div>
                        <?php } ?>
                        <?php if ($res['status'] == 'Scheduled') { ?>
                            <div class="status-label">
                                <span class="btn btn-sm btn-danger font-weight-500 py-0">Scheduled</span>
                                <span class="btn btn-sm btn-dark font-weight-500 py-0 shadow pointer reschedule-btn"
                                      data-toggle="collapse" data-target="#reschedule<?php echo $res['reqid']; ?>">Reschedule <i
                                            class="fas fa-long-arrow-alt-right fa-sm"></i></span>
                                <span id="btn-calendar"
                                      onclick="addToCalendar('<?php echo $res['reqid']; ?>');"
                                      class="btn btn-sm btn-dark font-weight-500 py-0 shadow pointer">
                                    <i class="fas fa-calendar"></i> Calendar
                                </span>
                                <div id="reschedule<?php echo $res['reqid']; ?>" class="collapse">
                                    <div class="input-group">
                                        <input type="date" class="border p-2 col-7" name="date"
                                               min="<?php echo date("Y-m-d"); ?>" required>
                                        <input type="time" class="border p-2 col-5" name="time" required>
                                    </div>
                                    <button class="btn btn-sm col-12 btn-light font-weight-500 btn-reschedule border"
                                            id="<?php echo $res['reqid']; ?>">Submit &nbsp;<i
                                                class="fas fa-save fa-sm"></i></button>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($res['status'] == 'Review') { ?>
                            <div class="status-label">
                                <span class="btn btn-sm btn-warning text-black font-weight-500 py-0">Submitted for Review</span>
                            </div>
                        <?php } ?>
                        <?php if ($res['status'] == 'Completed') { ?>
                            <div class="status-label">
                                <span class="btn btn-sm btn-success font-weight-500 py-0">Completed</span>
                            </div>
                        <?php } ?>
                        <?php if ($res['appointment_start'] != '') {
                            ?>
                            <hr class="my-2">
                            <span class="font-weight-600">Scheduled For</span>
                            <div class="scheduledhistory<?php echo $res['reqid']; ?>">
                                <?php
                                $getschecq = $a->con->prepare("SELECT * FROM schedule WHERE reqid = ? AND id <> (SELECT MAX(ID) FROM schedule WHERE reqid = ?)");
                                $getschecq->execute([$res['reqid'], $res['reqid']]);

                                while ($getschec = $getschecq->fetch()) {
                                    echo '<div style="opacity:0.5;"><i class="fas fa-history fa-sm pr-1"></i> ' . date("M dS, Y h:i A", strtotime($getschec["date"])) . '</div>';
                                }
                                ?>
                                <div class="font-weight-500"><?php echo '<i class="far fa-clock fa-sm pr-1"></i> ' . date("M dS, Y h:i A", strtotime($res['appointment_start'])); ?></div>
                            </div>
                            <?php if ($res['status'] == 'Scheduled') {
                                ?>
                                <div class="mt-2 forms<?php echo $res['reqid']; ?>">
                                    <div class="mt-2">
                                        <button id="<?php echo $res['reqid']; ?>"
                                                class="btn btn-sm btn-success col-12 shadow-sm font-weight-500 pointer btn-submit-review">
                                            Submit for Review <i class="fas fa-check-double fa-sm"></i></button>
                                    </div>
                                </div>
                                <?php
                            } else if ($res['status'] == 'Review') {
                                ?>
                                <hr class="my-2">
                                <span class="font-weight-600">Submitted for Review at</span>
                                <div><?php echo date("M dS, Y h:i A", strtotime($res['reviewdate'])); ?></div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="mt-3 mb-3 scheduled<?php echo $res['reqid']; ?>">
                                <span class="btn btn-danger col-12 shadow-sm font-weight-600 btn-sm pointer">Schedule Inspection &nbsp;<i
                                            class="fas fa-arrow-down fa-sm"></i></span>
                                <br>
                                <div class="input-group">
                                    <input type="date" class="border p-2 col-7" name="date"
                                           min="<?php echo date("Y-m-d"); ?>" required>
                                    <input type="time" class="border p-2 col-5" name="time" required>
                                </div>
                                <button class="btn btn-sm col-12 btn-light font-weight-500 btn-schedule border"
                                        id="<?php echo $res['reqid']; ?>">Submit &nbsp;<i class="fas fa-save fa-sm"></i>
                                </button>
                            </div>
                            <?php
                        }
                        if ($res['status'] == 'Completed') { ?>
                            <hr class="my-2">
                            <span class="font-weight-600">Completed At</span>
                            <div><?php echo date("M dS, Y h:i A", strtotime($res['completedate'])); ?></div>
                        <?php }
                        ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php include('includes/footer.php'); ?>
</body>

</html>


<script>
    $(".nav.assignedrequests<?php echo $crstatus; ?>").addClass('active-link');

    $(".filter-status").change(function () {
        window.location = "?status=" + $(".filter-status").val();
    });
    $(".btn-schedule").click(function () {
        var i = $(this).attr('id');
        var date = $("input[name='date']").val();
        var time = $("input[name='time']").val();
        if (date != '' && time != '') {
            swal({
                title: "Schedule it for " + date + " " + time + "?",
                text: "You will not be able to revert this action!",
                icon: "warning",
                className: "text-center",
                buttons: true, showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: '/adminfunctions',
                        type: 'post',
                        data: {
                            t: 'schedulerequest',
                            date: date,
                            time: time,
                            i: i
                        },
                        success: function (data) {
                            if (data != '') {
                                $(".scheduled" + i).html('<hr class="my-2"><span class="font-weight-600">Scheduled To</span><div class="font-weight-500"><i class="far fa-clock fa-sm pr-1"></i> ' + data + '</div>');
                                $(".row" + i + " .status-label").html('<span class="btn btn-sm btn-danger font-weight-500 py-0">Scheduled</span>');
                            }
                        }
                    });
                }
            });
        }
    });
    $(".btn-reschedule").click(function () {
        var i = $(this).attr('id');
        var date = $("input[name='date']").val();
        var time = $("input[name='time']").val();
        if (date != '' && time != '') {
            swal({
                title: "Schedule it for " + date + " " + time + "?",
                text: "You will not be able to revert this action!",
                icon: "warning",
                className: "text-center",
                buttons: true, showCancelButton: true,
                dangerMode: true,
            }, function (Proceed) {
                if (Proceed) {
                    $.ajax({
                        url: 'adminfunctions',
                        type: 'post',
                        data: {
                            t: 'schedulerequest',
                            date: date,
                            time: time,
                            i: i
                        },
                        success: function (data) {
                            if (data != '') {
                                $(".scheduledhistory" + i).html(data);
                                $('#reschedule' + i).collapse('hide');
                            }
                        }
                    });
                }
            });
        }
    });
    $(".btn-submit-review").click(function () {
        var i = $(this).attr('id');
        swal({
            title: "Are you sure to Submit for Review to Admin?",
            text: "You will not be able to update the forms once you submit it!",
            icon: "warning",
            className: "text-center",
            buttons: true, showCancelButton: true,
            dangerMode: true,
        }, function (Proceed) {
            if (Proceed) {
                $.ajax({
                    url: 'adminfunctions',
                    type: 'post',
                    data: {
                        t: 'submitreviewrequest',
                        i: i
                    },
                    success: function (data) {
                        if (data != '') {
                            $(".forms" + i).html('<hr class="my-2"><span class="font-weight-600">Submitted for Review at</span><div>' + data + '</div>');
                            $(".row" + i + " .status-label").html('<span class="btn btn-sm btn-warning text-black font-weight-500 py-0">Submitted for Review</span>');
                        }
                    }
                });
            }
        });
    });
    $(".btn-assign").click(function () {
        var i = $(this).attr('id');
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'changestatus',
                d: 'users',
                i: $(this).attr('id'),
                txt: txt
            },
            success: function (data) {
                window.location = window.location.href;
            }
        });
    });

    function addToCalendar(requestId) {
        $.ajax({
            url: '/operationsx.php',
            type: 'post',
            data: {
                t: 'addCalendar',
                reqId: requestId
            },
            success: function (data) {
                window.open("data:text/calendar;charset=utf8," + escape(data));
            }
        });
    };

</script>
