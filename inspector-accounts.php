<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php'); ?>
    <?php include('includes/header.php'); ?>
    <title>Inspector Accounts | <?php echo $site['name'] ?></title>
    <?php if ($user['type'] != 'Admin') {
        echo '<script> window.location = ' / index . php;
        ' </script>';
    } ?>
    <?php
    $cond = (isset($_GET['cust'])) ? ' where inspector = "' . $_GET['cust'] . '" ' : '';
    if (isset($_GET['cust'])) {
        $inspectorq = $a->con->prepare("select * from users where userid = ?");
        $inspectorq->execute([$_GET['cust']]);
        $inspector = $inspectorq->fetch();
    }
    $head = (isset($_GET['cust'])) ? '<h6 class="font-weight-bold mb-0 text-uppercase">'.$inspector['name'].'</h6>' : '';
    ?>
</head>

<body>
    <div class="pl-4 pr-4 pt-3 text-dark nav justify-content-between">
        <div>
            <?php echo $head; ?>
            <h4 class="mb-0 font-weight-bold text-dark">Inspector Accounts |
                <span class="text-secondary" style="font-size:70%;"><?php 
            $totalq = $a->con->prepare("select count(*) from accounts $cond");
            $totalq->execute();
            $total = $totalq->fetch();
            echo 'Total: '.number_format($total[0]);
                        ?></span>
            </h4>
        </div>
        <div class="text-right ">
            <a class="btn btn-sm btn-primary excel-btn text-light p-2 " style="cursor:pointer">Export Excel</a>
        </div>
    </div>
    <?php 
    $query = $a->con->prepare("select * from accounts $cond order by id desc");
    $query->execute();
    ?>
    <style>
        table th {
            letter-spacing: 0.9px;
        }

        table td {
            color: #404040;
        }

        .show-pass {
            cursor: pointer;
        }

        .show-in-report {
            display: none;
        }

    </style>
    <div class="pl-4 pr-4 p-3 text-dark">
        <?php 
        if($query->rowCount() > 0){
        ?>
            <table class="table table-responsive col-12 p-0 mb-5 bg-white shadow text-center" id="table">
                <thead class="bg-dark text-light font-weight-normal">
                <tr>
                    <th>#</th>
                    <th>Inspector</th>
                    <th>Account Manager</th>
                    <th class="hide-in-report">Account Type</th>
                    <th class="show-in-report">Account Type</th>
                    <th>Expert Advisor</th>
                    <th>Account Number</th>
                    <th class="hide-in-report">Password</th>
                    <th class="show-in-report">Password</th>
                    <th>Server</th>
                    <th class="hide-in-report">Status</th>
                    <th class="show-in-report">Status</th>
                    <th class="hide-in-report">Added At</th>
                    <th class="hide-in-report"></th>
                </tr>
            </thead>
            <tbody>
                <?php $cc = 1;while($res = $query->fetch()){ 
                $custq = $a->con->prepare("select * from users where userid = ?");
                $custq->execute([$res['inspector']]);
                $cust = $custq->fetch();
                ?>
                <tr>
                    <td> <?php echo $cc; ?> </td>
                    <td> <?php echo $cust['name'] ?> </td>
                    <td> <?php echo $res['accountmanager'] ?> </td>
                    <td class="hide-in-report">
                        <div class="text-center">
                            <?php echo $res['type'] ?> <span class="hide-in-report"><?php if($res['type'] == 'Live'){echo ' &nbsp; | &nbsp;<a href="inspector-chart.php?account='.$res[0].'&cust='.$res['inspector'].'"><b>View Chart</b></a>';} ?> </span>
                        </div>
                    </td>
                    <td class="show-in-report">
                        <div class="text-center">
                            <?php echo $res['type'] ?>
                        </div>
                    </td>
                    <td> <?php echo $res['advisor'] ?> </td>
                    <td> <?php echo $res['account'] ?> </td>
                    <td class="hide-in-report">
                        <span class="txt-pass<?php echo $res['id'] ?>">******</span> <i id="<?php echo $res['password']; ?>~<?php echo $res['id'] ?>" class="fas fa-eye text-primary show-pass"></i>
                    </td>
                    <td class="show-in-report">
                        <span class="txt-pass"><?php echo $res['password'] ?></span>
                    </td>
                    <td> <?php echo $res['server'] ?> </td>
                    <td class="hide-in-report">
                        <div>
                            <select class="select-status hide-in-report border" id="<?php echo $res[0] ?>">
                                <option <?php if($res['status'] == 'Pending'){ echo 'disabled selected'; } ?> value="Pending">Pending</option>
                                <option value="Active" <?php if($res['status'] == 'Active'){ echo 'disabled selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($res['status'] == 'Inactive'){ echo 'disabled selected'; } ?>>Inactive</option>
                            </select>
                            <span class="show-in-report" style="display:none;"><?php echo $res['status'] ?></span>
                        </div>
                    </td>
                    <td class="show-in-report">
                        <div class="text-center">
                            <?php echo $res['status'] ?>
                        </div>
                    </td>
                    <td class="hide-in-report"><?php echo date("F d, Y", strtotime($res['inserton'])); ?></td>
                    <td class="hide-in-report"><a href="update-account.php?x=<?php echo $res['id']; ?>"><i class="fas fa-pencil-alt text-primary"></i></a></td>
                </tr>
                <?php $cc++; } ?>
            </tbody>
        </table>
        <?php } else{
            ?>
        <h5 class="text-center text-secondary p-5">No Accounts Found</h5>
        <?php
        } ?>
    </div>
</body>
<script>

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2014-11-29/FileSaver.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.13/xlsx.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
        $(".excel-btn").click(function() {
            $(".excel-btn").text('Processing...');
            $(".show-in-report").css('display', 'block');
            $(".hide-in-report").css('display', 'none');
            setTimeout(function() {
                $("th:hidden,td:hidden").remove();
                var workbook = XLSX.utils.book_new();
                var worksheet_data = document.getElementById("table");
                var worksheet = XLSX.utils.table_to_sheet(worksheet_data);

                workbook.SheetNames.push("Inspector Accounts");
                workbook.Sheets["Inspector Accounts"] = worksheet;

                exportExcelFile(workbook);
            }, 1000);
            setTimeout(function() {
                window.location = window.location.href;
            }, 2000);

        });
    })

    function exportExcelFile(workbook) {
        return XLSX.writeFile(workbook, "Inspector-Accounts-<?php echo date("M dS, Y"); ?>.xlsx");
    }







    $(".show-pass").click(function() {
        var i = $(this).attr('id').split('~');
        if ($(this).hasClass('fa-eye')) {
            $(".txt-pass" + i[1]).html(i[0]);
            $(this).addClass('fa-eye-slash');
            $(this).removeClass('fa-eye');
        } else {
            $(".txt-pass" + i[1]).html('******');
            $(this).addClass('fa-eye');
            $(this).removeClass('fa-eye-slash');
        }
    });
    $(".nav.accounts").addClass('active-link');

    $(".select-status").change(function() {
        var txt = $(this).val().trim();
        $.ajax({
            url: 'adminfunctions',
            type: 'post',
            data: {
                t: 'changeaccountstatus',
                i: $(this).attr('id'),
                txt: txt
            },
            success: function(data) {
                $(".select-status option").attr("disabled", false);
                $(".select-status option[value='" + txt + "']").attr("disabled", true);
            }
        });
    });

</script>
