<?php session_start(); ?>

<?php error_reporting(E_ALL); ?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="UTF-8">

    <?php include('includes/essentials.php'); ?>

    <?php include('includes/header.php'); ?>

    <?php if ($user['type'] != 'Admin') {

        echo '<script> window.location = "/index.php" </script>';

    } ?>

    <title>All Requests | <?php echo $site['name'] ?></title>

</head>



<body>

<?php

$status = (isset($_GET['status'])) ? $_GET['status'] : '';

$statuscond = ($status != '') ? ' and status = "' . $status . '" ' : '';



?>

<div class="px-4 nav justify-content-between">

    <div>
        <!-- <?=$statuscond?> -->
        <h4 class="mb-0 font-weight-bold text-black">All Requests</h4>

    </div>

    <div class="p-0">

        <div class="input-group">

            <div class="input-group-prepend">

                <span class="input-group-text">Status</span>

            </div>

            <select class="form-control filter-status">

                <option value="">All</option>

                <option <?php echo ($status == 'Cancelled') ? 'selected class="font-weight-bold"' : ''; ?>>Cancelled

                </option>

                <option <?php echo ($status == 'Pending') ? 'selected class="font-weight-bold"' : ''; ?>>Pending

                </option>

                <option <?php echo ($status == 'Assigned') ? 'selected class="font-weight-bold"' : ''; ?>>Assigned

                </option>

                <option <?php echo ($status == 'Scheduled') ? 'selected class="font-weight-bold"' : ''; ?>>Scheduled

                </option>

                <option <?php echo ($status == 'Review') ? 'selected class="font-weight-bold"' : ''; ?>>Review</option>

                <option <?php echo ($status == 'Completed') ? 'selected class="font-weight-bold"' : ''; ?>>Completed

                </option>

            </select>

            <script>

                $(".filter-status").change(function () {

                    if ($(this).val() != '') {

                        window.location = "?status=" + $(this).val();

                    } else {

                        window.location = "all-requests";

                    }

                });



            </script>

        </div>

    </div>

</div>

<div class="pl-4 pr-4 p-3 text-dark">

    <div class="modal fade" id="assignmodal" tabindex="-1" role="dialog" aria-labelledby="assignmodal"

         aria-hidden="true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header bg-main text-center p-4 text-light">

                    <div class="text-center col-12 p-0">

                        <h4 class="mb-0">Assign an Inspector</h4>

                    </div>

                </div>

                <div class="modal-body text-center py-4 text-dark">

                    <form method="post">

                        <h6 class="text-center">Select an inspector from below</h6>

                        <div class="col-lg-6 col-md-7 col-sm-12 mx-auto">

                            <select class="form-control select-inspector">

                                <?php

                                $inspectorsq = $a->con->prepare("select * from users where type = ?");

                                $inspectorsq->execute(['Inspector']);

                                while ($inspectors = $inspectorsq->fetch()) {

                                    ?>

                                    <option value="<?php echo $inspectors[0]; ?>"><?php echo $inspectors['name']; ?></option>

                                    <?php

                                }

                                ?>

                            </select>

                        </div>

                        <div class="text-center mt-4">

                            <button type="button" class="btn btn-sm text-danger border-danger btn-light"

                                    data-dismiss="modal" aria-label="Close">

                                Close <i class="fas fa-times fa-sm"></i>

                            </button>

                            &nbsp;

                            <button type="submit" name="assigninspector"

                                    class="btn btn-sm btn-primary font-weight-500 px-3">Assign <i

                                        class="fas fa-check fa-sm pl-1"></i></button>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

    <div class="p-3 border bg-white rounded-10 shadow-sm">

        <table class="table col-12 p-0 mb-5" id="table">

            <thead class="bg-black text-light font-weight-normal">

            <tr>

                <th>Agency</th>

                <th>Applicant</th>

                <th>Property Location</th>

                <th>Inspection Type</th>

                <th>Added At</th>

                <th>Assigned Inspector</th>

                <th>Status</th>

                <th>Action</th>

            </tr>
                                <!-- <tr><td><?=$queryI = "select * from requests  WHERE deleted = false  ".$statuscond." order by id desc;"?></td></tr> -->
            </thead>

            <tbody>

            <?php

            $query = $a->con->prepare("select * from requests  WHERE deleted = false  ".$statuscond." order by id desc;");

            
            $query->execute();

            while ($res = $query->fetch()) {

                $agencyq = $a->con->prepare("select * from users where userid = ?");

                $agencyq->execute([$res['user']]);

                $agency = $agencyq->fetch();

                $agencytooltip = '<div><div>Company Name: ' . $agency['company'] . '</div><hr><div>Company Phone: ' . $agency['companyphone'] . '</div><hr><div>Agent Name: ' . $agency['name'] . '</div></div>';

                $usertooltip = '<div><div>Applicant Name: ' . $res['name'] . '</div><hr><div>Email: ' . $res['email'] . '</div><hr><div>Phone: ' . $res['phone'] . '</div></div>';

                ?>

                <tr class="row<?php echo $res['reqid']; ?>">

                    <td>

                        <div><?php echo $agency['company'] ?> <i class="fas fa-info-circle" data-toggle="tooltip"

                                                                 data-placement="top"
                                                                 data-html="true"
                                                                 title="<?=$agencytooltip?>"></i>
                                                                 
                                                                </div>

                    </td>

                    <td>

                        <div><?php echo $res['name'] ?> <i class="fas fa-info-circle" data-toggle="tooltip"

                                                           data-placement="top" title="<?php echo $usertooltip; ?>"></i>
                                                         

                        </div>

                    </td>

                    <td><?php echo $res['address'] ?> - <?php echo $res['zipcode'] ?> - <?php echo $res['city'] ?>

                        , <?php echo $res['state'] ?></td>

                    <td>

                        <?php

                        $itc = 0;

                        $itarr = explode(' - ', $res['type']);

                        $tforms = count($itarr);

                        $cforms = 0;

                        while ($itc < count($itarr)) {

                            $inspectiontypeq = $a->con->prepare("select * from inspectiontypes where id = ?");

                            $inspectiontypeq->execute([$itarr[$itc]]);

                            $inspectiontype = $inspectiontypeq->fetch();

                            ?>

                            <div><?php echo $inspectiontype['name']; ?></div>

                            <?php

                            $itc++;

                        }

                        ?>

                    </td>

                    <td><?php echo date("M dS, Y", strtotime($res['inserton'])); ?></td>

                    <td>

                        <div class="assigned<?php echo $res['reqid']; ?>">

                            <?php

                            if ($res['status'] == 'Completed' || $res['status'] == 'Cancelled') {

                                $inspectorq = $a->con->prepare("SELECT * FROM users WHERE userid = ?");

                                $inspectorq->execute([$res['assigned']]);

                                $inspector = $inspectorq->fetch();

                                ?>

                                <div><?php echo($inspector['name']) ?></div>

                                <?php

                            } else {

                                ?>

                                <select class="select-inspector border p-2 col-12"

                                        id="<?php echo $res['reqid']; ?>">

                                    <option value="">Assign Inspector &nbsp;<i class="fas fa-arrow-down fa-sm"></i>

                                    </option>

                                    <?php

                                    $inspectorsq = $a->con->prepare("SELECT * FROM  users WHERE type = ?");

                                    $inspectorsq->execute(['Inspector']);

                                    while ($inspectors = $inspectorsq->fetch()) {

                                        ?>

                                        <option value="<?php echo $inspectors['userid']; ?>" <?php echo($res['assigned'] == $inspectors['userid'] ? ' SELECTED ' : '') ?>><?php echo $inspectors['name']; ?></option>

                                        <?php

                                    }

                                    ?>

                                </select>

                                <?php

                            }

                            ?>

                        </div>

                    </td>

                    <!--

                    <td>

                        <div><?php echo $inspector['name'] ?></div>

                        <?php

                    if ($res['status'] == 'Pending' || $res['status'] != 'Assigned') {

                        ?>



                            <?php

                    } ?>

                    </td>

                    -->

                    <td>

                        <?php if ($res['status'] == 'Pending') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-primary py-0">Pending</span>

                            </div>

                        <?php } ?>

                        <?php if ($res['status'] == 'Assigned') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-warning font-weight-500 py-0">Assigned</span>

                            </div>

                        <?php } ?>

                        <?php if ($res['status'] == 'Scheduled') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-danger font-weight-500 py-0">Scheduled</span>

                            </div>

                        <?php } ?>

                        <?php if ($res['status'] == 'Cancelled') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-danger font-weight-500 py-0">Cancelled</span>

                                <?php

                                $canceltooltip = '<div><div>Cancellation Reason:</div><hr><div>' . $res['cancel'] . '</div></div>';

                                ?>

                                <button class="btn btn-sm btn-light bg-white border font-weight-500 py-0 px-1"

                                        data-toggle="tooltip" data-placement="top"

                                        title="<?php echo $canceltooltip; ?>"><i class="fas fa-info-circle"></i>

                                </button>

                            </div>

                            <div class="text-center">

                                <span class="font-weight-600 text-dark pointer btn-delete"

                                      id="<?php echo $res['reqid']; ?>"><i class="fas fa-trash fa-sm"></i>&nbsp;<u>Delete Request</u></span>

                            </div>

                        <?php } ?>

                        <?php if ($res['status'] == 'Review') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-warning text-black font-weight-500 py-0">Submitted for Review</span>

                            </div>

                            <div class="mark-box<?php echo $res['reqid']; ?>">

                                <button id="<?php echo $res['reqid']; ?>"

                                        class="btn btn-sm mt-1 btn-success py-0 col-12 shadow-sm font-weight-500 pointer btn-complete">

                                    Mark Completed <i class="fas fa-check-double fa-sm"></i></button>

                            </div>

                        <?php } ?>

                        <?php if ($res['status'] == 'Completed') { ?>

                            <div class="status-label">

                                <span class="btn btn-sm btn-success font-weight-500 py-0">Completed</span>

                            </div>

                        <?php } ?>

                    </td>

                    <!--

                    <td>

                    <?php

                                        if ($res['scheduled'] != '') {

                                            ?>

                    <hr class="my-2">

                    <span class="font-weight-600">Scheduled To</span>

                    <div><?php echo date("M dS, Y h:i A", strtotime($res['appointment_start'])); ?></div>

                    <?php

                                        }

                                        if ($res['status'] == 'Completed') { ?>

                    <hr class="my-2">

                    <span class="font-weight-600">Completed At</span>

                    <div><?php echo date("M dS, Y h:i A", strtotime($res['completedate'])); ?></div>

                    <?php } ?>

                    </td>

                    -->

                    <td>

                        <a href="request?reqid=<?php echo $res['reqid']; ?>"

                           class="btn btn-sm btn-primary">

                            View

                            <i class="fas fa-long-arrow-alt-right fa-sm"></i></a>

                    </td>

                </tr>

            <?php } ?>

            </tbody>


        </table>
                    
    </div>

</div>

<?php include('includes/footer.php'); ?>

</body>



</html>



<script>

    $(".nav.allrequests").addClass('active-link');



</script>

<script>

    $(".filter-status").change(function () {

        window.location = "?status=" + $(".filter-status").val();

    });



    $(".select-inspector").change(function () {

        var i = $(this).attr('id');

        var val = $(this).val();

        var txt = $(this).children("option").filter(":selected").text()

        swal({

            title: "Are you sure want to assign " + txt + "?",

            text: "Agency and inspector will be immediately notified!",

            icon: "warning",

            className: "text-center",

            buttons: true,

            showCancelButton: true,

            dangerMode: true,

        }, function (Proceed) {

            if (Proceed) {

                $.ajax({

                    url: '/adminfunctions',

                    type: 'post',

                    data: {

                        t: 'assigninspector',

                        val: val,

                        i: i

                    },

                    success: function (data) {

                        if (data == 'assigned') {

                            $(".assigned" + i).html('<div>' + txt + '</div>');

                            $(".row" + i + " .status-label").html('<span class="btn btn-sm btn-warning font-weight-500 py-0">Assigned</span>');

                        }

                    }

                });

            }

        });

    });

    $(".btn-assign").click(function () {

        var i = $(this).attr('id');

        $.ajax({

            url: 'adminfunctions',

            type: 'post',

            data: {

                t: 'changestatus',

                d: 'users',

                i: $(this).attr('id'),

                txt: txt

            },

            success: function (data) {

                window.location = window.location.href;

            }

        });

    });

    $(".btn-complete").click(function () {

        var i = $(this).attr('id');

        swal({

            title: "Are you sure to Mark it as Completed?",

            text: "You will not be able to revert this action!",

            icon: "warning",

            className: "text-center",

            buttons: true,

            showCancelButton: true,

            dangerMode: true,

        }, function (Proceed) {

            if (Proceed) {

                $.ajax({

                    url: 'adminfunctions',

                    type: 'post',

                    data: {

                        t: 'markcompleted',

                        i: i

                    },

                    success: function (data) {

                        if (data != '') {

                            $(".mark-box" + i).html('<hr class="my-2"><span class="font-weight-600">Completed At</span><div>' + data + '</div>');

                            $(".row" + i + " .status-label").html('<span class="btn btn-sm btn-success font-weight-500 py-0">Completed</span>');

                        }

                    }

                });

            }

        });

    });

    $(".btn-delete").click(function () {

        var i = $(this).attr('id');

        swal({

            title: "Confirmation",

            text: "Are you sure you want to delete this request?",

            icon: "warning",

            className: "text-center",

            buttons: true,

            showCancelButton: true,

            dangerMode: true,

        }, function (Proceed) {

            if (Proceed) {

                $.ajax({

                    url: 'adminfunctions',

                    type: 'post',

                    data: {

                        t: 'deleteRequest',

                        reqid: i

                    },

                    success: function (data) {

                        if (data != '') {

                            $(".row" + i).css('display', 'none');

                        }

                    }

                });

            }

        });

    });



    $(".status").click(function () {

        if ($(this).hasClass('btn-Active')) {

            var txt = 'Inactive';

        } else {

            var txt = 'Active';

        }

        $.ajax({

            url: 'adminfunctions',

            type: 'post',

            data: {

                t: 'changestatus',

                d: 'users',

                i: $(this).attr('id'),

                txt: txt

            },

            success: function (data) {

                window.location = window.location.href;

            }

        });

    });



</script>

