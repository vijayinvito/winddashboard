<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include('includes/essentials.php');
    $query = $a->con->prepare("select * from website");
    $query->execute();
    $result = $query->fetch();
    ?>
    <?php include('includes/header.php'); ?>

    <?php if (!(isset($_SESSION['user']))) {
        echo "<script> window.location = '/index.php' </script>";
    }
    ?>
    <?php
    if ($user['type'] != 'Admin') {
        echo "<script> window.location = '/index.php' </script>";
    }
    ?>
    <title>Set Up Site | <?php echo $site['name'] ?></title>
</head>

<body>

<div class="px-4 pt-3 text-dark nav justify-content-between">
    <h4 class="mb-0 font-weight-bold text-black">Website Configuration
    </h4>
</div>
<div class="px-4 py-3 nav justify-content-between">
    <form method="post" class="col-md-7 p-0 p-4 bg-white py-4  border rounded-10 shadow-sm"
          enctype="multipart/form-data">
        <div class="col-12 p-3">
            <div class="form-group row">
                <div class="col-md-3">
                    <label> Name </label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="text" name="name" value="<?php echo $result['name'] ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label> Logo </label>
                </div>
                <div class="col-md-9">
                    <div class="file-upload">
                        <div class="file-select">
                            <div class="file-select-button" id="fileName">Choose File</div>
                            <div class="file-select-name" id="noFile"><?php echo substr($result['logo'], 7) ?></div>
                            <input type="file" accept="image/*" name="logo" id="logo" onchange="readURL(this);">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label> Email Address</label>
                </div>
                <div class="col-md-9">
                    <input class="form-control" type="email" name="email" value="<?php echo $result['email'] ?>"
                           required>
                    <span class="font-85"> (this is where you will be receiving emails) </span>
                </div>
            </div>

            <br><br>
            <div class="form-group row">
                <div class="col-md-3">
                </div>
                <div class="col-md-9">
                    <button class="p-1 pl-3 pr-3 btn btn-main " name="submit" type="submit">Update Info</button>
                </div>
            </div>
        </div>
    </form>

    <div class="col-lg-5 col-md-5 col-sm-12 text-light border bg-black p-3">
        <h5 class="text-uppercase"> Preview </h5>
        <br>
        <img class="logopreview" style="max-width:15rem;" src="<?php echo $result['logo'] ?>">
    </div>
</div>
</body>

<?php include('includes/footer.php'); ?>

</html>
<?php
if (isset($_POST['submit'])) {
    $img = $_FILES['logo']['tmp_name'];
    $pic = $_FILES['logo']['name'];
    if ($pic != '') {
        $path = "assets/images/logo.png";
        copy($img, $path);
    } else {
        $path = $site['logo'];
    }
    $add = $a->con->prepare("update website set name = ?, logo = ?,email=? where id = ?");
    $add->execute([$_POST['name'], $path, $_POST['email'], $result['id']]);
    $_SESSION['message'] = 'websiteupdated';
    echo '<script> window.location = "setup.php" </script>';
    exit();
}
?>


<script>
    function readURL(input) {
        var filename = $("#logo").val();
        if (/^\s*$/.test(filename)) {
            $(".file-upload").removeClass('active');
            $("#noFile").text("No file chosen...");
        } else {
            $(".file-upload").addClass('active');
            $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
        }

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.logopreview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }

    }

</script>

<script>
    $(".nav.setup").addClass('active-link');
</script>
