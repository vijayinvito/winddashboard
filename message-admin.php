<?php session_start(); ?>

<!DOCTYPE html>

<html lang="en">

<script> 
alert('<?=$_SESSION['user']?>');
</script>

<head>

    <meta charset="UTF-8">

    <?php include('includes/essentials.php'); ?>

    <title>All Messages | <?php echo $site['name'] ?></title>

</head>



<body>

<?php include('includes/header.php') ?>

<?php if (!(isset($_SESSION['user'])) && $user['type'] != 'Admin') {

    echo '<script> window.location = "/index.php;"; </script>';

} ?>

<?php

$query = $a->con->prepare("select userid,name,email from users where status = 'Active' and userid = ?");

$query->execute([$_SESSION['user']]);

$res = $query->fetch();

?>

<?php

if (isset($_SESSION['user'])) {

    $inspectorq = $a->con->prepare("select * from users where userid = ?");

    $inspectorq->execute([$_SESSION['user']]);

    $inspector = $inspectorq->fetch();

} else {

    echo '<script> window.location = "/index.php;" </script>';

}

?>

<div class="px-4 nav justify-content-between">

    <h4 class="mb-0 font-weight-bold text-black"><?php echo $inspector['name']; ?> | All Messages

    </h4>

</div>

<style>

    .user-message a {

        color: white !important;

    }



</style>

<div class="px-4 py-2">

    <div class="col-lg-12 col-md-12 col-sm-12 p-0 no-padding-sm" id="live-chat">

        <style>

            .messagesdiv::-webkit-scrollbar {

                width: 6px;

            }



            /* Track */

            .messagesdiv::-webkit-scrollbar-track {

                background: #f1f1f1;

            }



            /* Handle */

            .messagesdiv::-webkit-scrollbar-thumb {

                background: #888;

            }



            /* Handle on hover */

            .messagesdiv::-webkit-scrollbar-thumb:hover {

                background: #555;

            }



            .messagesdiv .justify-content-end a {

                color: white !important;

            }



            .messagesdiv .justify-content-start a {

                color: black !important;

            }



        </style>



        <div class="col-12 p-0 bg-white messagesdiv border rounded-10 shadow-sm"

             style="height:65vh; padding-bottom:5rem; overflow-y:scroll;background-image:url('https://www.toptal.com/designers/subtlepatterns/patterns/triangle-mosaic.png');">

            <h6 class="p-5 text-secondary text-center"><i

                        class="far fa-comments fa-5x text-secondary text-center"></i><br><br>Start a conversation now

            </h6>

        </div>

        <div class="p-3 border nav justify-content-between col-12 mt-3 senderbox rounded-10 shadow-sm"

             style="padding-bottom:0 !important;background:white; z-index:4;">

            <div class="col-12 p-0 uploaded" style="display:none">

                <div class="pb-2 pt-0"><i class="fas fa-check-circle fa-lg text-success"></i> <b class="pt-1">File

                        Uploaded Successfully </b> <small> - Make a zip file for more files.</small></div>

            </div>

            <div class="input-group mb-3">

                <input class="form-control mt-0 py-1 px-2 rounded-0 border-secondary text-message" type="text" autofocus

                       placeholder="Type here..." style="border:1px solid lightgrey !important;">



                <div class="input-group-text bg-white rounded-0">

                    <input type="file" name="attachment" id="attachment" style="display:none">

                    <label class="pb-0 mb-0" for="attachment">

                        <i class="fas fa-paperclip text-black text-center" style="font-size:1.2rem; cursor:pointer"></i></label>

                </div>

                <div class="input-group-text bg-white rounded-0">

                    <i class="far fa-paper-plane text-black text-center send-message" autofocus

                       style="font-size:1.2rem; cursor:pointer"></i>

                </div>

            </div>

        </div>

        <input type="hidden" class="lastmsgid">





        <script>

            function readURL(input) {

                if (input.files && input.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function (e) {

                        $(".uploaded").css('display', 'block');

                        $(".text-message").val('File Attached!');

                    }

                    reader.readAsDataURL(input.files[0]);

                }

            }



            $("#attachment").change(function () {

                readURL(this);

            });



        </script>

        <script>

            var intervalgoing = '';



            function getconversationcont() {

                intervalgoing = setInterval(newfunction, 1000);

            }



            function newfunction() {

                $.ajax({

                    url: '/operationsx.php',

                    type: 'post',

                    data: {

                        t: 'lastmsg',

                        i: '<?php echo $_SESSION['user']; ?>'

                    },

                    success: function (data) {

                        if (data != $(".lastmsgid").val()) {

                            $(".lastmsgid").val(data);

                            $.ajax({

                                url: '/operationsx.php',

                                type: 'post',

                                data: {

                                    t: 'getconversation',

                                    i: '<?php echo $_SESSION['user']; ?>'

                                },

                                success: function (data) {



                                    $(".messagesdiv").html('');

                                    $(".messagesdiv").append(data);

                                    $('.messagesdiv').scrollTop($('.messagesdiv')[0].scrollHeight);

                                    $(".message-badge").html('');

                                }

                            })

                        } else {



                        }

                    }

                })

            }



            $(document).ready(function () {

                getconversationcont();

            });



        </script>

        <script>

            function sendtext() {

                if ($(".text-message").val() != '') {

                    $(".send-message").removeClass('fa-paper-plane');

                    $(".send-message").removeClass('far');

                    $(".send-message").addClass('fas');

                    $(".send-message").addClass('fa-circle-notch');

                    $(".send-message").addClass('fa-spin');

                    $(".fa-circle-notch").removeClass('send-message');





                    var formData = new FormData();

                    formData.append('t', 'sendtext');

                    formData.append('attachment', $('#attachment')[0].files[0]);

                    formData.append('i', '<?php echo $_SESSION['user']; ?>');

                    formData.append('txt', $(".text-message").val());

                    $.ajax({

                        url: '/operationsx.php',

                        type: 'POST',

                        data: formData,

                        processData: false, // tell jQuery not to process the data

                        contentType: false, // tell jQuery not to set contentType

                        success: function (data) {

                            $(".text-message").val('');

                            $('#attachment').val('');

                            $(".uploaded").css('display', 'none');



                            $(".send-message").addClass('far');

                            $(".send-message").removeClass('fas');

                            $(".fa-circle-notch").addClass('fa-paper-plane');

                            $(".fa-circle-notch").addClass('send-message');

                            $(".fa-circle-notch").removeClass('fa-spin');

                            $(".send-message").removeClass('fa-circle-notch');

                            getconversationcont();

                        }

                    });





                }

            }



            $(".send-message").click(function () {

                sendtext();

            });

            $(".text-message").keypress(function (e) {

                if (e.which == 13) {

                    sendtext();

                }

            });



        </script>





    </div>

</div>



<script>

    $(".btn-delete").click(function () {

        var i = $(this).attr('id');

        swal({

            title: "Are you sure you want to delete?",

            text: "You will not be able to revert this action!",

            icon: "warning",

            className: "text-center",

            buttons: true,

            showCancelButton: true,

            dangerMode: true,

        }, function (Proceed) {

            if (Proceed) {

                $.ajax({

                    url: 'adminfunctions',

                    type: 'post',

                    data: {

                        t: 'deleterecord',

                        d: 'messages',

                        i: i

                    },

                    success: function (data) {

                        window.location = window.location.href;

                    }

                })

            }

        });

    });



</script>

<script>

    $('.nav.messages-agency').addClass('active-link');

</script>